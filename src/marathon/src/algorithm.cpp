#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <cassert>

#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>



#define DEBUG if(debug) cerr 

bool debug = false;

using namespace std;
using namespace boost;


// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
				boost::property<boost::edge_weight_t, int > > > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!


// Custom Edge Adder Class, that holds the references
// to the graph, capacity map, weight map and reverse edge map
// ===============================================================
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = c;
        weightmap[e] = w; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = w; // new
        revedgemap[e] = rev_e; 
        revedgemap[rev_e] = e; 
    }
};

void testcase() {
	int n, m, s, f;
	
	cin >> n >> m >> s >> f;
	
	Graph G(n);
	
    EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
    EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
    ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
    ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
    EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);
    
    for (int i = 0; i < m; i++) {
    	int a,b,c,d;
    	
    	cin >> a >> b >> c >> d;
    	
    	eaG.addEdge(a,b,c,d);
    	eaG.addEdge(b,a,c,d);
    	
    }
    
    // =======================
	std::vector<Vertex> predsource(n);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
	std::vector<Vertex> predtarget(n);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor

	std::vector<int> distsource(n);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
	std::vector<int> disttarget(n);
	Vertex start = s;
	
	boost::dijkstra_shortest_paths(G, start, // We MUST provide at least one of the two maps
		boost::predecessor_map(boost::make_iterator_property_map(predsource.begin(), boost::get(boost::vertex_index, G))).	// predecessor map as Named Parameter
		distance_map(boost::make_iterator_property_map(distsource.begin(), boost::get(boost::vertex_index, G))));	// distance map as Named Parameter

	int minDistance = distsource[f];

	DEBUG << "shortest path: " << distsource[f] << endl;
	DEBUG << "start: " << s << " finish: " << f << endl;
	
	boost::dijkstra_shortest_paths(G, f, // We MUST provide at least one of the two maps
		boost::predecessor_map(boost::make_iterator_property_map(predtarget.begin(), boost::get(boost::vertex_index, G))).	// predecessor map as Named Parameter
		distance_map(boost::make_iterator_property_map(disttarget.begin(), boost::get(boost::vertex_index, G))));	// distance map as Named Parameter

	assert(distsource[f] == disttarget[s]);



	EdgeIt ebeg, eend;
	for (boost::tie(ebeg, eend) = boost::edges(G); ebeg != eend; ++ebeg) {
		Vertex u = source(*ebeg, G);
		Vertex v = target(*ebeg, G);
	
		
		DEBUG << *ebeg << " " << weightmap[*ebeg] << " distsource: " << distsource[u] << " pre: " << predsource[u] << " disttarget: " << disttarget[v] << " pre: " << predtarget[v] << " weight: " << weightmap[*ebeg];
		
		if (distsource[u] + weightmap[*ebeg] + disttarget[v] != minDistance) {
			
			DEBUG << " setting capacity of edge " << *ebeg << " from " << capacitymap[*ebeg] <<  " to 0" << endl;
			capacitymap[*ebeg] = 0;
		} else {
			DEBUG << " leaving capacity of edge " << *ebeg << " at " << capacitymap[*ebeg] << endl;
		}
	}
	
	cout << boost::push_relabel_max_flow(G, s, f) << endl;
	
	
	/*
	DEBUG << "c flow values:" << std::endl;
  	graph_traits<Graph>::vertex_iterator u_iter, u_end;
 	 graph_traits<Graph>::out_edge_iterator ei, e_end;
  	for (boost::tie(u_iter, u_end) = vertices(G); u_iter != u_end; ++u_iter)
  	  for (boost::tie(ei, e_end) = out_edges(*u_iter, G); ei != e_end; ++ei)
      if (capacitymap[*ei] > 0)
        DEBUG << "f (" << *u_iter << "," << target(*ei, G) << ") " 
                  << (capacitymap[*ei] - rescapacitymap[*ei]) << std::endl;
	
	*/
	
}


int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    int t;
    cin >> t;
    
    for (int i = 0; i < t; i ++) {
    	testcase();
    }
    
}
