#include <iostream>
#include <stack>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

void testCase() {


    int n, m, start;
    cin >> n;
    cin >> m;
    cin >> start;

    vector<vector<int>> g(n);

    vector<int> tStart(n, -1);
    vector<int> tEnd(n, -1);

    for (int i = 0; i < m; i++) {

        int a, b;

        cin >> a;
        cin >> b;


        g[a].push_back(b);
        g[b].push_back(a);

    }

    for (int i = 0; i < n; i++) {
        sort(g[i].begin(), g[i].end());
    }

    stack<pair<int, int>> s;

    s.push(make_pair(start, 0)); //Vertex, index vom nächsten

    int time = 0;
    while (!s.empty()) {

        pair<int, int> vPair = s.top();
        s.pop();
        int v = vPair.first;
        int i = vPair.second;

        if (tStart[v] == -1) {
            tStart[v] = time;
            time++;
        }

        if (i > g[v].size() - 1) {
            tEnd[v] = time;
            time++;
        } else {
            s.push(make_pair(v, i + 1));
            if (tStart[g[v][i]] == -1) {
                s.push(make_pair(g[v][i], 0));
            }
        }


    }


    for (auto i : tStart) {
        cout << i << " ";
    }

    cout << endl;

    for (auto i: tEnd) {
        cout << i << " ";
    }

    cout << endl;

}

int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}