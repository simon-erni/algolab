#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>

using namespace std;

void testCase() {

    int n;
    cin >> n;
    int k;
    cin >> k;

    vector<int> v;

    vector<int> vSum;


    int t_sum = 0;
    for (int i = 0; i < n; i++) {

        int x;
        cin >> x;
        t_sum += x;
        v.push_back(x);

        vSum.push_back(t_sum);

    }


    int minI = 0;
    int minJ = 0;

    int minDiff = 2147483647;

    for (int i = 0; i < n; i++) {

        if (abs(vSum[i] - k) < minDiff) {
            minDiff = abs(vSum[i] - k);
            minI = 0;
            minJ = i;
        }
    }

    for (int i = 0; i < n; i++) {

        vector<int>::iterator low = lower_bound(vSum.begin(), vSum.end(), vSum[i] + k);
        int up = low - vSum.begin();
        int down = up - 1;


        int sum = vSum[down] - vSum[i];

        int diff = abs(k - sum);

        if (diff < minDiff) {
            minDiff = diff;
            minI = i + 1;
            minJ = down;
        } // lexicographically ordered visit, so doesn't matter


        sum = vSum[up] - vSum[i];

        diff = abs(k - sum);

        if (diff < minDiff) {
            minDiff = diff;
            minI = i + 1;
            minJ = up;
        } // lexicographically ordered visit, so doesn't matter


    }

    cout << minI << " " << minJ << endl;

}


int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}