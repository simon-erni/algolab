#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

// BGL Graph definitions
// ===================== 
// Graph Type with nested interior edge properties for Cost Flow Algorithms
typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
    boost::property<boost::edge_capacity_t, long,
        boost::property<boost::edge_residual_capacity_t, long,
            boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
                boost::property <boost::edge_weight_t, long> > > > > Graph; // new!
// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type      EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type       ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor          Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map, weight map and reverse edge map
// ===============================================================
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long cap, long cost) {
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = cap;
        weightmap[e] = cost; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = -cost; // new
        revedgemap[e] = rev_e; 
        revedgemap[rev_e] = e; 
    }
};

using namespace std;

int n,m,s;

int byToI(int i) {
    return i + 1;
}

int siToI(int i) {

    return i + n + 1;
}

int stToI(int i) {

    return i + n + m + 1;
}

int moneyToCost(int m) {

    int c = 100 - m;

    assert(c >= 0);

    return c;
}

int costToMoney(int cost) {

    int m = 100 - cost;

    assert(m >= 0);
    
    return m;
}
void testcase() {

    cin >> n >> m >> s;

    Graph G(n + m + s + 2);

    int source = 0;
    int target = n + m + s + 1;

    EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
    EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
    ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
    ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
    EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);

    for (int i = 0; i < s; i++) {
        int c;
        cin >> c;
        eaG.addEdge(stToI(i), target, c, 0);

    }


    for (int i = 0; i < m; i++) {
        int state;
        cin >> state;
        state--;
        eaG.addEdge(siToI(i), stToI(state), 1, 0);

    }

    for (int i = 0; i < n; i++) {
        eaG.addEdge(source, byToI(i), 1, 0);
        for (int j = 0; j < m; j++) {

            int amount;
            cin >> amount;

            eaG.addEdge(byToI(i), siToI(j), 1, moneyToCost(amount));
        }

    }

    //int flow = boost::push_relabel_max_flow(G, source, target);
    //boost::cycle_canceling(G);
    //int cost = boost::find_flow_cost(G);

    //cout << flow << " " << -cost << endl;

    boost::successive_shortest_path_nonnegative_weights(G, source, target);
    int cost = 0;
    int flow = 0;
    // Iterate over all edges leaving the source to sum up the flow values.
    OutEdgeIt e, eend;
    for(boost::tie(e, eend) = boost::out_edges(boost::vertex(source,G), G); e != eend; ++e) {
        int f = capacitymap[*e] - rescapacitymap[*e];
        flow += f;
        
    }

    for (int i = 0; i < n; i++) { 
        for(boost::tie(e, eend) = boost::out_edges(boost::vertex(byToI(i),G), G); e != eend; ++e) {
            int f = capacitymap[*e] - rescapacitymap[*e];
            if (f < 0) {
                continue;
            }
            cost += f * moneyToCost(weightmap[*e]);
            
        }
    }

    cout << flow << " " << cost << endl;


    


}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testcase();
    }
}
