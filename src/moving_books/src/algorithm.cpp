#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <cassert>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
using namespace std;


#define debug if (debuge) cerr
const bool debuge = false;

// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;


// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	Edge addEdge(int from, int to, long capacity) {
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
		return e;
	}
};


int friendT(int f) {
	return f + 2;
}

EdgeCapacityMap capacitymap;
Graph G;
vector<Edge> limitE;
int start, ziel;
int n,m;
bool possible(int limit) {

	debug << "trying " << limit << " ... ";

	for (Edge e : limitE) {
		capacitymap[e] = limit;
	}
	
	long flow = boost::push_relabel_max_flow(G, start, ziel);
	
	if (flow >= m) {
		debug << " works! " << endl;
		return true;
	} else {
		debug << " doesn't work" << endl;	
		return false;
	}

}

void testcase() {
	
	
	cin >> n >> m;
	
	debug << "###### " << endl;
	debug << "n: " << n << " m: " << m << endl;
	debug << "###### " << endl;
	
	G = Graph(n + 3);
	
	start = 0;
	int source2 = 1;
	ziel = 2 * n + 1;
	
	capacitymap = boost::get(boost::edge_capacity, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);
	
	eaG.addEdge(start, source2, m);

	
	vector<int> s(n);
	int maxS = 0;
	for (int i = 0; i < n; i++) {
		cin >> s[i];
	}
	sort(s.begin(), s.end()); 
	
	vector<int> b(m);
	for (int i = 0; i < m; i++) {
		cin >> b[i];
	}
	sort(b.begin(), b.end());
	
	if (s[n - 1] < b[m - 1]) {
		cout << "impossible" << endl;
		return;
	}

	
	limitE = vector<Edge>();
	limitE.reserve(n);
	int last = 0;
	for (int i = 0; i < n; i++) {
		//How many can he carry?
		int cancarry = distance(b.begin(), upper_bound(b.begin(), b.end(), s[i]));
		
		assert(cancarry >= last);
		int cap = cancarry - last;
		
		last = cancarry;
		
		eaG.addEdge(source2, friendT(i), cap);
		
		limitE.push_back(eaG.addEdge(friendT(i), ziel, 0));
		
		if (i > 0) {
			eaG.addEdge(friendT(i - 1), friendT(i), m);
		}
		
	}
	
	int l = m/n;
	if (m % n != 0) {
		l++;
	} //Round up
	int r = m;
	
	debug << "l: " << l << " r: " << r << endl;
	
	while (l < r) {
		debug << "l: " << l << " r: " << r << endl;

	
		if (r - l == 1) {
			if (possible(l)) {
				r = l;
			} else {
				l = r;
			}
			
			break;
		}
		
		int m = (r - l) / 2 + l;
		
		if (possible(m)) {
			r = m;
		} else {
			l = m + 1;
		}
	}
	
	cout << l*3 - 1 << endl;
	

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
	    testcase();
    }
}
