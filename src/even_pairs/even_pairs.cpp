#include <iostream>
#include <vector>

using namespace std;


void testCase() {

    int n;
    cin >> n;


    int countEven = 0;
    int countOdd = 0;

    int sum = 0;

    for (int i = 0; i < n; i++) {

        int k;
        cin >> k;

        sum += k;

        if (sum % 2 == 0) {
            countEven++;
        } else {
            countOdd++;
        }

    }

    int numPairs = 0;

    numPairs += (countEven) * (countEven - 1) / 2;
    numPairs += (countOdd) * (countOdd - 1) / 2;

    numPairs += countEven;

    cout << numPairs << endl;

}


int main() {

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}