#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


void testCase() {

    int n;
    cin >> n;

    vector<int> a;
    for (int i = 0; i < n; i++) {

        int x;
        cin >> x;


        a.push_back(x);

    }

    int x;
    cin >> x;
    if (x == 0) {
        sort(a.begin(), a.end());
    } else {
        sort(a.begin(), a.end(), greater<int>());
    }

    for (int i = 0; i < n; i++) {
        cout << a[i] << " ";
    }

    cout << endl;

}


int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}