#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <utility>
#include <stdexcept>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_2<K> Vb; 
typedef CGAL::Triangulation_face_base_with_info_2<long,K> Fb; 
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds; 
typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation;

typedef Triangulation::Edge_iterator  Edge_iterator;
typedef Triangulation::Face_iterator  Face_iterator;

typedef Triangulation::Vertex_handle Vertex;
typedef Triangulation::Face_handle Face;
typedef K::Point_2 Point;
typedef K::FT	FT;

using namespace std;

#define debug if (debuge) cerr
const bool debuge = false;

FT maxVal(const FT &v1, const FT &v2) {
	if (v1 >= v2) {
		return v1;
	}
	return v2;
}

FT minVal(const FT &v1, const FT &v2) {
	if (v1 <= v2) {
		return v1;
	}
	return v2;
}

bool findPath(const vector<FT> &maxEscape, const Triangulation &t, const Point &p, const long &d) {
	
	Point nearest = t.nearest_vertex(p)->point();
	if (CGAL::squared_distance(p, nearest) < d) {
		return false;
	}
	Face f = t.locate(p);
	if (t.is_infinite(f)) {
		return CGAL::squared_distance(p, nearest) >= d;
	}
	
	
	return maxEscape[f->info()] >= d;
}

void testcase(long n) {

	std::vector<Point> pts(n);
	
	for (long i = 0; i < n; ++i) {
		std::cin >> pts[i];
	}
	
	Triangulation t;
	t.insert(pts.begin(), pts.end());

	long k = 0;
	for (Face_iterator f = t.faces_begin(); f != t.faces_end(); f++) {
		f->info() = k;
		k++;
	}
	vector<FT> maxR(k);
	vector<FT> maxEscape(k, FT(0));
	vector<vector<pair<long,FT>>> G(k);
	for (Face_iterator f = t.faces_begin(); f != t.faces_end(); f++) {
		long u = f->info();
		debug << "Face " << u << ": " << f->vertex(0)->point() << ", " << f->vertex(1)->point() << ", "<< f->vertex(2)->point() << " neighbors: ";
		maxR[u] = K::Circle_2(f->vertex(0)->point(), f->vertex(1)->point(), f->vertex(2)->point()).squared_radius();
		for (int v = 0; v < 3; v++) {
			FT dist = t.segment(f, v).squared_length() / 4;
			debug << f->neighbor(v)->info();

			if (t.is_infinite(f->neighbor(v))) {
				maxEscape[u] = maxVal(maxEscape[u], dist);
				debug << " (inf)";
			} else {
				G[u].push_back(make_pair(f->neighbor(v)->info(), dist));
			}
			debug << ", ";
		}
		debug << endl;
	}
	vector<bool> changed(k, true);
	long changedCount = 1;
	while (changedCount > 0) {
		changedCount = 0;
		for (int u = 0; u < k; u++) {
			if (!changed[u]) {
				continue;
			}
			changed[u] = false;
			for (pair<long,FT> v : G[u]) {
				FT newVal = minVal(maxR[v.first], minVal(maxEscape[u], v.second));
				if (maxEscape[v.first] < newVal) {
					maxEscape[v.first] = newVal;
					changed[v.first] = true;
					changedCount++;
				}
			}
		}
	}
	for (int u = 0; u < k; u++) {
		debug << "Face " << u << " maxEscape: " << maxEscape[u] << endl;
	}

	int m;
	cin >> m;
	
	for (int i = 0; i < m; i++) {
		Point p;
		long d;
		
		cin >> p >> d;
		
		if (findPath(maxEscape, t, p, d)) {
			cout << "y";
		} else {
			cout << "n";
		}
	}
	
	cout << endl;

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    long n;
    cin >> n;
    
    while (n > 0) {
    	testcase(n);
    	cin >> n;
    }
}
