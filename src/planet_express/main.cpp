#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <set>
#include <cassert>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/strong_components.hpp>


typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::property<boost::edge_weight_t, int> 		// interior properties of edges
		>					Graph;

typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef boost::property_map<Graph, boost::edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t

using namespace std;
using namespace boost;

bool debug = false;

void testcase() {

	int n,m,k,T;
	cin >> n >> m >> k >> T;
	
	vector<int> t;
	for (int i = 0; i < T; i++) {
		int j;
		cin >> j;
		t.push_back(j);
	}
		
	Graph G(n);
	WeightMap weightmap = boost::get(boost::edge_weight, G);	// start by defining property maps for all interior properties defined in Lines 37, 38

	for (int i = 0; i < m; i++) {
		int u,v,c;
		cin >> u >> v >> c;
		
		Edge e;
		bool success;
		
		tie(e,success) = add_edge(v,u,G);
		weightmap[e] = c;
	}
	
	std::vector<int> componentmap(n);
	int ncc = boost::strong_components(G, boost::make_iterator_property_map(componentmap.begin(), boost::get(boost::vertex_index, G)));
	
	vector<vector<int>> cmap(ncc);
	
	for (int i = 0; i < T; i++) {

		cmap[componentmap[t[i]]].push_back(t[i]);

	} 
	
	for (int i = 0; i < ncc; i++) {
	
		Vertex v = add_vertex(G);
		for (int u = 0; u < cmap[i].size(); u++) {
			
			Edge e;
			bool success;
		
			tie(e,success) = add_edge(cmap[i][u],v,G);
			weightmap[e] = cmap[i].size() - 1;

			
			add_edge(v,cmap[i][u],G);
			//weightmap[e] = 0;

			
			
		}
		
	} 
	
	int min = INT_MAX;
	
	Vertex start = n - 1;
		
	std::vector<Vertex> predmap(n + ncc);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
	std::vector<int> distmap(n + ncc);
		
	boost::dijkstra_shortest_paths(G, start, // We MUST provide at least one of the two maps
	boost::predecessor_map(boost::make_iterator_property_map(predmap.begin(), boost::get(boost::vertex_index, G))).	// predecessor map as Named Parameter
	distance_map(boost::make_iterator_property_map(distmap.begin(), boost::get(boost::vertex_index, G))));	// distance map as Named Parameter

	for (int i = 0; i < k; i++) {				
		if (distmap[i] < min) {
			min = distmap[i];
		}
	}
		
	if (min <= 1000000) {
		cout << min << endl;
	} else {
		cout << "no" << endl;
	}

}


int main() {
	
	ios_base::sync_with_stdio(false);
	
	int t;
	
	cin >> t;
	
	for (int i = 0; i < t; i++) {
		testcase();
	}

}