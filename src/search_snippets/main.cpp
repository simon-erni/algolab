#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <limits>
#include <cmath>

using namespace std;

void testCase() {

    int n;
    cin >> n;

    vector<int> count;


    for (int i = 0; i < n; i++) {

        int x;
        cin >> x;

        count.push_back(x);
    }

    vector<vector<int>> pos(n);
    vector<pair<int, int>> all; //position, element idx

    vector<int> p(n, 0);

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < count[i]; j++) {
            int position;
            cin >> position;

            pos[i].push_back(position);
            all.push_back(make_pair(position, i));

        }

        // sort(pos[i].begin(), pos[i].end()); ---> unnecessary, as the input is already sorted
    }

    sort(all.begin(), all.end());

    int minDistance = 2147483647;


    int maxPos = 0;

    for (int i = 0; i < n; i++) {
        if (pos[i][0] > maxPos) {
            maxPos = pos[i][0];
        }
    }


    for (int i = 0; i < all.size(); i++) {

        int posCurrent = all[i].first;
        int elCurrent = all[i].second;

        if (maxPos - posCurrent + 1 < minDistance) {
            minDistance = maxPos - posCurrent + 1;
        }

        p[elCurrent]++;

        if (p[elCurrent] >= pos[elCurrent].size()) {
            break;
        }

        if (pos[elCurrent][p[elCurrent]] > maxPos) {
            maxPos = pos[elCurrent][p[elCurrent]];
        }

    }

    cout << minDistance + 1 << endl;


}

int main() {

    ios_base::sync_with_stdio(false);

    int t;

    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

}