#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <cassert>

using namespace std;

vector<bool> pattern;
vector<bool> original;
vector<pair<long,long>> dp;

int dist(int group) {
    int start = group * pattern.size();
    int d = 0;
    for (int i = 0; i < pattern.size(); i++) {
        if (original[start + i] != pattern[i]) {
            d++;
        }
    }
    return d;
}


void testcase() {

    int n,k,x;
    cin >> n >> k >> x;
    pattern.clear();
    pattern.resize(k);
    for (int i = 0; i < k; i++) {
        if (x & 1 << i) {
            pattern[k - i - 1] = true;
        }
    }

    //DP
    dp.clear();
    dp.resize(n/k);

    original.clear();
    original.resize(n);

    for (int i = 0; i < n; i++ ) {
        int x;
        cin >> x;
        original[i] = x;
    }

    //Init 0-> 1'st group, 0 == non inversed
    int d0 = dist(0);
    dp[0].first = std::min(d0, k - d0 + 1);
    dp[0].second = std::min(k - d0, d0 + 1);

    for (int group = 1; group < n / k; group++) {
        int d = dist(group);
        dp[group].first = std::min(dp[group - 1].first + d,     dp[group - 1].second + 1 + (k - d));
        dp[group].second = std::min(dp[group - 1].second + k - d, dp[group - 1].first + 1 + (d));
    }

    cout << min(dp[(n/k) - 1].first , dp[(n/k) - 1].second+1)  << endl;

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        //debuge = i == 2;
        testcase();
    }
}
