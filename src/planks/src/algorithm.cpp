#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <cassert>
#include <stdexcept>
#include <map>

using namespace std;
void testcase() {

    int n;
    cin >> n;
    vector<int> p;
    int total = 0;
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        p.push_back(x);
        total += x;
    }

    if ((total / 4) * 4 != total) {
        cout << "0" << endl;
        return;
    }

    int side = total / 4;

    for (int i = 0; i < n; i++) {
        if (p[i] > side) {
            cout << "0" << endl;
            return;
        }
    }

    vector<int> oneside;

    for (int s = 0; s < 1<<n; ++s) { // Iterate through all subsets 
        int sum = 0;
        for (int i = 0; i < n; ++i) { 
            if (s & 1<<i) sum += p[i]; // If i-th element in subset 
        }
        if (sum == side) {
            oneside.push_back(s);
        } 
    }

    cerr << "oneside: " << oneside.size() << endl;
    if (oneside.size() == 0) {
        cout << "0" << endl;
        return;
    }
    
    map<int,int> twosides;

    for (int i = 0; i< oneside.size() - 1; i++) {
        for (int j = i + 1; j< oneside.size(); j++ ) {
            if ((oneside[i] & oneside[j]) == 0 ) { //two sides don't clash
                int two = oneside[i] | oneside[j];
                auto it = twosides.find(two);
                if (it != twosides.end()) {
                    it->second++;
                } else {
                    twosides.insert(make_pair(two, 1));
                }
            }
        }
    }
  /*  vector<pair<int,int>> two;
    two.reserve(twosides.size());
    for (auto it = twosides.begin(); it != twosides.end(); it++) {
        two.push_back(*it);
    }*/

    cerr << "twosides: "<< twosides.size() << "done" << endl;
    int t = 0;
    for (auto i = twosides.begin(); i != twosides.end(); i++) {
        
        int s = i->first;
        int inverse = 0;
        for (int j = 0; j < n; ++j) { 
            if (!(s & 1<<j)) {
                inverse |= 1<<j;
            }
        }
        auto it = twosides.find(inverse);
        if (it != twosides.end()) {
            t += it->second * i->second;
        }
        
    }

    cout << t / 6<< endl;



}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
