#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <utility>

// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>


using namespace std;
using namespace boost;


// Directed graph with integer weights on edges.
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef adjacency_list<vecS, vecS, undirectedS,
		no_property,
		property<edge_weight_t, int>
		>					Graph;
typedef graph_traits<Graph>::vertex_descriptor		Vertex;	// Vertex type		
typedef graph_traits<Graph>::edge_descriptor		Edge;	// Edge type
typedef graph_traits<Graph>::edge_iterator		EdgeIt;	// Edge iterator
// Property map edge -> weight
typedef property_map<Graph, edge_weight_t>::type	WeightMap;

void testcases() {
	
	int n,m,s,a,b;

	cin >> n >> m >> s >> a >> b;

	vector<Graph> G(s, Graph(n));
	
	Graph GT(n);
	
	
	vector<WeightMap> weightmap;
	WeightMap w;
	w = get(edge_weight, GT);
	
		
	vector<vector<int>> temp(n, vector<int>(n, 0));

	for (int i = 0; i < s; i++) {
		weightmap.push_back(get(edge_weight, G[i]));
	}
	
	for (int i = 0; i < m; ++i) {
		int u, v;		// Each edge: <from> <to> <weight>
		cin >> u >> v;
		
		Edge e;	bool success;	
		tie(e, success) = boost::add_edge(u,v,GT);
		w[e] = 2147483647;
		
		for (int k = 0; k < s; k++) {
			
			int weight;
			cin >> weight;

			
			Edge e;	bool success;					// *** We swap u and v to create ***
			tie(e, success) = add_edge(u, v, G[k]);	// *** the reversed graph GT!   ***
			weightmap[i][e] = weight;
			
		}
	}
	
	
	for (int i = 0; i < s; i++) {
		int x;
		cin >> x;
		
		vector<Edge> mst;
		boost::kruskal_minimum_spanning_tree(G[i], std::back_inserter(mst)); 
		
		std::vector<Edge>::iterator ebeg, eend = mst.end(); 
		
		for (int k = 0; k < mst.size(); k++) { 
		
			int u = source(mst[k], G[i]);
			int v = target(mst[k], G[i]);
			
			Edge e = edge(u,v,GT).first;

			if (w[e] > weightmap[i][mst[k]]) {
				w[e] = weightmap[i][mst[k]];
			}
		}
		
	}
	/*
	EdgeIt eit, eend; 
	for (tie(eit, eend) = edges(GT); eit != eend; ++eit) {
		cerr << "Edge from " << source(*eit, GT) << " to " << target(*eit, GT) << " weight " << w[*eit] << endl;
	}*/
	
	vector<int> distmap(n);
	vector<Vertex> predmap(n);	// vectors as an exterior property map.

	
	dijkstra_shortest_paths(GT, a,
		predecessor_map(make_iterator_property_map(	// named parameters
			predmap.begin(), get(vertex_index, GT))).
		distance_map(make_iterator_property_map(	// concatenated by .
			distmap.begin(), get(vertex_index, GT))));

		
	int length = distmap[b];
	
	cout << length << "\n";
	
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false);
	int t;
	cin >> t;

	for (int i = 0; i < t; i++) {
		testcases();

	}
}