#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>

#define trace(x) cerr << #x << ": " << x << endl

// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::property<boost::edge_weight_t, int> 		// interior properties of edges
		>					Graph;
typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef boost::property_map<Graph, boost::edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t


using namespace std;

void print(const vector<int> &v) {
    for (int i : v) {
        cerr << i << " ";
    }
    cerr << endl;
}

void print(const vector<bool> &v) {
    for (bool i : v) {
        cerr << i << " ";
    }
    cerr << endl;
}

void testcase() {
    int n,m;
    cin >> n >> m;

    Graph G(n);
    vector<int> cost(n);
    for(int i = 0; i < m; i++) {
        int u,v;
        cin >> u >> v;
        u--;
        v--;
        boost::add_edge(u,v,G);
    }
    for (int i = 0; i < n; i++) {
        cin >> cost[i];
    }

    std::vector<int> componentmap(n);	// We MUST use such a vector as an Exterior Property Map: Vertex -> Component
	int ncc = boost::strong_components(G, boost::make_iterator_property_map(componentmap.begin(), boost::get(boost::vertex_index, G)));

    trace(ncc);
    cerr << "componentmap: " << endl;
    print(componentmap);

    vector<int> minCost(ncc, INT_MAX);
    for (int i = 0; i < n; i++) {
        minCost[componentmap[i]] = std::min(minCost[componentmap[i]], cost[i]);
    }

    cerr << "minCost: " << endl;
    print(minCost);

    EdgeIt ebeg, eend;
    vector<bool> roots(ncc, true);
	for (boost::tie(ebeg, eend) = boost::edges(G); ebeg != eend; ++ebeg) {
        Vertex u = source(*ebeg, G);
        Vertex v = target(*ebeg, G);

        if (componentmap[u] != componentmap[v]) {
            roots[componentmap[v]] = false;
        }
    }
    cerr << "roots:" << endl;
    print(roots);

    long sum = 0;
    for (int i = 0; i < ncc; i++) {
        if (roots[i]) {
            sum += minCost[i];
        }
    }

    cout << sum << endl;

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
