#include <iostream>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Ray_2.h>
#include <CGAL/Point_2.h>
#include <CGAL/Segment_2.h>

typedef CGAL::Exact_predicates_exact_constructions_kernel KE;
typedef CGAL::Exact_predicates_inexact_constructions_kernel KI;


typedef KI::Point_2 PointI;
typedef KI::Ray_2 RayI;
typedef KI::Segment_2 SegmentI;


typedef KE::Point_2 PointE;
typedef KE::Ray_2 RayE;
typedef KE::Segment_2 SegmentE;



double floor_to_double(const KE::FT& x) {
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

void testCase(long n) {

    long x, y, a, b;

    std::cin >> x;
    std::cin >> y;
    std::cin >> a;
    std::cin >> b;

    PointE startE(x,y);

    RayI ray(PointI(x,y), PointI(a,b));
	RayE rayE(PointE(x,y), PointE(a,b));

	bool foundFirst = false;
	KE::FT minDistance;
	PointE minPoint;

    for (int i = 0; i < n; i++) {
        long r,s,t,u;

        std::cin >> r;
        std::cin >> s;
        std::cin >> t;
    	std::cin >> u;

		SegmentI segment(PointI(r,s), PointI(t,u));

		if (CGAL::do_intersect(segment,ray)) { // Predicate, ok if inexact
			
			SegmentE segmentE(PointE(r,s), PointE(t,u));
			
			auto o = CGAL::intersection(rayE,segmentE); //Construction! Exact
			if (const PointE* point = boost::get<PointE>(&*o)) {

				KE::FT distance = CGAL::squared_distance(*point,startE);

				if (!foundFirst || distance < minDistance) {
					minDistance = distance;
					foundFirst = true;
					minPoint = *point;
				}

			} else {
			    const SegmentE* seg = boost::get<SegmentE>(&*o);
			    
			    PointE source = seg->source();
			    PointE target = seg->target();

                KE::FT distance = CGAL::squared_distance(source,startE);

                if (!foundFirst || distance < minDistance) {
                    minDistance = distance;
                    foundFirst = true;
                    minPoint = source;
                }
                
                distance = CGAL::squared_distance(target,startE);

                if (!foundFirst || distance < minDistance) {
                    minDistance = distance;
                    foundFirst = true;
                    minPoint = target;
                }
			}
		}
    }



	if (!foundFirst) {
		std::cout << "no" << std::endl;
	} else {
		std::cout << std::fixed << std::setprecision(0) << floor_to_double(minPoint.x()) << " " << floor_to_double(minPoint.y()) << std::endl;
	}


    

}


int main() {
	
	std::ios_base::sync_with_stdio(false);

    long n;

    std::cin >> n;

    while (n > 0) {
        testCase(n);
        std::cin >> n;
    }


}
