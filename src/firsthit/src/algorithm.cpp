#include <iostream>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>

#include <CGAL/Ray_2.h>
#include <CGAL/Point_2.h>
#include <CGAL/Segment_2.h>

typedef CGAL::Exact_predicates_exact_constructions_kernel K;

typedef K::Point_2 P;
typedef K::Ray_2 R;
typedef K::Segment_2 S;
typedef K::FT FT;

#define trace(x) if (tracee) cerr << #x << ": " << x << endl

const bool tracee = true;

double floor_to_double(const K::FT& x) {
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

void minPoint(const R &ray, const S &segment, S &segmentToUpdate) {

	auto o = CGAL::intersection(ray, segment);
	if (const P* op = boost::get<P>(&*o)) {
		segmentToUpdate = S(ray.source(), *op);
	} else  if (const S* os = boost::get<S>(&*o)) {

		if (CGAL::squared_distance(ray.source(), os->source()) < CGAL::squared_distance(ray.source(), os->target())) {
			segmentToUpdate = S(ray.source(), os->source());
		} else {
			segmentToUpdate = S(ray.source(), os->target());
		}		 	
	}
}

using namespace std;
vector<S> segments;
void testCase(long n) {
	
	long a,b,c,d;

	cin >> a >> b >> c >> d;

	P r1(a,b);
	P r2(c,d);

    R ray(r1,r2);
    S seg;
    
    
    P point;

	bool foundFirst = false;
		
	segments.resize(n);

    for (int i = 0; i < n; i++) {

		
		long a1,a2,a3,a4;
		cin >> a1 >> a2 >> a3 >> a4;
		
		P p1(a1,a2);
		P p2(a3,a4);
		
		segments[i] = S(p1, p2);
	}
	
	random_shuffle(segments.begin(), segments.end());
	
	int k = 0;
	for (; k < n; k++) {
	
		if (!CGAL::do_intersect(segments[k],ray)) {
			continue;
		}
		
		minPoint(ray, segments[k], seg);
		foundFirst = true;
		break;
	}
	
	if (!foundFirst) {
		std::cout << "no" << std::endl;
		return;
	}
	
	for (int i = k + 1; i < n; i++) {
		
		if (!CGAL::do_intersect(segments[i],seg)) {
			continue;
		}
		
		minPoint(ray ,segments[i], seg);
		
	}

	std::cout << floor_to_double(seg.target().x()) << " " << floor_to_double(seg.target().y()) << "\n";
	

}


int main() {
	
	std::ios_base::sync_with_stdio(false);

    long n;

    std::cin >> n;
    
    std::cout << std::fixed << std::setprecision(0);

    while (n > 0) {
        testCase(n);
        std::cin >> n;
    }


}