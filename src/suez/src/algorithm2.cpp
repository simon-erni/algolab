#include <iomanip>
#include <iostream>
#include <vector>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpq.h>

typedef CGAL::Gmpq ET;
typedef CGAL::Quadratic_program<ET> program_t;
typedef CGAL::Quadratic_program_solution<ET> solution_t;
typedef CGAL::Quotient<ET> objective_t;

double ceil_to_double( objective_t const & o )
{
double d = std::ceil( CGAL::to_double( o ) );
while( d < o ) d += 1;
while( d-1 >= o ) d -= 1;
return d;
}

struct coord
 {
 int x, y;
 };

void test_case()
{
// Read Input
int n, m, h, w;
std::cin >> n >> m >> h >> w;
std::vector<coord> red_rects( n );
std::vector<coord> blue_rects( m );
for( int i = 0; i < n; ++i )
{
std::cin >> red_rects[i].x >> red_rects[i].y;
}
for( int i = 0; i < m; ++i )
{
std::cin >> blue_rects[i].x >> blue_rects[i].y;
}


// Set Up Linear Program
program_t lp( CGAL::SMALLER, true, 1, false, 0 );

// Objective Function
 for( int i = 0; i < n; ++i )
 {
 lp.set_c( i, -2 * (w + h) );
 }

// RED-RED Constraints
int ids = 0;
for( int i = 0; i < n; ++i )
{
for( int j = i+1; j < n; ++j )
{
int id = ids++;
ET dx = std::abs( red_rects[i].x - red_rects[j].x );
ET dy = std::abs( red_rects[i].y - red_rects[j].y );
lp.set_a( i, id, 1 );
lp.set_a( j, id, 1 );
lp.set_b( id, 2 * std::max( dx/w, dy/h ) );
 }
 }

 // RED-BLUE Constraints
 for( int i = 0; i < n; ++i )
 {
 ET limit = 33554432; // safe because from Section 4 we know that a_i < 2^25
 for( int j = 0; j < m; ++j )
 {
 ET dx = std::abs( red_rects[i].x - blue_rects[j].x );
 ET dy = std::abs( red_rects[i].y - blue_rects[j].y );
 ET current_limit = 2 * std::max( dx/w, dy/h ) - 1;
 limit = std::min( limit, current_limit );
 }
 int id = ids++;
 lp.set_a( i, id, 1 );
 lp.set_b( id, limit );
 }
 // Call Solver
 solution_t s = CGAL::solve_linear_program( lp, ET() );
 std::cout
 << std::setprecision( 0 ) << std::fixed
 << ceil_to_double( -s.objective_value() ) << "\n";
 }

 int main()
 {
 std::ios::sync_with_stdio( false );
 int t; std::cin >> t;
 while( t-- ) test_case();
 }