#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <utility>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpq.h>

using namespace std;

// choose exact integral type
typedef CGAL::Gmpq ET;
typedef CGAL::Quotient<ET> objective_t;

// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

double ceil_to_double(objective_t const & o) {
    double d = ceil(CGAL::to_double(o));
    while (d < o) d++;
    while (d - 1 >= o) d--;
    
    return d;

}

void testcase() {

    int n,m;
    ET h,w;
    cin >> n >> m >> h >> w;
    
    vector<pair<ET,ET>> free;
    free.reserve(n);

    vector<pair<ET,ET>> occ;
    occ.reserve(m);

    for (int i = 0; i < n; i++) {
        int x,y;
        cin >> x >> y;
        free.push_back(make_pair(x,y));
    }

    for (int i = 0; i < m; i++) {
        int x,y;
        cin >> x >> y;
        occ.push_back(make_pair(x,y));
    }

    Program lp (CGAL::SMALLER, true, 1, false, 0);

    //Variables: 0 - (n-1) for each a
    //Constraints: just ++

    int c = 0;

    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {

            lp.set_a(i,c,ET(1));
            lp.set_a(j,c,ET(1));
            lp.set_b(c, ET(2)*  CGAL::max(CGAL::abs(free[i].first - free[j].first)/w, CGAL::abs(free[i].second - free[j].second)/h));

            c++;
        }
    }


    if (m >= 1) { 
        for (int i = 0; i < n; i++) {

            ET min = ET(2)*CGAL::max(CGAL::abs(free[i].first - occ[0].first)/w, CGAL::abs(free[i].second - occ[0].second)/h) - ET(1);

            for (int j = 1; j < m; j++) {

                ET newMin = ET(2)*CGAL::max(CGAL::abs(free[i].first - occ[j].first)/w, CGAL::abs(free[i].second - occ[j].second)/h) - ET(1);
                
                if (newMin < min) {
                    min = newMin;
                } 
            }

            lp.set_a(i,c,ET(1));
            lp.set_b(c, min);

            c++;
        }
    }

    for (int i = 0; i < n; i++) {

        lp.set_c(i, ET(-2)*(h+w));
    }

    // solve the program, using ET as the exact type
    Solution s = CGAL::solve_linear_program(lp, ET());
    assert(s.solves_linear_program(lp));
    assert(s.is_optimal());


    cout << setprecision(0) << fixed << ceil_to_double(-s.objective_value()) << endl;

    //cerr << s << endl;

    //cerr << "#####" << endl;
    

}


int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;

    cin >> t;

    for (int i = 0; i < t; i++) {
        testcase();

    }


}
