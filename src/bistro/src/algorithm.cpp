#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>

#include <CGAL/Delaunay_triangulation_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt KE;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Finite_faces_iterator  Face_iterator;
typedef Triangulation::Edge_iterator  Edge_iterator;

using namespace std;

void testcase(int n) {

  	std::vector<K::Point_2> pts;

  	pts.reserve(n);
  	
  	for (std::size_t i = 0; i < n; ++i) {
		K::Point_2 p;
		std::cin >> p;
    	pts.push_back(p);
	}

	Triangulation t;
	t.insert(pts.begin(), pts.end());
	
	int m;
	cin >> m;
	
	cout << fixed;
	for (int i = 0; i < m ; i++) {
		K::Point_2 p;
		std::cin >> p;
		long result = CGAL::squared_distance(p, t.nearest_vertex(p)->point());
		cout << result << endl;
	}
  


}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    int n;
    cin >> n;
    
    while (n > 0) {
    	testcase(n);
    	cin >> n;
    }
    
    
    
}
