#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

using namespace std;

#define debug if (debuge) cerr
const bool debuge = true;

#define trace(x) debug << #x << " = " << x << endl

typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
    boost::property<boost::edge_capacity_t, long,
        boost::property<boost::edge_residual_capacity_t, long,
            boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
                boost::property <boost::edge_weight_t, long> > > > > Graph; // new!
// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type      EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type       ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor          Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator


class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long c, long w) {
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = c;
        weightmap[e] = w; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = -w; // new
        revedgemap[e] = rev_e; 
        revedgemap[rev_e] = e; 
        
        debug << "added edge (" << u << "," << v << ") c: " << c << " w: " << w << endl;
    }
};


void testcase() {
	long n,m;
	cin >> n >> m;
	
	trace(n);
	trace(m);
	
	vector<pair<long,long>> c(n);
	vector<pair<long,long>> pos(n);
	set<int> sS;
	
	map<long,long> ma;
	
	for (int i = 0; i < n; i++) {
		long a,b;
		cin >> a >> b;
		sS.insert(a);
		sS.insert(b);
		
		ma[a]++;
		ma[b]--;
		
		if (a>b) {
			ma[0]++;
		}
		
		c[i] = make_pair(a,b);
	}
	
	
	int minK = n;
	int k = 0;
	int minPlace = 0;
	for (auto it = ma.begin(); it != ma.end(); it++) {
		k += it->second;
		if (k <= 10 && k < minK) {
			minK = k;
			minPlace = it->first;
		}
	}
	
	vector<pair<long,long>> conflicting;
	for (int i = 0; i < n; i++) {
		
		if (c[i].first <= minPlace && c[i].second >= minPlace) {
			conflicting.push_back(c[i]);
		}
	}
	
	vector<int> s;
	s.reserve(sS.size());
	for (int k : sS) {
		s.push_back(k);
		debug << k << " ";
	}
	debug << endl;
	Graph G(s.size() + 2);
	
	int source = s.size();
	int target = s.size() + 1;
	
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
    EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
    ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
    ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
    EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);
		
	for (int i = 0; i < s.size() - 1; i++) {
		eaG.addEdge(i, i + 1, 1, s[i + 1] - s[i]);
	}
	
	eaG.addEdge(s.size() - 1, 0, 1, m - s[s.size() - 1] + s[0]); // Over the edge
	
	for (int i = 0; i < n; i++) {
	
		trace(c[i].first);
		trace(c[i].second);
	
		int start = distance(s.begin(), lower_bound(s.begin(), s.end(), c[i].first));
		int end = distance(s.begin(), lower_bound(s.begin(), s.end(), c[i].second));
		
		end++;
		
		end = end % s.size();
		
		long cost = 0;
		if (c[i].first <= c[i].second) {
			cost = c[i].second - c[i].first;
		} else {
			cost = m - c[i].first + c[i].second;
		}

		
		
		
		eaG.addEdge(start, end, 1, cost);
	}
	
	int iMin = distance(s.begin(), lower_bound(s.begin(), s.end(), minPlace));
	
	trace(iMin);
	
	long minCost = LONG_MAX;
	
	for (pair<long,long> path : conflicting) {
		
		int start = distance(s.begin(), lower_bound(s.begin(), s.end(), path.first));
		int end = distance(s.begin(), lower_bound(s.begin(), s.end(), path.second));
		
		end++;
		
		end = end % s.size();
		
		eaG.addEdge(start, target, 1, 0);
		eaG.addEdge(source, end, 1,0);
		
		trace(start);
		trace(end);
		boost::successive_shortest_path_nonnegative_weights(G, source, target);
		
		minCost = std::min(minCost, boost::find_flow_cost(G));
		
		trace(minCost);
		
		boost::remove_edge(start, target, G);
		boost::remove_edge(source, end, G);
		
	}
	
	eaG.addEdge(source, iMin, 1, 0);
	
	if (iMin - 1 == -1) {
		iMin = s.size();
	}
	eaG.addEdge(iMin - 1, target, 1, 0);
	
	boost::successive_shortest_path_nonnegative_weights(G, source, target);
		
	minCost = std::min(minCost, boost::find_flow_cost(G));
		
	cerr << m - 1 - minCost << endl;
	
	
	
	
		
	
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
    	testcase();
    }
}
