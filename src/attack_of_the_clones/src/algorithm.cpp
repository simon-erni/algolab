#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <map>
#include <utility>
#include <cassert>
#include <climits>

using namespace std;

#define debug if (debuge) cerr
const bool debuge = false;

#define trace(x) debug << #x << " = " << x << endl

struct Jedi {
	long start, end;
};

bool operator < (const Jedi& j1, const Jedi&j2) {
	return j1.end < j2.end;
}

long tt(long anchor, long m, long i) {

	long r = i - anchor;
	
	if (r < 0) {
		r = m + r;
	}
	
	return r;
	
}


long size(long m, long start, long end) {
		
	if (start < end) {
		return end - start;
	} else {
		return m - start + end;
	}		
}

long maxJedis(const vector<Jedi> &jedis, long start, long end) { // Exclusive!
	long count = 0;
	
	debug << "maxJedis called with start: " << start << " end: " << end << endl;
	for (Jedi jedi : jedis) {
	
		debug << "jedi: " << jedi.start << "," << jedi.end << " start: " << start << " end: " << end;
		
		if (start < end) {
			if (jedi.start <= jedi.end && jedi.start > start && jedi.end < end) {
				debug << " case 1 ";
				count++;
				start = jedi.end;
			}
		} else {
			if (jedi.start <= jedi.end && ( jedi.start > start || jedi.end < end)  ) {
				debug << " case 2 ";
				count++;
				start = jedi.end;
			} else if (jedi.start > start && jedi.end < end) {
					debug << " case 3 ";
					count++;
					start = jedi.end;
			}		
		}
		debug << " newCount: " << count << endl;
		
	}
	
	return count;

}

void testcase() {
	long n,m;
	cin >> n >> m;
	
	trace(n);
	trace(m);
	
	vector<Jedi> jedis;
	map<long,long> startCount;
	long counter = 0;
	jedis.reserve(n);
	for (int i = 0; i < n; i++) {
		Jedi j;
		cin >> j.start >> j.end;
		
		//Make everything zero based
		j.start--;
		j.end--;
		
		jedis.push_back(j); //End - Start first!
		
		startCount[j.start]++;
		startCount[j.end]--;	
		
		if (j.end < j.start) {
			counter++;
		}
	}
	
	
	vector<long> segments;
	long bestPlace = 0;
	long bestCount = LONG_MAX;
	long segCount = 0;
	
	for (pair<long,long> seg : startCount) {
		segments.push_back(seg.first);
		counter += seg.second;
		
		debug << seg.first << " ";
			
		if (counter <= 10 && counter < bestCount) {
			bestPlace = segCount;
			bestCount = counter;
		}
		segCount++;
	}
	
	trace(bestPlace);
	trace(bestCount);
		
	for (Jedi &jedi : jedis) { //Transform to compressed coordinates
		debug << "updated jedi (" << jedi.start << "," << jedi.end << ") to ";
		jedi.start = distance(segments.begin(), lower_bound(segments.begin(), segments.end(), jedi.start));
		jedi.end = distance(segments.begin(), lower_bound(segments.begin(), segments.end(), jedi.end));
	
		
		int diff1 = size(segments.size(), jedi.start, jedi.end);
		
		//Transform to make bestPlace = 0;
		jedi.start = tt(bestPlace, segments.size(), jedi.start);
		jedi.end = tt(bestPlace, segments.size(), jedi.end);
		
		int diff2 = size(segments.size(), jedi.start, jedi.end);
		
		assert(diff1 == diff2);
		
		assert(tt(bestPlace, segments.size(), bestPlace) == 0);
	
		debug << "(" << jedi.start << "," << jedi.end << ")" << endl;
	}
	
	sort(jedis.begin(), jedis.end()); //First ending first!
	
	long result = 0;
	for (Jedi jedi : jedis) {
		if (jedi.start == 0 || jedi.end == 0 || jedi.start > jedi.end ) {
			debug << "Intersecting: " << jedi.start << "," << jedi.end << endl;
			
			result = std::max(result, maxJedis(jedis, jedi.end, jedi.start) + 1);
		}
	}
	
	result = std::max(result, maxJedis(jedis, 0, 0));
	
	
	cout << result << endl;
	
	
	
	
	
	

}


int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
    	testcase();
    }
}
