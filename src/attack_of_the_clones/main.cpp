#import <iostream>
#import <vector>
#import <algorithm>
#import <utility>
#import <climits>
#import <cassert>
#include <queue>

using namespace std;

typedef std::priority_queue<pair<int,int>, std::vector<pair<int,int>>, std::greater<pair<int,int>> > min_heap;

void testCase() {
	
	int n,m;
	cin >> n >> m;
	
	vector<pair<int,int>> seg(n);
	
	for (int i = 0; i < n; i++) {

		int a,b;
		
		cin >> a >> b;
		
		seg[i] = make_pair(a,b);
		
	}
	
	sort(seg.begin(), seg.end());
	
	min_heap minEnd;
	
	vector<int>
	
	//Go around twice! Twice the fun & makes life easier.
	for (int k = 1; k <= 2; k++) {
	
	
		for (int i = 0; i < n; i++) {
	
			int start = seg[i].first * k;
			int end = seg[i].second * k;
			
			while (!minEnd.empty() && minEnd.top().second < start) {
				minEnd.pop();
			}
			
			
			

		}

	}	
	
	
}

int main() {
	ios_base::sync_with_stdio(false);
	int t;
	cin >> t;
	
	for (int i = 0; i< t; i++) {
		testCase();
	}
	
	return 0;
}