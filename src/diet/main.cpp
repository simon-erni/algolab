// example: find the risk-optimal investment strategy
// in Swatch and Credit Suisse shares (data as in lecture)
#include <iostream>
#include <cassert>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

// choose exact rational type
#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq ET;
// solution type the solver provides
typedef CGAL::Quotient<ET> SolT;
// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

using namespace std;

// round up to next integer double
double ceil_to_double(const SolT& x)
{
  double a = std::ceil(CGAL::to_double(x));
  while (a < x) a += 1;
  while (a-1 >= x) a -= 1;
  return a;
}

double floor_to_double(const SolT& x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

void solve(int n, int m) {
	
	
	//Smaller, Lowerbound, 0, Upperbound, 0
	Program lp (CGAL::SMALLER, true, 0, false, 0); 
	/*
	int x = 0;
	int y = 1;
	
	qp.set_a(x, 0, ET(1));
	qp.set_a(y, 0, ET(1));
	qp.set_b(0, ET(4)); // <=
	
	
	qp.set_a(x, 1, ET(4));
	qp.set_a(y, 1, ET(2));
	qp.set_b(   1, ET(a*b)); // <=
	
	qp.set_a(x, 2, ET(-1));
	qp.set_a(y, 2, ET(1));
	qp.set_b(   2, ET(1)); // <=
	

	qp.set_d(x,x,ET(2*a));
	qp.set_c(y,ET(-b));
	*/
	
	
	
	for (int i = 0; i < n; i++) {
	
		int min, max;
		
		cin >> min >> max;
				
		lp.set_b(2*i, max); //2*i 'te Gleichung für den Nutrient
		lp.set_b(2*i + 1, min);
		
		lp.set_r(2*i, CGAL::SMALLER);
		lp.set_r(2*i + 1, CGAL::LARGER);
		
	}
	
	for (int i = 0; i < m; i++) {
	
		int price;
		cin >> price;
		
		lp.set_c(i, price);
		
		for (int k = 0; k < n; k++) {
			int c;
			cin >> c;
			
			lp.set_a(i, 2*k, c);
			lp.set_a(i, 2*k + 1, c);
		}
		
	}
	
	Solution s = CGAL::solve_linear_program(lp, ET());
	
	if (s.is_optimal()) {
		cerr << "unrounded: " << CGAL::to_double(s.objective_value()) << endl;
		cout << (int) floor_to_double(s.objective_value()) << endl;

	} else if (s.is_infeasible()) {
		cout << "No such diet." << endl;
	} else {
		cout << "unbounded" << endl;
	}

}

int main() {
	ios_base::sync_with_stdio(false);
	
	int n,m;
	
	cin >> n >> m;

	while (n != 0 && m != 0) {
		
		solve(n,m);
		
		cin >> n >> m;
	}
	
	
	return 0;
}