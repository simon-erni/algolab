#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <set>
#include <cassert>
#include <climits>

using namespace std;


void testCase() {

    int n;

    cin >> n;

    vector<long> loc;
    vector<pair<long, long>> max_locations;

    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;

        loc.push_back(x);
    }

    sort(loc.begin(), loc.end());

    long max = 0;
    long a = 0;
    long b = 0;
    long minDist = LONG_MAX;

    while (b < n) {

        assert(a <= b);
        assert(loc[b] - loc[a] >= 0);

        if (loc[b] - loc[a] > 200) {
            a++;
        } else {

            long distance = loc[b] - loc[a];

            if (distance % 2 == 1) {
                distance++;
            }
            distance = distance / 2;

            if (b - a + 1 > max || (b - a + 1 == max && distance < minDist)) {
                max = b - a + 1;
                max_locations.clear();

                max_locations.emplace_back(a, b);
                minDist = distance;

            } else if (b - a + 1 == max && distance == minDist) {

                max_locations.emplace_back(a, b);

            }

            b++;


        }

    }


    vector<long> final;

    for (auto &max_location : max_locations) {

        long diff = (loc[max_location.second] - loc[max_location.first]);
        final.push_back(diff / 2 + loc[max_location.first]);
        if (diff % 2 == 1) {
            final.push_back(diff / 2 + loc[max_location.first] + 1);
        }


    }

    sort(final.begin(), final.end());

    cout << max << " " << minDist << endl;
    for (auto i : final) {
        cout << i << " ";
    }

    cout << endl;

}


int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}