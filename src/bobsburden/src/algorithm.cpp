#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>

#include <cassert>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::property<boost::edge_weight_t, int> 		// interior properties of edges
		>					Graph;
typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef boost::property_map<Graph, boost::edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t


using namespace std;

int tt(int i, int j) {
	
//	cerr << "getting called with " << i << " " << j << endl;
	assert(j <= i);
	assert(i >= 1);
	assert(j >= 1);

	int above = (i * (i - 1)) / 2;
	
	return above + j - 1;
	
}

Graph G;
WeightMap weightmap;
int n;

void addEdge(int u, int v, int w) {

	Edge e;	bool success;
	boost::tie(e, success) = boost::add_edge(u, v, G);
	weightmap[e] = w;
}

vector<long> shortestPath(int i, int j) {
		//Start top
	// Dijkstra shortest paths
	// =======================
	std::vector<Vertex> predmap(n);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
	std::vector<long> distmap(n);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
	boost::dijkstra_shortest_paths(G, tt(i,j), // We MUST provide at least one of the two maps
		boost::predecessor_map(boost::make_iterator_property_map(predmap.begin(), boost::get(boost::vertex_index, G))).	// predecessor map as Named Parameter
		distance_map(boost::make_iterator_property_map(distmap.begin(), boost::get(boost::vertex_index, G))));	// distance map as Named Parameter
		
	return distmap;
}



void testcase() {
	int k;
	cin >> k;
	
	n = k*(k+1) / 2;
	
	G = Graph(n);
	weightmap = boost::get(boost::edge_weight, G);
	vector<int> weight(n);
	for (int i = 1; i <= k ; i++) {
		for (int j = 1; j <= i; j++) {
			int w;
			cin >> w;
			
			int u = tt(i,j);
			weight[u] = w;
			
			
			//Always add reverse edge!
			
			//Top Left
			if (j > 1 && i > 1) {
				int v = tt(i-1,j-1);
				addEdge(u,v, weight[u]);
				addEdge(v,u, weight[v]);
			}
			//Top Right
			if (i > 1 && j < i) {
				int v = tt(i-1,j);
				addEdge(u,v, weight[u]);
				addEdge(v,u, weight[v]);
			}
			//Left
			if (j > 1) {
				int v = tt(i,j-1);
				addEdge(u,v, weight[u]);
				addEdge(v,u, weight[v]);
			}
			
			
		}
	}
	
	vector<long> distTop = shortestPath(1,1);
	vector<long> distLeft = shortestPath(k,1);
	vector<long> distRight = shortestPath(k,k);
	
	long minDist = LONG_MAX;
	
	for (int i = 0; i < n; i++) {
		minDist = std::min(minDist, distTop[i] + distLeft[i] + distRight[i] + weight[i]);
	}
	
	cout << minDist << endl;

	
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    
    cin >> t;
    for (int i = 0; i < t; i++) {
    	testcase();
    }
}
