#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <utility>

// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>


using namespace std;
using namespace boost;


// Directed graph with integer weights on edges.
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef adjacency_list<vecS, vecS, directedS,
		no_property,
		property<edge_weight_t, int>
		>					Graph;
typedef graph_traits<Graph>::vertex_descriptor		Vertex;	// Vertex type		
typedef graph_traits<Graph>::edge_descriptor		Edge;	// Edge type
typedef graph_traits<Graph>::edge_iterator		EdgeIt;	// Edge iterator
// Property map edge -> weight
typedef property_map<Graph, edge_weight_t>::type	WeightMap;




void testcases() {
	
	int n,m,q;

	cin >> n >> m >> q;

	vector<pair<int,int>> lonlat(n);	
	Graph G(n);
	
	for (int i = 0; i < n; i++) {
		int lon,lat;
		cin >> lon >> lat;
		lonlat[i] = make_pair(lon,lat);
	}	
	
	WeightMap weightmap = get(edge_weight, G);
	for (int i = 0; i < m; ++i) {
		int u, v, w;		// Each edge: <from> <to> <weight>
		cin >> u >> v >> w;
		Edge e;	bool success;					// *** We swap u and v to create ***
		tie(e, success) = add_edge(u, v, G);	// *** the reversed graph GT!   ***
		weightmap[e] = w;
	}
	
	vector<Edge> mst;
	kruskal_minimum_spanning_tree(G, back_inserter(mst));
	
	
	vector<pair<int,int>> queries(q);
	
	for (int i = 0; i < q; i++) {
		int s,t;
		
		cin >> s >> t;
		
		queries[i] = make_pair(s,t);
	}	
	
	for (int i = 0; i < q; i++) {
	
		vector<int> distmap(n);
		vector<Vertex> predmap(n);	// vectors as an exterior property map.
		
			dijkstra_shortest_paths(G, queries[i].first,
				predecessor_map(make_iterator_property_map(	// named parameters
					predmap.begin(), get(vertex_index, G))).
				distance_map(make_iterator_property_map(	// concatenated by .
					distmap.begin(), get(vertex_index, G))));

		
		int length = distmap[queries[i].second];
		
		if (length == 2147483647) {
			cout << "unreachable" << "\n";
		} else {
			cout << length << "\n";
		}
		
		
	}
	
	
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false);
	
	int t;
	cin >> t;

	for (int i = 0; i < t; i++) {
		testcases();

	}
}