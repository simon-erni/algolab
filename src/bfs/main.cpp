#include <iostream>
#include <queue>

using namespace std;

void testCase() {

    int n, m, start;

    cin >> n;
    cin >> m;
    cin >> start;

    vector<vector<int>> g(n);

    vector<int> d(n, -1);

    for (int i = 0; i < m; i++) {

        int a, b;
        cin >> a;
        cin >> b;

        g[a].push_back(b);
        g[b].push_back(a);

    }

    queue<int> q;

    q.push(start);
    d[start] = 0;

    while (!q.empty()) {

        int v = q.front();
        q.pop();

        for (auto neighbour : g[v]) {
            if (d[neighbour] == -1) {
                d[neighbour] = d[v] + 1;
                q.push(neighbour);
            }
        }
    }

    for (auto v : d) {
        cout << v << " ";
    }

    cout << endl;

}

int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {

        testCase();

    }

}