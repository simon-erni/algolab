#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <set>
#include <stdexcept>
#include <cassert>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

// BGL Graph definitions
// ===================== 
// Graph Type with nested interior edge properties for Cost Flow Algorithms
typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
    boost::property<boost::edge_capacity_t, long,
        boost::property<boost::edge_residual_capacity_t, long,
            boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
                boost::property <boost::edge_weight_t, long> > > > > Graph; // new!
// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type      EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type       ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor          Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator

using namespace std;

#define debug if (debug_e) cerr
#define trace if (trace_e) cerr
const bool debug_e = false;
const bool trace_e = false;

long edges = 0;
// Custom Edge Adder Class, that holds the references
// to the graph, capacity map, weight map and reverse edge map
// ===============================================================
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long cap, long cost) {
        if (cap == 0) {
            return;
        }
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = cap;
        weightmap[e] = cost; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = -cost; // new
        revedgemap[e] = rev_e; 
        revedgemap[rev_e] = e;
        trace << "  added edge from " << u << " to " << v << " with cap " << cap << " and cost " << cost << endl;
        edges++;
    }
};



int n,s;
int ttt(vector<vector<int>> &stationTimes, int station, int time) {
    assert(station >= 0);
    assert(station < stationTimes.size());

    assert(time >= 0);
    auto it = lower_bound(stationTimes[station].begin(), stationTimes[station].end(), time);
    assert(it != stationTimes[station].end());
    int j = std::distance(stationTimes[station].begin(), it);

    int k = 0;
    for (int i = 0; i < station; i++) {
        k += stationTimes[i].size();
    }

    return j + k;
}

long ccc(int maxcost, int time1, int time2, int cost) {
    assert(time2 > time1);
    assert(maxcost >= cost);

    assert(cost >= 0);

    long result = maxcost * (time2 - time1) - cost;
    assert(result > 0);
    return result;
}

void testcase() {
    cin >> n >> s;

    debug << "n: " << n << " s: " << s << endl;
    
    long totalCap = 0;
    vector<int> initialCap(s,0);
    for (int i = 0; i < s ;i++ ) {
        cin >> initialCap[i];
        totalCap += initialCap[i];
    }
    
    vector<set<int>> stationTimes(s, set<int>());
    for (int i = 0; i < s; i++) {
        stationTimes[i].insert(0);
    }
    
    int maxTime = 0;
    int maxProfit = 0;
    vector<vector<int>> r(n, vector<int>(5,0)); //requests
    for (int i = 0; i < n; i++) {
        cin >> r[i][0] >> r[i][1] >> r[i][2] >> r[i][3] >> r[i][4];
        r[i][0]--;
        r[i][1]--;
        stationTimes[r[i][0]].insert(r[i][2]);
        stationTimes[r[i][1]].insert(r[i][3]);
        
        maxTime = std::max(maxTime, r[i][2]);
        maxTime = std::max(maxTime, r[i][3]);
        maxProfit = std::max(maxProfit, r[i][4]);
    }
    trace << "ok" << endl;
    for (int i = 0; i < s; i++) {
        stationTimes[i].insert(maxTime);
    }
    long N = 0;
    vector<vector<int>> times(s, vector<int>());
    for (int i = 0; i < s; i++) {
        for (int time : stationTimes[i]) {
            times[i].push_back(time);
            N++;
        }
    }


    for (int i = 0; i < s; i++) {
        trace << "station: " << i << ": ";
        for (int time : stationTimes[i]) {
            trace << time << " {" << ttt(times, i, time) << "}, ";
        }
        trace << endl;
    }

    Graph G(N + 2);
    int source = N;
    int target = N + 1;

    EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
    EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
    ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
    ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
    EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);
    for (vector<int> req : r) {
        int u = ttt(times, req[0], req[2]);
        int v = ttt(times, req[1], req[3]);
        long w = ccc(maxProfit, req[2], req[3], req[4]);

        eaG.addEdge(u,v,1,w);
    }

    for (int stat = 0; stat < s ; stat++) {
        eaG.addEdge(source, ttt(times, stat, 0), initialCap[stat], 0);

        for (int i = 0; i < times[stat].size() - 1; i++) {
            long w = ccc(maxProfit, times[stat][i], times[stat][i + 1], 0);
            eaG.addEdge(ttt(times, stat, times[stat][i]), ttt(times, stat, times[stat][i + 1]), LONG_MAX, w);
        }

        eaG.addEdge(ttt(times, stat, times[stat][times[stat].size() - 1]), target, LONG_MAX, 0);

    }

    debug << "Vertices: " << N+2 << " Edges: " << edges << endl;
    debug << "start calc ..." << endl;
    boost::successive_shortest_path_nonnegative_weights(G, source, target);
    debug << "start cycle cancle ..." << endl;
    //boost::cycle_canceling(G);
    debug << "start flow cost ..." << endl;
    long cost = boost::find_flow_cost(G);

    cout << - cost + maxTime*maxProfit*totalCap << endl;


    

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testcase();
    }
}
