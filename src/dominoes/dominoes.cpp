#include <iostream>
#include <vector>

using namespace std;


void testCase() {


    int buffer = 0;
    int amountFallen = 0;


    vector<int> h;

    int n;
    cin >> n;

    int x;

    for (int i = 0; i < n; i++) {
        cin >> x;
        h.push_back(x);
    }


    for (int i = 0; i < n && buffer >= 0;) {


        int max = h[i] - 1;

        if (max > buffer) {
            buffer = max;
        }
        amountFallen++;
        i++;
        buffer--;

    }


    cout << amountFallen << endl;

}


int main() {

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}