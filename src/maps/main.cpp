#include <iostream>
#include <map>
#include <string>
#include <set>

using namespace std;

void testCase() {

    int q;

    cin >> q;


    multimap<string, int> x;

    for (int i = 0; i < q; i++) {

        int a;
        string b;

        cin >> a;
        cin >> b;

        if (a == 0) {

            x.erase(b);

        } else if (a > 0) {

            x.insert(make_pair(b, a));

        }


    }

    string s;
    cin >> s;


    auto pos = x.equal_range(s);
    if (pos.first == x.end()) {
        cout << "Empty" << endl;
    } else {


        set<int> result;


        for (auto i = pos.first; i != pos.second; i++) {
            result.insert(i->second);
        }

        for (auto i : result) {
            cout << i << " ";
        }

        cout << endl;

    }
}

int main() {

    ios_base::sync_with_stdio(false);

    int t;

    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}
