#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <queue>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/max_cardinality_matching.hpp>



using namespace std;

#define debug if (debuge) cerr

const bool debuge = false;

// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::property<boost::edge_weight_t, long> 		// interior properties of edges
		>					Graph;
typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef boost::property_map<Graph, boost::edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t

int n,start;


long toWG(int u, int v, long w) {

    assert(0<= u && u <= n - 1);
    assert(0<= v && v <= n - 1);
    assert(w > 0);

    long result = w*n + (n - 1 - max(u,v));

    assert(result > w); //Assert no overflows

    return result;
}

long toOW(int u, int v, int w) {

    return (w - ((n - 1 - max(u,v)))) / n;
}



void testcase() {
    
    cin >> n >> start;
    start--;

    Graph G(n); //Weight, from & to (fully connected graph anyway)
    WeightMap weightmap = boost::get(boost::edge_weight, G);
   
    for (int j = 1; j <= n - 1; j++) {  
     	
        for (int k = 1; k <= n - j; k++) {

            long w;
            cin >> w;

            Edge e;	bool success;
            int u = j - 1;
            int v = j + k - 1;

            w = toWG(u,v, w);

		    boost::tie(e, success) = boost::add_edge(u, v, G);  

            weightmap[e] = w; // Make the weights unique (tie break)
            debug << "added "<< e << " w:" << w << endl;
        }
    }
     
    std::vector<Vertex> primpredmap(n);
    
	boost::prim_minimum_spanning_tree(G, boost::make_iterator_property_map(primpredmap.begin(), boost::get(boost::vertex_index, G)), boost::root_vertex(start));
    debug << "OK" << endl;

    Graph MST(n);
    WeightMap mstWeight = boost::get(boost::edge_weight, MST);
    vector<pair<pair<int,int>,long>> nomst;
    long weight = 0;
    debug << "Edges in MST: " << endl;
    for (int i = 0; i < n; ++i) {
		Vertex u = i;
		OutEdgeIt oebeg, oeend;
		for (boost::tie(oebeg, oeend) = boost::out_edges(u, G); oebeg != oeend; ++oebeg) {
			Vertex v = boost::target(*oebeg, G);	// source(*oebeg, G) is guaranteed to be u, even in an undirected graph.
			if (primpredmap[u] == v) {
                Edge e;	bool success;
                boost::tie(e, success) = boost::add_edge(u,v,MST);
                mstWeight[e] = toOW(u,v,weightmap[*oebeg]);
                weight += toOW(u,v,weightmap[*oebeg]);
				debug << u << " -- " << v << " (weight " << toOW(u,v,weightmap[*oebeg]) << ")\n";
			} else if (u < v && primpredmap[v] != u) {
                nomst.push_back(make_pair(make_pair(u,v),toOW(u,v,weightmap[*oebeg])));
            }
		}
	}

    debug << "total weight: " << weight  << endl;

    vector<vector<long>> maxE(n, vector<long>(n, 0));

    for (int i = 0; i < n; i++) {

        debug << "starting at " << i << endl; 

        queue<Vertex> s;
        vector<bool> visited(n,false);
        s.push(i);

        maxE[i][i] = 0;
        visited[i] = true;

        while (!s.empty()) {
            Vertex u = s.front();
            s.pop();
            visited[u] = true;

            debug << "visiting " << u << ", out edges: ";
            
            OutEdgeIt oebeg, oeend;
            for (boost::tie(oebeg, oeend) = boost::out_edges(u, MST); oebeg != oeend; ++oebeg) {

			    Vertex v = boost::target(*oebeg, MST);	// source(*oebeg, G) is guaranteed to be u, even in an undirected graph.
                if (!visited[v]) {
                    maxE[i][v] = std::max(maxE[i][u], mstWeight[*oebeg]);
                    assert(maxE[i][v] > 0);
                    s.push(v);
                }
                debug << " maxE[" << i << "," << v << "] = " << maxE[i][v] << " ## ";

            }
            debug << endl;
		}

    }

    long minweight = LONG_MAX;

    debug << "min: " << endl;
    for (int i = 0; i < nomst.size(); i++) {
        long u = nomst[i].first.first;
        long v = nomst[i].first.second;
        long w = nomst[i].second;
        debug << "trying to add edge (" << u << "," << v << ")" << " w:" << w << " max weight in graph: " << maxE[u][v] << endl;
        long newWeight = weight - maxE[u][v] + w;
        minweight = std::min(newWeight, minweight);
    }

    cout << minweight << endl;



}





int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    int t;
    cin >> t;
    for (int i = 0 ; i < t; i++) {
        testcase();
    }

}
