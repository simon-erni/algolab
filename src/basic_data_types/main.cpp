#include <iostream>
#include <string>
#include <iomanip>

using namespace std;


void testCase() {


    int n; cin >> n;

    long long m; cin >> m;

    string s; cin >> s;

    double d; cin >> d;


    cout << setprecision(2) << fixed;
    cout << n << " " << m << " " << s << " " << d << "\n";

}


int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}