#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <set>


using namespace std;

struct Edge{
    int u,v,w;
};

struct Vertex {
    long u,oo,eo,oe,ee;
};

void update(long &u, long &v, int &w, set<int> &toChange, long &count, int &vI) {
    if (u != -1 && (v == -1 || u + w < v)) {
        v = u + w;
        count++;
        toChange.insert(vI);
    }
}

void printV(const vector<Vertex> &v) {
    for (Vertex u : v) {
        cerr << "v: " << u.u << " oo: " << u.oo << " eo: " << u.eo << " oe: " << u.oe << " ee: " << u.ee << endl;
    }
}

void testcase() {
    int n,m,s,t;
    cin >> n >> m >> s >> t;

    cerr << "n: " << n << " m: " << m << " s: "<< s << " t: " << t << endl;

    vector<vector<Edge>> g(n, vector<Edge>());
    vector<Vertex> v(n);
    for (int i = 0; i < n; i++) {
        v[i].oo = v[i].eo = v[i].oe = v[i].ee = -1;
        v[i].u = i;
    }
    
    for  (int i = 0; i < m; i++) {
        Edge e;
        cin >> e.u >> e.v >> e.w;
        g[e.u].push_back(e);
    }

    set<int> changed;
    set<int> toChange;
    long count = 0;
    v[s].ee = 0;
    changed.insert(s);

    while (!changed.empty()) {
        //Round + 1 % 2 is the new vector to change, round % 2 is the current one
        for (int u : changed) {
            for (Edge e : g[u]) { //neighbors (or same)
                if (e.w % 2 == 0) {
                    update(v[u].oo, v[e.v].eo, e.w, toChange, count, e.v);
                    update(v[u].eo, v[e.v].oo, e.w, toChange, count, e.v);
                    update(v[u].oe, v[e.v].ee, e.w, toChange, count, e.v);
                    update(v[u].ee, v[e.v].oe, e.w, toChange, count, e.v);
                } else {
                    update(v[u].oo, v[e.v].ee, e.w, toChange, count, e.v);
                    update(v[u].eo, v[e.v].oe, e.w, toChange, count, e.v);
                    update(v[u].oe, v[e.v].eo, e.w, toChange, count, e.v);
                    update(v[u].ee, v[e.v].oo, e.w, toChange, count, e.v);
                }
            }
        }

        changed = toChange;
        toChange = set<int>();
    }

    //printV(v);

    if (v[t].oo == -1) {
        cout << "no" << endl;
    }  else {
        cout << v[t].oo << endl;
    }


    
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
