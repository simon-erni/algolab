#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <utility>
#include <map>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/range_search_delaunay_2.h>


#include <CGAL/Triangulation_vertex_base_with_info_2.h>


#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Point_set_2.h>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<long,K> Vb; 
typedef CGAL::Triangulation_face_base_2<K> Fb; 
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds; 
//typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation;
typedef CGAL::Point_set_2<K,Tds> Triangulation;
typedef K::Circle_2 C;

typedef K::FT FT;
typedef Triangulation::Point Point;
typedef Triangulation::Geom_traits::Circle_2 Circle;
typedef Triangulation::Vertex_handle Vertex;

struct Participant {
    Point p;
    long r;
};

#define debug if (debuge) cerr

const bool debuge = false;

using namespace std;

void testcase() {

    long m,n;
    cin >> m >> n;

    vector<Participant> play;

    for (long i = 0; i < m; i++) {
        Participant part;
        long x,y;
        cin >> x >> y >> part.r;
        Point p(x,y);
        part.p = p;

        play.push_back(part);

    }    

    long h;
    cin >> h;

    vector<pair<Point,long>> lamps;
    for (long i = 0; i < n; i++) {
        long x,y;
        cin >> x >> y;
        Point p(x,y);

        lamps.push_back(make_pair(p,i));
    }
    
    cerr << "total lamps:" << lamps.size() << endl;

    Triangulation t;
    t.insert(lamps.begin(), lamps.end());

    vector<long> winners;
    long maxKilledRound = -1;
    bool survivors = false;
    
    long remaining = m;

    for (int i = 0; i < play.size(); i++) {
      	
      	vector<Vertex> l;
      	C circle(play[i].p, play[i].r + h);

    	t.range_search(circle, back_inserter(l));
    	cerr << "searching for " << i << " in " << l.size() << " lamps " << endl;
    	
    	long minL = LONG_MAX;
    	for (Vertex v : l) {
    		cerr << v->info() << endl;
    		minL = std::min(v->info(), minL);
    	}
    	
    	if (minL > maxKilledRound) {
    		maxKilledRound = minL;
    		winners.clear();
    		winners.push_back(i);
    	} else if (minL == maxKilledRound) {
    		winners.push_back(i);
    	}

    }

    sort(winners.begin(), winners.end());
    for (int i = 0; i < winners.size(); i++) { 
        cout << winners[i] << " ";
    }
    cout << endl;


}
int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        //debuge = i == 5;
        testcase();
    }
}
