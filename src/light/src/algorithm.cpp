#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <utility>
#include <map>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K> Triangulation;
typedef K::FT FT;
typedef Triangulation::Point Point;
typedef Triangulation::Geom_traits::Circle_2 Circle;
typedef Triangulation::Vertex_handle Vertex;

struct Participant {
    Point p;
    long r;
    long i;
};

#define debug if (debuge) cerr

const bool debuge = false;

using namespace std;

void testcase() {

    long m,n;
    cin >> m >> n;

    vector<Participant> play;

    for (long i = 0; i < m; i++) {
        Participant part;
        long x,y;
        cin >> x >> y >> part.r;
        Point p(x,y);
        part.p = p;
		part.i = i;
        play.push_back(part);

    } 
    
    long h;
    cin >> h;

    vector<Point> lamps;
    for (long i = 0; i < n; i++) {
        long x,y;
        cin >> x >> y;
        Point p(x,y);

        lamps.push_back(p);
    }
    

    Triangulation t;
    t.insert(lamps.begin(), lamps.end());

    vector<long> winners;
    long maxKilledRound = -1;
    bool survivors = false;

    debug << "H: " << h << endl;
    
    long saved = 0;
    long done = 0;
    for (int i = 0; i < play.size(); i++) {
      
        Vertex v = t.nearest_vertex(play[i].p);
		FT dist = CGAL::square(FT(h+play[i].r));
		
        if (dist <= CGAL::squared_distance(play[i].p, v->point())) {
            
            if (!survivors) {
                winners.clear();
                maxKilledRound = LONG_MAX;
                survivors = true;
            }
            
            winners.push_back(play[i].i);
            continue;
        } else if (survivors) {
            debug << "ignoring player (no survivor) " << i << endl;
            continue;
        }

        long firstHit = LONG_MAX;
        for (long j = 0; j < lamps.size(); j++) {
            if (dist > CGAL::squared_distance(play[i].p, lamps[j])) {
				
				saved += lamps.size() - j - 1;
				
                firstHit = j;
                break;
            }
            done++;
        }

        if (firstHit > maxKilledRound) {
            winners.clear();
            winners.push_back(play[i].i);
            maxKilledRound = firstHit;
        } else if (firstHit == maxKilledRound) {
            winners.push_back(play[i].i);
        }

    }

	cerr << "saved: " << saved << " done: " << done << endl;
    sort(winners.begin(), winners.end());
    for (int i = 0; i < winners.size(); i++) { 
        cout << winners[i] << " ";
    }
    cout << endl;
    


}
int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testcase();
    }
}
