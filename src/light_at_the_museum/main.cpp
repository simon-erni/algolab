#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <climits>

using namespace std;

vector<pair<vector<int>, int>> getCombinations(const int start, const int end, const vector<int> &b, const vector<vector<pair<int,int>>> &l, int n, int m) {

	vector<pair<vector<int>, int>> comb;

	for (int s = 0; s < 1<<((end-start) + 1); ++s) { // n (split) is the count of the switches
		
		vector<int> combination(m,0);
		int changes = 0;
		
		bool skip = false;
		
		for (int i = 0; i < (end-start) + 1; ++i) { 
			if (s & 1<<i) { //The bit in the i-th position must be 1 - should i change the i-th switch or not
				changes++;
				for (int k = 0; k < m; k++) {
					combination[k] += l[start + i][k].second;

					if (combination[k] > b[k]) {
						skip = true;
						break;
					}
				}	
			} else {
				for (int k = 0; k < m; k++) {
					combination[k] += l[start + i][k].first;
					
					if (combination[k] > b[k]) {
						skip = true;
						break;
					}
				}	
			}
			if (skip) {
				break;
			}
		}
		
		if (!skip) {
			comb.push_back(make_pair(combination,changes));
		}
		 
	}
	
	return comb;


}


void testcase() {

	int n,m;
	
	cin >> n;
	cin >> m;
	
	vector<int> b(m);
	
	for (int i = 0; i < m; i++) {
		cin >> b[i];
	}
		
	vector<vector<pair<int,int>>> l(n, vector<pair<int,int>>(m));
	for (int i = 0; i < n; i++) {
		for (int k = 0; k < m; k++) {
		
			int on, off;
			cin >> on >> off;
			
			l[i][k] = make_pair(on,off);
		}
	}
	
	int split = (n/2) - 1; //inclusive, zero based;
	
	vector<pair<vector<int>, int>> comb1 = getCombinations(0,split, b, l,n,m);	//The combination, stored along with the number of changes
	vector<pair<vector<int>, int>> comb2 = getCombinations(split + 1, n - 1, b, l,n,m);	//The combination, stored along with the number of changes
	
	if (comb1.size() == 0 || comb2.size() == 0) {
		cout << "impossible" << endl;
		return;
	}
	
	sort(comb1.begin(), comb1.end());
	sort(comb2.begin(), comb2.end());


	int minChanges = INT_MAX;
	bool found = false;
	for (int i = 0; i < comb1.size(); i++) {
	
		vector<int> rem(m); 
		for (int k = 0; k < m; k++) {
			rem[k] = b[k] - comb1[i].first[k];
		}
		
		auto it = lower_bound(comb2.begin(), comb2.end(), make_pair(rem,0));
		
		if (it == comb2.end()) {
			continue;
		}
						
		for (int j = distance(comb2.begin(), it); j < comb2.size(); j++) {
		
			bool suitable = true;
			
			for (int k = 0; k < m; k++) {
				if (comb1[i].first[k] + comb2[j].first[k] != b[k]) {
					suitable = false;
					break;
				}
			}
			
			if (!suitable) {
				break;
			} else if (comb2[j].second  + comb1[i].second < minChanges) {
				minChanges = comb2[j].second + comb1[i].second;
				found = true;
			}	
		}
	}
	
	if (found) {
		cout << minChanges << endl;
	} else {
		cout << "impossible" << endl;
	}
}


int main() {
	
	ios_base::sync_with_stdio(false);
	
	int t;
	cin >> t;
	
	for (int i = 0; i < t; i++) {
		testcase();
	}
	
	return 0;
}