#include <iostream>
#include <string>

using namespace std;


string reverseString(string s) {
    string r;
    for (long i = s.length() - 1; i >= 0; i--) {

        r.push_back(s[i]);

    }

    return r;

}


void testCase() {

    string a;
    cin >> a;
    string b;
    cin >> b;

    cout << a.length() << " " << b.length() << "\n";
    cout << a << b << "\n";

    string c = reverseString(a);

    string d = reverseString(b);

    char tempC = c[0];
    char tempD = d[0];

    d[0] = tempC;
    c[0] = tempD;


    cout << c << " " << d << endl;

}


int main() {

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}