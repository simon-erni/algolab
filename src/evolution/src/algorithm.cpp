#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <map>
#include <string>

using namespace std;

#define debug if (debuge) cerr

const bool debuge = false;

void testcase() {

    int n,q;
    cin >> n >> q;

    vector<long> age(n,-1);
    vector<vector<int>> jumpUp(n, vector<int>());
    vector<int> parent(n,-1);
    vector<vector<int>> children(n, vector<int>());
    vector<string> names(n);

    map<string,int> name_map; // name --> int (0-(n-1))

    int root = -1;
    long maxAge = -1;

    for (int i = 0; i < n; i++) {
        
        string name;
        cin >> name >> age[i];

        if (age[i] > maxAge) {
            root = i;
            maxAge = age[i];
        }

        name_map.insert(make_pair(name,i));
        names[i] = name;

    }

    debug << "root is " << root << endl;

    for (int i = 0; i < n - 1; i++) {
        string s,p;
        cin >> s >> p;

        auto s_it = name_map.find(s);
        auto p_it = name_map.find(p);
        assert(s_it != name_map.end() && p_it != name_map.end());

        int u = p_it->second;
        int v = s_it->second;

        //u ---> v
        parent[v] = u;
        children[u].push_back(v);
    }


    vector<pair<int,int>> stack;
    stack.reserve(n);

    stack.push_back(make_pair(root,0)); //next element to visit;

    while (!stack.empty()) {

        int el, next;
        tie(el,next) = stack[stack.size() - 1];
        stack.pop_back();
        
        if (next == 0 && stack.size() > 0) { //Never visited before
            debug << "visiting " << el << " next: " << next << " children: " << children[el].size() << endl;

            int i = stack.size() / 2;
            while (i < stack.size() - 1) {

                debug << stack.size() << " " << i << " " << endl;
                jumpUp[el].push_back(stack[i].first);
                i += ((stack.size() - i) / 2) + 1;
            }

            debug << "jump ups: ";
            for (int k = 0; k < jumpUp[el].size() ; k++) {
                debug << jumpUp[el][k] << ",";
            }
            debug << endl;

        }

        //Visiting next
        if (next < children[el].size()) {
            stack.push_back(make_pair(el, next + 1));
            stack.push_back(make_pair(children[el][next], 0));
        }

    }

    for (int i = 0; i < q; i++) {

        string s;
        long b;
        cin >> s >> b;

        auto s_it = name_map.find(s);
        assert(s_it != name_map.end());

        int u = s_it->second;

        while (u != root) {
            //Test all the jumpups first
            for (int k = 0; k < jumpUp[u].size(); k++) {
                if (age[jumpUp[u][k]] <= b) {
                    u = jumpUp[u][k];
                    continue;
                }
            }

            if (age[parent[u]] <= b) {
                u = parent[u];
            } else {
                break;
            }
        }

        cout << names[u] << " ";

    }

    cout << endl;



}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
