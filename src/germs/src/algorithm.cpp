#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>

#include <CGAL/Delaunay_triangulation_2.h>

typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt KE;
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Finite_faces_iterator  Face_iterator;
typedef Triangulation::Finite_vertices_iterator  Finite_vertices_iterator;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef Triangulation::Point_iterator  Point_iterator;
typedef Triangulation::Vertex_circulator  Vertex_circulator;

using namespace std;

void testcase(int n) {

    int l,b,r,q;
    cin >> l >> b >> r >> q;

    vector<K::Point_2> points;
    points.reserve(n);

    for (int i = 0; i < n; i++) {
        K::Point_2 p;
        cin >> p;
        points.push_back(p);
    }

        

    Triangulation t;
    t.insert(points.begin(), points.end());

    vector<long> results;
    results.reserve(n);

    for (Finite_vertices_iterator v = t.finite_vertices_begin(); v != t.finite_vertices_end(); ++v) {

        K::FT dist = CGAL::min(CGAL::abs(v->point().x() - r), CGAL::abs(v->point().x() - l));
        dist = CGAL::min(dist, CGAL::abs(v->point().y() - b));
        dist = CGAL::min(dist, CGAL::abs(v->point().y() - q));

        dist = CGAL::sqrt(dist - K::FT(1)/K::FT(2));

        Triangulation::Edge_circulator c = t.incident_edges(v);
        
        if (c != 0) {
            
            do {
                if (!t.is_infinite(c)) {

                    dist = CGAL::min(dist,CGAL::sqrt((CGAL::sqrt(t.segment(c).squared_length()) - 1) / K::FT(2)));

                }

                c++;
            } while (c != t.incident_edges(v));

        }

        long result = (long) ceil(CGAL::to_double(dist));

        results.push_back(result);

       // cerr << "point " << v->point() << " min: " << dist << "," << result << endl;

        
       
    }

    sort(results.begin(), results.end());

    cout << results[0] << " " << results[results.size() / 2] << " " << results[results.size() - 1] << endl;

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);

    int n;
    cin >> n;
    while (n != 0) {
        testcase(n);
        cin >> n;

    }

    
}
