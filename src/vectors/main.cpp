#include <iostream>
#include <vector>

using namespace std;


void testCase() {

    int n;

    cin >> n;

    vector<int> a;
    for (int i = 0; i < n; i++) {

        int x;
        cin >> x;
        a.push_back(x);

    }

    int d;

    cin >> d;


    a.erase(a.begin() + d);

    int l, r;

    cin >> l;
    cin >> r;

    a.erase(a.begin() + l, a.begin() + r + 1);

    for (int i = 0; i < a.size(); i++) {

        cout << a[i];

        if (i != a.size() - 1) {
            cout << " ";
        }

    }

    cout << endl;
    
}


int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}