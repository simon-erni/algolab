#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <cassert>

using namespace std;

#define debug if (debuge) cerr

const bool debuge = false;

#define trace(x) debug << #x << ": " << x << endl

vector<int> dp;
vector<vector<int>> c;
int n;

void printState(const vector<int> &state) {
	if (!debuge) {
		return;
	}
	for (int k : state) {
		cerr << k << " ";
	}
	cerr << endl;
}

int tt(const vector<int> &state) {
	int result = 0;
	for (int i = 0; i < n; i++) {
		int coeff = state[i] + 1;
		
		assert(coeff >= 0);
		for (int j = i + 1; j < n; j++) {
			
			coeff = coeff * (c[j].size() + 1);
			
		}
		result += coeff;
	}
	return result;
}

int maxV(const vector<int> &state) {
	
	bool allZero = true;

	for (int i = 0; i < state.size(); i++) {
		if (state[i] >= 0) {
			allZero = false;
		}
		if (state[i] < -1) {
			return 0;
		}
		
	}

	if (allZero) {
		return 0;
	}
	
	if (dp[tt(state)] != -1) {
		debug << "maxV: ";
		printState(state);
		debug << "returning " << dp[tt(state)] << " from cache at pos " << tt(state) << endl;
		return dp[tt(state)];
	}
	
		vector<int> newState(n);


	int curMax = 0;
		
	for (int s = 1; s < 1<<n; ++s) { // Iterate through all possible subsets
		bool possible = true;
		int color = -1;
		int amount = 0;
				
		for (int i = 0; i < n; ++i) {
			if (s & 1<<i)  {// If i-th element in subset
			
				
				if (state[i] < 0) {
					possible = false;
					break;
				}
				
			
				if (color == -1) {
					color = c[i][state[i]];
					amount++;
				} else if (c[i][state[i]] == color){
					amount++;
				} else {
					possible = false;
					break;
				}
				
				newState[i] = state[i] - 1;
			}  else {
				newState[i] = state[i];
			}
		}
		
		
		if (!possible) {
			continue;
		}
		
		int points = 0;
		if (amount > 1) {
			points = 1<<(amount - 2);
		}
		
		
		curMax = std::max(curMax, maxV(newState) + points);
		
	}		
	
	
	dp[tt(state)] = curMax;
	
	debug << "maxV: ";
	printState(state);
	debug << "result: " << curMax << " at pos " << tt(state)<< endl;
	
	return curMax;

	
}

void testcase() {
	
	
	cin >> n;
		
	c = vector<vector<int>>(n);
	
	int total = 1;
	for (int i = 0; i < n; i++) {
		int d;
		cin >> d;
		c[i] = vector<int>(d);
		total = total * (d + 1);
	}
	
	trace(total);
	dp = vector<int>(total, -1);
	
	debug << "DP: ";
	printState(dp);
	
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < c[i].size(); j++) {
			cin >> c[i][j];
		}
	}
	
	vector<int> startState(n, -1);
	
	for (int i = 0; i < n; i++) {
		startState[i] = c[i].size() - 1;
	}
	
	
	cout << maxV(startState) << endl;
	
	
	
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    //t = 2;
    for (int i = 0; i < t; i++) {
    	testcase();
    }
}
