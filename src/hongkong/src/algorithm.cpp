#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <queue>
#include <utility>
#include <set>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>

#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Exact_predicates_exact_constructions_kernel KE;

#define debug if (debuge) cerr
 bool debuge = false;

#define trace if (tracee) cerr
 bool tracee = false;
using namespace std;

struct FaceInfo {
    KE::FT maxR;
    KE::Point_2 p0,p1,p2;

    vector<pair<long,KE::FT>> n;
    long i;
};

typedef CGAL::Triangulation_vertex_base_2<K> Vb; 
typedef CGAL::Triangulation_face_base_with_info_2<long,K> Fb; 
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds; 
typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation;

typedef Triangulation::Edge_iterator  Edge_iterator;
typedef Triangulation::Face_iterator Face_iterator;

typedef Triangulation::Vertex_handle Vertex;
typedef Triangulation::Face_handle Face;


struct Balloon {
    KE::Point_2 p;
    KE::FT r;
};

KE::FT maxV(KE::FT f1, KE::FT f2) {
    if (f1 >= f2) {
        return f1;
    } else {
        return f2;
    }
}

KE::FT minV(KE::FT f1, KE::FT f2) {
    if (f1 <= f2) {
        return f1;
    } else {
        return f2;
    }
}

void testcase() {
    long n,m;
    KE::FT r;
    cin >> n >> m >> r;

    std::vector<K::Point_2> trees;
    std::vector<Balloon> balloons;
    trees.reserve(n);
    balloons.reserve(m);


    for (int i = 0; i < n; ++i) {
        K::Point_2 p;
        std::cin >> p;
        trees.push_back(p);
    }

    Triangulation t;
    t.insert(trees.begin(), trees.end());

    vector<FaceInfo> faces;
    long faceI = 0;
    for (Face_iterator f = t.faces_begin(); f != t.faces_end(); ++f) {
        FaceInfo fi;
        fi.n.resize(3,make_pair(0,KE::FT(0)));
        fi.i = faceI;
        f->info() = fi.i;
        faces.push_back(fi);
        faceI++;
    }

    for (Face_iterator f = t.faces_begin(); f != t.faces_end(); ++f) {
        FaceInfo &fi = faces[f->info()];

        fi.p0 = KE::Point_2(f->vertex(0)->point().x(), f->vertex(0)->point().y());
        fi.p1 = KE::Point_2(f->vertex(1)->point().x(), f->vertex(1)->point().y());
        fi.p2 = KE::Point_2(f->vertex(2)->point().x(), f->vertex(2)->point().y());
        fi.n[0].second = CGAL::squared_distance(fi.p1, fi.p2);
        fi.n[1].second = CGAL::squared_distance(fi.p0, fi.p2);
        fi.n[2].second = CGAL::squared_distance(fi.p0, fi.p1);

        fi.maxR = KE::Circle_2(fi.p0,fi.p1,fi.p2).squared_radius();

        for (int i = 0; i < 3; i++) {
            if (t.is_infinite(f->neighbor(i))) {

                fi.n[i].first = -1;

                if (fi.maxR < fi.n[i].second) {
                    debug << "best neighbor of " << fi.i << " is infinite" << endl;
                    fi.maxR = fi.n[i].second;
                }
            } else {
                fi.n[i].first = f->neighbor(i)->info();
            }
        }
    }

    for (int i = 0; i < faces.size(); i++) {
        debug << "face " << i << ": " << faces[i].p0 << ", " << faces[i].p1 << ", " << faces[i].p2 << " neighbors: ";
        for (int j = 0; j < faces[i].n.size(); j++) {
            debug << faces[i].n[j].first;
            if (faces[i].n[j].first == -1) {
                debug << " (inf)";
            }
            debug << ", ";
        } debug << endl;
    }

    long changedCount = 1;
    vector<bool> changed(faceI, true);

    while (changedCount > 0) {
        changedCount = 0;
        for (long j = 0; j < changed.size(); j++) {
            if (!changed[j]) {
                continue;
            }
            changed[j] = false;
            debug << "visiting face " << j << endl;
            FaceInfo fi = faces[j];
            
            for (pair<long,KE::FT> neighborI : faces[j].n) {
                debug << "    visiting neighbor " << neighborI.first << endl;
                if (neighborI.first == -1) {
                    continue;
                }
                FaceInfo &neigh = faces[neighborI.first];


                if (neigh.maxR < neighborI.second && fi.maxR > neigh.maxR) {

                    neigh.maxR = minV(fi.maxR, neighborI.second);

                    debug << "      face " << neigh.i << " has new maxR" << neigh.maxR << endl;

                    changed[neigh.i] = true;
                    changedCount++;
                }
            }
        }
    }

    for (int i = 0; i < m ; i++) {

        Balloon b;
        long x,y;
        cin >> x >> y >> b.r;
        b.p = KE::Point_2(x,y);

        Vertex v = t.nearest_vertex(K::Point_2(x,y));
        KE::Point_2 nearest(v->point().x(), v->point().y());
        KE::FT sqdist = CGAL::squared_distance(nearest, b.p);

        KE::FT neededFly = (r + b.r) * (r + b.r) * 4;
        KE::FT neededWalk = neededFly;

        Face start = t.locate(K::Point_2(x,y));
    
        if(t.is_infinite(start)) {
            if (sqdist < (r + b.r) * (r + b.r)) {
                cout << "n";
                continue;
            } else {
                cout << "y";
                continue;
            }
        } 

                FaceInfo startF = faces[start->info()];


        debug << "Balloon " << i << " at " << b.p << " with r " << b.r << " has face: " << startF.i << endl;

        if (sqdist >= neededFly) {
            cout << "y";
            continue;
        } else if (sqdist < (r + b.r) * (r + b.r)) {
            debug << "too close";
            cout << "n";
            continue;
        } else if (startF.maxR >= (r + b.r) * (r + b.r) * 4) {
            cout << "y";
            continue;
        } else {
            debug << "default no";
            cout << "n";
            continue;
        }
    }

    cout << "\n";




}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    //t = 1;
    for (int i = 0; i < t; i++) {
        //debuge = i + 1 == 13;
        testcase();
    }
}