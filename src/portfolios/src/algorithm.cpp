#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>

#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

// choose exact rational type
#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq ET;
// solution type the solver provides
typedef CGAL::Quotient<ET> SolT;
// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;


using namespace std;

void testcase(int n, int m) {
	
	// by default, we have a nonnegative QP with Ax >= b
	Program qp (CGAL::LARGER, true, 0, false, 0); 
	
	for (int i = 0; i < n; i++) {
		int c,r;
		cin >> c >> r;
		
		qp.set_a(i, 0, c);
		qp.set_a(i, 1, r);
	}
	
	qp.set_r(0, CGAL::SMALLER);
	qp.set_r(1, CGAL::LARGER);
	
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			int cv;
			cin >> cv;
			
			if (j <= i) {
				qp.set_d(i,j,ET(2*cv));
			}
		}
	}
	
	assert(qp.is_nonnegative());
	
	for (int i = 0; i < m; i++) {
		int C,R,V;
		
		cin >> C >> R >> V;
		
		qp.set_b(0,C);
		qp.set_b(1,R);
		
		Solution s = CGAL::solve_nonnegative_quadratic_program(qp, ET());
		assert(s.solves_quadratic_program(qp));
		
		if (s.is_optimal() && s.objective_value() <= V) {
			cout << "Yes." << endl;
		} else {
			cout << "No." << endl;
		}
		
	}

}


int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int n,m;
    
    cin >> n >> m;
    
    while (n != 0) {
    	testcase(n,m);
    	cin >> n >> m;
    }

}
