#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>

using namespace std;


// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;

        //cerr << "adding edge " << from << "," << to << " cap: " << capacity << endl;
	}
};

int m,n,k,c;

int tI(int i, int j) {
    return 2*(m*i + j);
}

int tO(int i, int j) {
    return tI(i,j) + 1;
}

void testcase() {

    cin >> m >> n >> k >> c;

    Graph G(2*n*m + 2);
    EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);

    int source = 2*n*m;
    int target = 2*n*m + 1;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            eaG.addEdge(tI(i,j), tO(i,j), c);
            if (i + 1 < n) {
                eaG.addEdge(tO(i, j), tI(i + 1, j), 1);
                eaG.addEdge(tO(i + 1, j), tI(i, j), 1);
            }
            if (j + 1 < m) {
                eaG.addEdge(tO(i, j), tI(i, j + 1), 1);
                eaG.addEdge(tO(i, j + 1), tI(i, j), 1);
            }

            if (i == 0 || i == n - 1 || j == 0 || j == m - 1) {
                eaG.addEdge(tO(i,j), target, 1);
            }
            if ((i == 0 && (j == 0 || j == m - 1)) || (i == n - 1 && (j == 0 || j == m - 1) )){
                eaG.addEdge(tO(i,j), target, 1);
            }
        }
    }

    for (int i = 0; i < k; i++) {
        int x,y;
        cin >> y >> x;

        //cerr << x << "," << y << endl;

        eaG.addEdge(source, tI(x,y), 1);
    }

    long flow = boost::push_relabel_max_flow(G, source, target);
    cout << flow << endl;
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
