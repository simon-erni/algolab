#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

bool debug = false;

// 10,5  --> Überschuss im Team A == 5

//0,5
//10,15


// [0] : how much points in team A are too many
// [1] : how many people are used
vector<pair<long,int>> generate(const vector<int>& list, int k) {
	
	vector<pair<long, int>> combinations;
	
	vector<int> state(list.size(),0);
	
	while (true) {
		pair<long,int> combination = make_pair(0,0);
		
		for (int i = 0; i < list.size(); i++) {
			if (state[i] == 0) {
				//Team A
				combination.first += list[i];
			} else if (state[i] == 1) {
				//Team B
				combination.first -= list[i];
			} else {
				//No team!
				combination.second++;
			}
			

		}
		
		if (combination.second <= k) {
			combinations.push_back(combination);
		}
		
		//0000
		//0001
		//0002
		//0010
		//Generate next state, fast preferrably 
		state[0]++;
		for (int i = 0; i < state.size() - 1; i++) {
			if (state[i] == 3) {
				state[i + 1]++;
				state[i] = 0;
			}
		}
		
		//Overflow at the end
		if (state[state.size() - 1] > 2) {
			break;
		}
		
	
	}
	
	sort(combinations.begin(), combinations.end());
	
	return combinations;
	
}

void testcase() {

	int n,k;
	
	cin >> n >> k;
	
	vector<int> s1;
	vector<int> s2;

	int split = n/2 - 1;
	
	for (int i = 0; i < n; i++) {
		int s;
		cin >> s;
		
		if (i <= split) {
			s1.push_back(s);
		} else {
			s2.push_back(s);
		}
	}
	
	

	vector<pair<long,int>>  comb1 = generate(s1, k);
	vector<pair<long,int>>  comb2 = generate(s2, k);
	
	if (debug) {
		cerr << "FIRST: " << endl;

		for (int i = 0; i < comb1.size(); i++) {
			cerr << comb1[i].first << "," << comb1[i].second << endl;
		}

		cerr << "SECOND: " << endl;
		for (int i = 0; i < comb2.size(); i++) {
			cerr << comb2[i].first << "," << comb2[i].second << endl;
		}
	}
	
	long count = 0;
	for (int i = 0; i < comb1.size(); i++) {
		
		auto first = lower_bound(comb2.begin(), comb2.end(), make_pair(0 - comb1[i].first, 0));
		auto last = lower_bound(comb2.begin(), comb2.end(), make_pair(0 - comb1[i].first, k - comb1[i].second + 1));
		
		if (first == comb2.end()) {
			continue;
		}
		
		long firstI = first - comb2.begin();
		long lastI = last - comb2.begin();

		count += (last - first);
		
	}
	
	cout << count << endl;
	

}

int main() {

	ios_base::sync_with_stdio(false);
	
	int t;
	cin >> t;
	
	for (int i = 0; i < t; i++) {
		testcase();
	}

	return 0;
}