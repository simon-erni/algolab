// example: find the risk-optimal investment strategy
// in Swatch and Credit Suisse shares (data as in lecture)
#include <iostream>
#include <cassert>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

// choose exact rational type
#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq ET;
// solution type the solver provides
typedef CGAL::Quotient<ET> SolT;
// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

using namespace std;

// round up to next integer double
double ceil_to_double(const SolT& x)
{
  double a = std::ceil(CGAL::to_double(x));
  while (a < x) a += 1;
  while (a-1 >= x) a -= 1;
  return a;
}

double floor_to_double(const SolT& x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

void type1(int a, int b) {
	
	
	//Smaller, Lowerbound, 0, Upperbound, 0
	Program qp (CGAL::SMALLER, true, 0, false, 0); 

	int x = 0;
	int y = 1;
	
	qp.set_a(x, 0, ET(1));
	qp.set_a(y, 0, ET(1));
	qp.set_b(0, ET(4)); // <=
	
	
	qp.set_a(x, 1, ET(4));
	qp.set_a(y, 1, ET(2));
	qp.set_b(   1, ET(a*b)); // <=
	
	qp.set_a(x, 2, ET(-1));
	qp.set_a(y, 2, ET(1));
	qp.set_b(   2, ET(1)); // <=
	

	qp.set_d(x,x,ET(2*a));
	qp.set_c(y,ET(-b));
	
	Solution s = CGAL::solve_quadratic_program(qp, ET());
	
	if (s.is_optimal()) {
		cerr << "unrounded: " << CGAL::to_double(-s.objective_value()) << endl;
		cout << (int) floor_to_double(-s.objective_value()) << endl;

	} else if (s.is_infeasible()) {
		cout << "no" << endl;
	} else {
		cout << "unbounded" << endl;
	}

}

void type2(int a, int b) {
	
	// by default, we have a nonnegative QP with Ax >= b
	//Smaller, Lowerbound, 0, Upperbound, 0
	Program qp (CGAL::LARGER, false, 0, false, 0); 

	int x = 0;
	int y = 1;
	int z = 2;
	
	qp.set_a(x, 0, ET(-1));
	qp.set_b(   0, ET(0));
	
	qp.set_a(y, 1, ET(-1));
	qp.set_b(   1, ET(0));
	
	qp.set_a(z, 2, ET(1));
	qp.set_b(   2, ET(0));
	
	qp.set_a(x, 3, ET(1));
	qp.set_a(y, 3, ET(1));
	qp.set_b(   3, ET(-4));
	
	qp.set_a(x, 4, ET(4));
	qp.set_a(y, 4, ET(2));
	qp.set_a(z, 4, ET(1));
	qp.set_b(   4, ET(-a*b));
	
	qp.set_a(x, 5, ET(-1));
	qp.set_a(y, 5, ET(1));
	qp.set_b(   5, ET(-1));

	qp.set_d(x,x,ET(2*a));
	qp.set_d(z,z,ET(2*1));
	qp.set_c(y,ET(b));
	
	Solution s = CGAL::solve_quadratic_program(qp, ET());
	
	if (s.is_optimal()) {
	
		cerr << "unrounded: " << CGAL::to_double(s.objective_value()) << endl;
		cout << (int) ceil_to_double(s.objective_value()) << endl;

	} else if (s.is_infeasible()) {
		cout << "no" << endl;
	} else {
		cout << "unbounded" << endl;
	}

	
}

int main() {
	ios_base::sync_with_stdio(false);
	
	int p;
	
	cin >> p;

	while (p != 0) {
	
		int a, b;
		
		cin >> a >> b;

		if (p == 1) {
			type1(a,b);
		} else {
			type2(a,b);
		}
		
		cin >> p;
	}
	
	
	return 0;
}