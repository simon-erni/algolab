#import <iostream>
#import <vector>
#import <algorithm>
#import <utility>
#import <climits>
#import <cassert>

using namespace std;

void testCase() {
	
	int n;
	cin >> n;
	
	vector<pair<int,int>> boats(n); //Position, Length
	
	for (int i = 0; i < n; i++) {
		int l,p;
		cin >> l >> p;
		boats[i] = make_pair(p,l);
	}
	
	sort(boats.begin(), boats.end());
	
	int boatsCount = 0;
	int tempLastPos = INT_MAX;
	int fixedLastPos = INT_MIN; //We always take the first, it's kinda free.
	
	//Go through all the rings
	for (int i = 0; i < n; i++) {
			assert(tempLastPos >= fixedLastPos);

		//Refresh fixed pos (current position > tempLastPos)
		if (boats[i].first >= tempLastPos) {
			fixedLastPos = tempLastPos;
			boatsCount++;
			
			tempLastPos = INT_MAX;
			
			
		}
		
	
		//cerr << "Boatcount " << boatsCount << " Testing ring " << i << "with pos " << boats[i].first << " and length " << boats[i].second << " : templastpos: " << tempLastPos << " : fixedLastPos : " << fixedLastPos <<  " ";
		
		

		
		
		//Check if the pos is valid (considering fixed last pos) - the pos just needs to be bigger than the fixedLastPos
		if (boats[i].first >= fixedLastPos) {
			//Calculate the first position the boat can be moved to
			
			int nextPos;
			//Either its nahtlos
			if (fixedLastPos + boats[i].second >= boats[i].first) {
			
				nextPos = fixedLastPos + boats[i].second;
			
			} else { //Or there's a gap!
			
				nextPos = boats[i].first;
			
			}
			
			//cerr << "nextPos: " << nextPos;
			
			// Check if it is better than the templastpos
			
			if (nextPos < tempLastPos) {
				tempLastPos = nextPos;
				//cerr << " it's better than the templastpos";

			}
			
			
			//cerr << endl;
			
		
		}
			
		
	}
	
	if (boats[n-1].first <= tempLastPos) {
		boatsCount++;
	}
	
	cout << boatsCount << endl;
	
	
}

int main() {
	ios_base::sync_with_stdio(false);
	int t;
	cin >> t;
	
	for (int i = 0; i< t; i++) {
		testCase();
	}
	
	return 0;
}