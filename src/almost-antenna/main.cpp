#include <iostream>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Ray_2.h>
#include <CGAL/Point_2.h>
#include <CGAL/Segment_2.h>
#include <vector>

typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt KE;
typedef CGAL::Min_circle_2_traits_2<KE>  Traits;
typedef CGAL::Min_circle_2<Traits>      Min_circle;
typedef CGAL::Exact_predicates_inexact_constructions_kernel KI;


typedef KI::Point_2 PointI;
typedef KI::Ray_2 RayI;
typedef KI::Segment_2 SegmentI;


typedef KE::Circle_2 CircleE;
typedef KE::Point_2 PointE;
typedef KE::Ray_2 RayE;
typedef KE::Segment_2 SegmentE;



double floor_to_double(const KE::FT& x) {
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

double ceil_to_double(const KE::FT& x) {
  double a = std::ceil(CGAL::to_double(x));
  while (a < x) a += 1;
  while (a-1 >= x) a -= 1;
  return a;
}


void testCase(long n) {

	std::vector<PointE> points;
	
	for (int i = 0; i < n; i++) {
		long x,y;
		std::cin >> x >> y;
		
		PointE point(x,y);
		points.push_back(point);
	}
	
	
	Min_circle minCircle(points.begin(), points.end(), true);
	
	//If there are more than 3 points on the supporting boundary, we cannot leave out only 1, thus output directly:
	//If there is only 1 point, the radius is 0 
	if (minCircle.number_of_support_points() > 3 || minCircle.number_of_support_points() <= 1) {
		std::cout << std::fixed << std::setprecision(0) << ceil_to_double(CGAL::sqrt(minCircle.circle().squared_radius())) << std::endl;
		return;
	}
	
	//std::cout << "Before removing " << minCircle.number_of_support_points() << " points:" << std::endl;

		
	for (int i = 0; i < points.size(); i++) {
		//std::cout << "Point " << points[i].x() << " " << points[i].y() << std::endl;
	}
		
	//So now we know we have 2 or 3 points on the support boundary. We now compute the min circle again, without these two
	//std::vector<PointE> boundardPoints(minCircle.number_of_support_points());
	std::vector<PointE> innerPoints;
	
	for (int i = 0; i < n; i++) {
		
		PointE pointToAdd = points[i];
		bool toRemove = false;	
		for (int i = 0; i < minCircle.number_of_support_points() ; i++) {
	
			PointE pointToRemove = minCircle.support_point(i);
			
			if (pointToRemove.x() == pointToAdd.x() && pointToRemove.y() == pointToAdd.y()) {
				toRemove = true;
				break;
			}
		
		}
		
		if (!toRemove) {
			innerPoints.push_back(pointToAdd);
		}
	}
	
	/*
	std::cout << "Inner circle" << std::endl;
	
	for (auto it = innerPoints.begin(); it != innerPoints.end(); it++) {
		//std::cout << "Point " << it->x() << " " << it->y() << std::endl;
	}
	*/
	bool first = false;
	KE::FT minSquaredRadius;
	
	for (int i = 0; i < minCircle.number_of_support_points() ; i++) {

		Min_circle minInnerCircle(innerPoints.begin(), innerPoints.end(), true);

		PointE pointToRemove = minCircle.support_point(i);
		
		for (int j = 0; j < minCircle.number_of_support_points() ; j++) {
			PointE pointToAdd = minCircle.support_point(j);
			if (i != j) {
				minInnerCircle.insert(pointToAdd);
			}
		}
		
		if (!first || minInnerCircle.circle().squared_radius() < minSquaredRadius) {
			minSquaredRadius = minInnerCircle.circle().squared_radius();
			first = true;
		}
			


	}
	
	std::cout << std::fixed << std::setprecision(0) << ceil_to_double(CGAL::sqrt(minSquaredRadius)) << std::endl;

}


int main() {
	
	std::ios_base::sync_with_stdio(false);

    long n;

    std::cin >> n;

    while (n > 0) {
        testCase(n);
        std::cin >> n;
    }


}
