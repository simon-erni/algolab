#include <iostream>
#include <vector>
#include <algorithm>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>
#include <cassert>

#include <queue>
// BGL includes
#include <boost/tuple/tuple.hpp>

// Namespaces
// using namespace std;
// using namespace boost;


// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;
typedef	boost::graph_traits<Graph>::out_edge_iterator			OutEdgeIt;


using namespace std;

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
	}
};

void testcases() {
	int n,m;
	
	cin >> n >> m;
	
	// Create Graph and Maps
	Graph G(n);
	
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);

	// Add edges
	for (int i = 0; i < m; i++) {
		int a,b,c;
		cin >> a >> b >> c;
		
		eaG.addEdge(a,b,c);
	}


	// Calculate flow
	// If not called otherwise, the flow algorithm uses the interior properties
	// - edge_capacity, edge_reverse (read access),
	// - edge_residual_capacity (read and write access).
	
	long minFlow = 100000000L;
	long total = 0;
	long wasted = 0;
	Vertex minSource;
	
	Vertex minTarget;

	for (int i = 1; i < n; i++) {	
			total++;
			
			Vertex target = i;
			
			long flow = boost::push_relabel_max_flow(G, 0, target);
			if (flow < minFlow) {
				minFlow = flow;
				wasted = 0;
				minTarget = target;
				minSource = 0;
			} else {
				wasted++;
			}
	}


	for (int i = 1; i < n; i++) {	
			total++;
			
			Vertex source = i;
			
			long flow = boost::push_relabel_max_flow(G, source, 0);
			if (flow < minFlow) {
				minFlow = flow;
				wasted = 0;
				minTarget = 0;
				minSource = source;
			} else {
				wasted++;
			}
	}


	long flow = boost::push_relabel_max_flow(G, minSource, minTarget);
	assert(flow == minFlow);

	cerr << "n: "<< n << " m: " << m << " total: " << total <<" wasted: " << wasted << " necessary: " << total - wasted << endl;
	std::cout << minFlow << std::endl;
	
	// BFS to find vertex set S
	std::vector<int> vis(n, false); // visited flags
	std::queue<int> Q; // BFS queue (from std:: not boost::)
	vis[minSource] = true; // Mark the source as visited
	Q.push(minSource);
	long visitedCount = 0;
	while (!Q.empty()) {
		const int u = Q.front();
		Q.pop();
		OutEdgeIt ebeg, eend;
		for (boost::tie(ebeg, eend) = boost::out_edges(u, G); ebeg != eend; ++ebeg) {
			const int v = boost::target(*ebeg, G);
			// Only follow edges with spare capacity
			if (rescapacitymap[*ebeg] == 0 || vis[v]) continue;
			vis[v] = true;
			Q.push(v);
		}
	}

	// Output S
	for (int i = 0; i < n; ++i) {
		if (vis[i]) visitedCount++;
	}
		
	std::cout << visitedCount << " ";

	int curr = 0;
	for (int i = 0; i < n; ++i) {
		if (vis[i]) {
			std::cout << i;
			curr++;
			
			if (curr < visitedCount) {
				cout << " ";
			}
		}
		

	}
	std::cout << std::endl;
/**
	// Iterate over all the edges to print the flow along them
	EdgeIt ebeg, eend;
	for (tie(ebeg, eend) = edges(G); ebeg != eend; ++ebeg) {
		std::cout << "edge from " << boost::source(*ebeg, G) << " to " << boost::target(*ebeg, G) 
				  << " runs " << capacitymap[*ebeg] - rescapacitymap[*ebeg]
                                  << " units of flow (negative for reverse direction)." << std::endl;
	}
*/
}

// Main function to loop over the testcases
int main() {
	std::ios_base::sync_with_stdio(false);
	int T;
	cin >> T;
	for (; T > 0; --T)	testcases();
	return 0;
}