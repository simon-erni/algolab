// ALGOLAB BGL Tutorial 1
// Tutorial example problem

// Compile and run with one of the following:
// g++ -std=c++11 -O2 bgl-tutorial_problem.cpp -o bgl-tutorial_problem; ./bgl-tutorial_problem < bgl-tutorial_problem.in
// g++ -std=c++11 -O2 -I path/to/boost_1_58_0 bgl-tutorial_problem.cpp -o bgl-tutorial_problem; ./bgl-tutorial_problem < bgl-tutorial_problem.in

// Includes
// ========
// STL includes
#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/biconnected_components.hpp>

// Namespaces
using namespace std;
using namespace boost;

namespace boost {
	struct edge_component_t {
		enum {
			num = 555
		};
		typedef edge_property_tag kind;
	} edge_component;
}

// Directed graph with integer weights on edges.
typedef adjacency_list<vecS, vecS, undirectedS,
		no_property,
		property<edge_component_t, int>
		>					Graph;
typedef graph_traits<Graph>::vertex_descriptor		Vertex;	// Vertex type		
typedef graph_traits<Graph>::edge_descriptor		Edge;	// Edge type
typedef graph_traits<Graph>::edge_iterator		EdgeIt;	// Edge iterator
typedef graph_traits<Graph>::out_edge_iterator	OutEdgeIt;
typedef graph_traits<Graph>::vertex_iterator	VertexIt;

// Property map edge -> weight
// typedef property_map<Graph, edge_weight_t>::type	WeightMap;



// Functions
// ========= 
void testcases() {
	// Read and build graph
	int n, m; // 1st line: <vertex_no> <edge_no> <target>
	cin >> n >> m;
	
	Graph G(n);	// Creates an empty graph on V vertices
	for (int i = 0; i < m; i++) {
		int u, v;		// Each edge: <from> <to>
		cin >> u >> v;
		Edge e;	bool success;			
		tie(e, success) = add_edge(u,v, G);
		//cerr << "adding edge " << u << "," << " to the graph" << "\n";
	}
	
	
	property_map<Graph, edge_component_t>::type component = get(edge_component, G);
	int nBI = biconnected_components(G, component);
	

	EdgeIt eit, eend;
	vector<vector<Edge>> edgeMap(nBI);
	auto es = edges(G);
	for (auto eit = es.first; eit != es.second; ++eit) {
		//cerr << "adding edge" << source(*eit, G) << "," << target(*eit, G) << " to component "  << component[*eit] << endl;
		edgeMap[component[*eit]].push_back(*eit);
	}
	
	vector<pair<int,int>> finalEdges;
	
	for (int i = 0; i < edgeMap.size(); i++) {
		if (edgeMap[i].size() == 1)	 {
			int u,v;
			
			u = source(edgeMap[i][0], G);
			v = target(edgeMap[i][0], G);
			
			if (u < v) {
				finalEdges.push_back(make_pair(u,v));
			} else {
				finalEdges.push_back(make_pair(v,u));
			}
		} else {
			//cerr << "not size 0" << endl;
		}
		
	
	}
	
	sort(finalEdges.begin(), finalEdges.end());
	
	cout << finalEdges.size() << "\n";
	for (int i = 0; i < finalEdges.size(); i++){
		cout << finalEdges[i].first << " " << finalEdges[i].second << "\n";
	}
	
	
	
	
	
	
}

// Main function looping over the testcases
int main() {
	ios_base::sync_with_stdio(false);
	int T;	cin >> T;	// First input line: Number of testcases.
	while(T--)	testcases();
	return 0;
}

