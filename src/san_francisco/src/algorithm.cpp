#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <utility>

using namespace std;

#define debug if (debuge) cerr
const bool debuge = false;


#define trace if (tracee) cerr
const bool tracee = false;


void testcase() {
    long n,m,x,k;

    cin >> n >> m >> x >> k;

    debug << " n: " << n << " m: " << m << " k: " << k << "x: " << x << endl; 

    vector<vector<pair<long,long>>>g (n, vector<pair<long,long>>());
    vector<bool> sink(n, true);
    vector<long> newMax(n,0);
    vector<long> maxPoints(n, 0);

    vector<bool> visited(n, false);
    vector<bool> toVisit(n, false);

    for (int i = 0; i < m; i++) {
        long u,v,p;
        cin >> u >> v >> p;

        sink[u] = false;

        g[u].push_back(make_pair(v,p));

    }

    //Simulate!
    visited[0] = true;
    for (long move = 1; move <= k; move++) { //How many moves are made
        for (long u = 0; u < n; u++) {
            if (!visited[u]) {
                continue;
            }
            trace << "visiting " << u << endl;
            for (long v_i = 0; v_i < g[u].size(); v_i++) {
                long v = g[u][v_i].first;
                long w = g[u][v_i].second;

                if (maxPoints[u] + w > newMax[v]) {
                    newMax[v] = maxPoints[u] + w;
                    trace << "     new max for neighbour " << v << ": " << newMax[v] << endl;
                }

                if (newMax[v] >= x) {
                    cout << move << endl;
                    return;
                }

                if (sink[v]) { //Skip visiting v, it's a sink anyways
                    newMax[0] = std::max(newMax[v], newMax[0]);
                    toVisit[0] = true;
                } else {
                    toVisit[v] = true;
                }
            }
        }

        //Commit
        for (long u = 0; u < n; u++) {
            maxPoints[u] = std::max(newMax[u], maxPoints[u]);
            nexMax[u] = 0;
            visited[u] = toVisit[u];
            toVisit[u] = false;
        }        
        
    }

    cout << "Impossible" << endl;

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
     
    
    for (int i = 0; i < t ; i++) {
        testcase();
    }
}
