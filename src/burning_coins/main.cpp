#include <iostream>
#include <vector>
#include <cassert>

using namespace std;


void testCase() {

    int n;
    cin >> n;

    vector<int> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i];
    }

    vector<vector<int>> dp(n, vector<int>(n, 0));

    for (int width = 0; width < n; width++) {

        for (int left = 0; left + width < n; left++) {

            int right = left + width;
            assert(left <= right);
            assert(right < n);

            //Is it my turn or my opponents turn
            if ((n % 2 == 1 && width % 2 == 0) || (n % 2 == 0 && width % 2 == 1)) {
                // My turn, maximize the minimum

                if (left == right) {
                    dp[left][right] = v[left];
                } else {
                    int takeLeft = v[left] + dp[left + 1][right];
                    int takeRight = v[right] + dp[left][right - 1];

                    if (takeLeft > takeRight) {
                        dp[left][right] = takeLeft;
                    } else {
                        dp[left][right] = takeRight;
                    }
                }


            } else if (left != right) { //At zero width, I can gain nothing, you loose! good day sir.
                //Opponents turn, minimize the maximum
                int takeLeft = dp[left + 1][right];
                int takeRight = dp[left][right - 1];

                if (takeLeft < takeRight) {
                    dp[left][right] = takeLeft;
                } else {
                    dp[left][right] = takeRight;
                }

            }


        }


    }

    cout << dp[0][n - 1] << endl;
}


int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}