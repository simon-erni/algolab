#include <iostream>
#include <vector>

using namespace std;


void testCase() {


    int n;
    cin >> n;
    int sum = 0;

    int k;
    for (int i = 0; i < n; i++) {

        cin >> k;

        sum += k;

    }

    cout << sum << endl;


}


int main() {

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}