#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

void testCase() {
	int n,m,k;
	cin >> n >> m >> k;
	
	vector<int> d(n);

	vector<int> l;
	vector<int> r;

	
	for (int i = 0; i < n; i++) {
		cin >> d[i];
	}
	
	int left = 0;
	int right = 0;
	
	int curSum = d[0];
	while (right <= n - 1) {
	
		if (curSum == k) {
			l.push_back(left);
			r.push_back(right);
			
			curSum -= d[left];

			if (right + 1 >= n) {
				break;
			}
			
			curSum += d[right + 1];
			left++;
			right++;
					
		} else if (curSum < k) {
			
			if (right + 1 >= n) {
				break;
			}
			
			curSum += d[right + 1];
			right++;
						
		} else {
		
			curSum -= d[left];
			left++;
			
			if (left > right) {
				right = left;
				curSum = d[right];
			}	
		} 
	}	


	if (l.size() < m) {
		cout << "fail" << endl;
		return;
	}
	
	vector<int> next (l.size(), -1);
	
	int x = 0;
	
	for (int i = 0; i < next.size() - 1; i++) {

		while (r[i] >= l[x] && x < l.size()) {
			x++;
		}
		
		if (x >= l.size()) {
			break;
		}

		next[i] = x;
			
	}
	
	vector<vector<int>> dp(m + 1, vector<int>(l.size() + 1, 0));

	for (int a = 1; a <= m; a++) {
		
		for (int i = l.size() - 1; i >= 0; i--) {
			
			// Is this the element?
			int current = r[i] - l[i] + 1;
			int nextEl = 0; //Assume, I take this one for a, what would be the maximum of the next ?
			
			if (next[i] != -1 && a > 1) {
				nextEl = dp[a-1][next[i]];
			}
			
			if (nextEl == 0 && a > 1) {
				current = 0; //Not allowed to take one as this is the last element and nothing comes after it!
			}
			
			if (current + nextEl > dp[a][i+1]) {
				dp[a][i] = current + nextEl;
			} else {
				dp[a][i] = dp[a][i+1];
			}
			
		}
	}
	
	
	
	if (dp[m][0] == 0) {
		cout << "fail" << endl;
	} else {
		cout << dp[m][0] << endl;
	}
		
}

int main() {
	
	std::ios_base::sync_with_stdio(false);

    long t;
    std::cin >> t;

    for (int i = 0; i < t; i++) {
    	testCase();
    }


}
