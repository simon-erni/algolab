#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#include <set>
#include <cassert>
#include <utility>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>


typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;

using namespace std;

bool debug = false;


// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
	
		//cerr << "adding edge from " << from << " to " << to << " with capacity " << capacity << endl;
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
		
		if (debug) cerr << "adding edge from " << from << " to " << to << " with capacity " << capacity<< endl;
	}
};



void testcase() {	

	int n,m;
	
	cin >> n >> m;
		
	Graph G(n + m + 2);
	
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);
	
	for (int i = 0; i < m; i++) {
		
		int a, b, c;
		
		cin >> a >> b >> c;
		
		int player1 = a + m + 1;
		int player2 = b + m + 1;
		
		int game = i + 1;
		
		
		if (c == 1) {
			eaG.addEdge(0, player1, 1);
		} else if (c == 2) {
			eaG.addEdge(0, player2, 1);
		} else {
			eaG.addEdge(0,game,1);
			eaG.addEdge(game, player1, 1);
			eaG.addEdge(game, player2, 1);
		}
		
	}
	
	long total = 0;
	
	for (int i = 0; i < n; i++) {
	
		int from = i + m + 1;
		int to = n + m + 1;
		
		int capacity;
		cin >> capacity;
		
		total += capacity;
		
		eaG.addEdge(from, to, capacity);
		
	}
	
	if (total != m) {
		cerr << "total not matching with games" << endl;
		cout << "no" << endl;
		return;
	}
	
	Vertex src = 0;
	Vertex sink = n + m + 1;
	

	// Calculate flow
	// If not called otherwise, the flow algorithm uses the interior properties
	// - edge_capacity, edge_reverse (read access),
	// - edge_residual_capacity (read and write access).
	long flow = boost::push_relabel_max_flow(G, src, sink);

	if (flow != m) {
		cout << "no" << endl;
	} else {
		cout << "yes" << endl;
	}
	

}


int main() {
	
	ios_base::sync_with_stdio(false);
	
	int t;
	
	cin >> t;
	
	for (int i = 0; i < t; i++) {
		testcase();
	}

}