#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <cassert>

using namespace std;
int n;

vector<int> a;
vector<int> b;

vector<long> cA;
vector<long> cB;

void cache() {

    cA = vector<long>(n);
    cB = vector<long>(n);

    cA[0] = a[0];
    cB[0] = b[0];
    for (int i = 1; i < n; i++) {
        cA[i] = cA[i - 1] + a[i];
    }

    for (int i = 1; i < n; i++) {
        cB[i] = cB[i - 1] + b[i];
    }

}

long sum(int a1, int a2, int b1, int b2) {

    assert(a1 <= a2 && b1 <= b2 && a1 >= 0 && a1 < n && b1 >= 0 && b1 < n);

    int kA = a2 - a1 + 1;
    int kB = b2 - b1 + 1;

    int sA = cA[a2];
    int sB = cB[b2];

    if (a1 > 0) {
        sA -= cA[a1 - 1];
    }

    if (b1 > 0) {
        sB -= cB[b1 - 1];
    }

    return (sA - kA) * (sB - kB);

}

void testcase() {
    cin >> n;

    a = vector<int>(n);
    b = vector<int>(n);

    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }
    
    for (int i = 0; i < n; i++) {
        cin >> b[i];
    }

    cache();

    vector<vector<long>> dp(n, vector<long>(n, 0));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            
            dp[i][j] = sum(0,i,0,j);

            long minL = LONG_MAX;
            long minR = LONG_MAX;
            int l = i;
            int r = j;

            for (; l > 0; l--) {
               minL = std::min(minL, dp[l-1][r-1] + sum(l, i, r, j));
            }

            for (; r > 0; r--) {
                dp[i][j] = std::min(dp[i][j], dp[l-1][r-1] + sum(l, i, r, j));
            }
        }
    }

    cout << dp[n - 1][n - 1] << endl;

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testcase();
    }
    
}
