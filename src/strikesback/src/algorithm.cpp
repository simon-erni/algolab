#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <cassert>


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpq.h>

// choose exact integral type
typedef CGAL::Gmpq ET;

// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;

void testcase() {

    long a,s,b,e;
    cin >> a >> s >> b >> e;

    std::vector<K::Point_2> particles;
    vector<int> density;

    

    for (int i = 0; i < a; i++) {
        K::Point_2 p;
        cin >> p;
        int d;
        cin >> d;
        
        particles.push_back(p);
        density.push_back(d);
    }
    

    std::vector<K::Point_2> shooting;
    for (int i = 0; i < s; i++) {
        K::Point_2 p;
        cin >> p;
        shooting.push_back(p);
    }

    std::vector<K::Point_2> bounty;
    for (int i = 0; i < b; i++) {
        K::Point_2 p;
        cin >> p;
        bounty.push_back(p);
    }

    Triangulation t;
    t.insert(bounty.begin(), bounty.end());
    
    
    vector<vector<long>> d(s, vector<long>(a, 0)); //For shooter i, can target j be met?
    for (int i = 0; i < s; i++) {
        long maxDistance = LONG_MAX;

        if (b > 0) {
            K::Point_2 nearBounty = t.nearest_vertex(shooting[i])->point();
        
            maxDistance = CGAL::squared_distance(nearBounty, shooting[i]); //Needs to be really smaller
        }

        for (int j = 0; j < a; j++) {
            
            long distance = CGAL::squared_distance(shooting[i], particles[j]);
            if (distance < maxDistance) {
                distance = std::max((long) 1, distance);
                d[i][j] = distance;
                assert(distance > 0);
            }

        }
    }

    Program lp (CGAL::LARGER, true, 0, false, 0);

    for (int i = 0; i < a;i++) {
        for (int j = 0; j < s; j++) {
            if (d[j][i] == 0) {
                lp.set_a(j, i, 0);
                continue;
            }

            ET ef = ET(1)/ET(d[j][i]);
            cerr << "Particle " << i << " can be shot by " << j << " with energyfactor " << ef << endl;

            lp.set_a(j, i, ef);
        }

        lp.set_b(i, ET(density[i]));
    }

   

    for (int i = 0; i < s; i++) {
        lp.set_c(i,ET(1));
    }

    Solution sol = CGAL::solve_linear_program(lp, ET());
    assert(sol.solves_linear_program(lp));

    if (sol.objective_value() <= ET(e) && sol.is_optimal()) {
        cout << "y" << endl;
    } else {
        cout << "n" << endl;
    }





}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t ; i++) {
        testcase();
    }
}
