#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <utility>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

// BGL Graph definitions
// ===================== 
// Graph Type with nested interior edge properties for Cost Flow Algorithms
typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
    boost::property<boost::edge_capacity_t, long,
        boost::property<boost::edge_residual_capacity_t, long,
            boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
                boost::property <boost::edge_weight_t, long> > > > > Graph; // new!
// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type      EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type       ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor          Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator

using namespace std;


class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;


public:
    long source;
    long target;
    long maxBudget;
    pair<Edge,Edge> limitEdge;

    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    pair<Edge,Edge> addEdge(int u, int v, long cap, long cost) {
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = cap;
        weightmap[e] = cost; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = -cost; // new
        revedgemap[e] = rev_e; 
        revedgemap[rev_e] = e; 

        return make_pair(e,rev_e);
    }

    bool works(long limit) {
        capacitymap[limitEdge.first] = limit;
        capacitymap[limitEdge.second] = 0;
        ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);

        boost::successive_shortest_path_nonnegative_weights(G, source, target);
        long cost = boost::find_flow_cost(G);
        if (cost > maxBudget || rescapacitymap[limitEdge.first] > 0) {
            return false;
        }

        return true;

    }  

};



#define debug if (debuge) cerr

const bool debuge = false;


void testcase() {

    int n,m,b,source,dest;
    cin >> n >> m >> b >> source >> dest;

    Graph G(n + 1);
    EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
    EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
    ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
    ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
    EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);
    
    eaG.target = n;
    eaG.source = source;
    eaG.maxBudget = b;
    eaG.limitEdge = eaG.addEdge(dest,n, LONG_MAX, 0);

    for (int i = 0; i < m; i++) {
        
        int x,y,cost,cap;
        cin >> x >> y >> cost >> cap;

        eaG.addEdge(x,y,cap,cost);
    }

    long left = 0;
    long right = 1;

    while(eaG.works(right)) {
        debug << "finding right edge " << right << endl;
        left = right;
        right = right * 2;
    }

    while (left < right) {

        long m = (right - left) / 2 + left;
        debug << "trying " << m << endl;
        if (eaG.works(m)) {
            left = m; //Maybe m + 1 doesn't work, so.
        } else {
            right = m - 1;
        }

        if (right - left == 1) {
            if (eaG.works(right)) {
                left = right;
            } else {
                right = left;
            }
        }

    }

    cout << left << endl;


}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0 ; i < t; i++) {
        testcase();
    }
}
