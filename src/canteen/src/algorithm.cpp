#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <utility>

// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

// BGL Graph definitions
// ===================== 
// Graph Type with nested interior edge properties for Cost Flow Algorithms
typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
    boost::property<boost::edge_capacity_t, long,
        boost::property<boost::edge_residual_capacity_t, long,
            boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
                boost::property <boost::edge_weight_t, long> > > > > Graph; // new!
// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type      EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type       ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor          Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::edge_iterator           EdgeIt;

typedef boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map, weight map and reverse edge map
// ===============================================================
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long cap, long cost) {
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = cap;
        weightmap[e] = cost; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = -cost; // new
        revedgemap[e] = rev_e; 
        revedgemap[rev_e] = e; 
    }
};


using namespace std;
int n;

long k = 20;

int dayToI(int day) {

    return day + 1;
}

int priceToC(int price) {
    int newCost = k - price;

    assert(newCost >= 0);
    return newCost;
}

int cToPrice(int c) {
    int newCost = k - c;

    assert(newCost >= 0);
    return newCost;
}

void testcase() {
    
    cin >> n;    

    Graph G(n + 2);
    int source = 0;
    int target = n + 1;
    EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
    EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
    ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
    ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
    EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);

    long totalStudents = 0;

    for (int i = 0; i < n; i++) {
        int a,c;
        cin >> a >> c;

        eaG.addEdge(source, dayToI(i), a, c);

    }

    for (int i = 0; i <n ;i++) {
        int s,p;
        cin >> s >> p;

        totalStudents += s;

        eaG.addEdge(dayToI(i), target, s, priceToC(p));
    }

    for (int i = 0; i < n - 1; i++) {
        int v,e;
        cin >> v >> e;

        eaG.addEdge(dayToI(i), dayToI(i+1), v, e);

    }

    boost::successive_shortest_path_nonnegative_weights(G, source, target);

    long flow = 0;

    // Iterate over all edges leaving the source to sum up the flow values.
    OutEdgeIt e, eend;
    for(boost::tie(e, eend) = boost::out_edges(boost::vertex(target,G), G); e != eend; ++e) {
        flow += rescapacitymap[*e] - capacitymap[*e];

    }
    long cost = 0;

    EdgeIt edge, edgeEnd;
    for(boost::tie(edge, edgeEnd) = boost::edges(G); edge != edgeEnd; ++edge) {
        long f = capacitymap[*edge] - rescapacitymap[*edge];

        if (f <= 0) {
            continue;
        }

        if (boost::target(*edge, G) == target) {
            cost += cToPrice(weightmap[*edge]) * f;
        } else {
            cost -= weightmap[*edge] * f;
        }
    }



    if (flow >= totalStudents) {
        cout << "possible " << flow << " " << cost << endl;
    } else {
        cout << "impossible " << flow << " " << cost << endl;
    }




    

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    int t;
    cin >> t;

    for (int i = 0; i< t; i++) {
        testcase();
    }

}
