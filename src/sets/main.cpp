#include <iostream>
#include <set>

using namespace std;


void testCase() {

    int q;

    cin >> q;

    set<int> x;

    for (int i = 0; i < q; i++) {

        int a, b;

        cin >> a;
        cin >> b;

        if (a == 0) {
            x.insert(b);
        } else {

            auto found = x.find(b);

            if (found != x.end()) {
                x.erase(found);
            }
        }


    }

    if (x.empty()) {
        cout << "Empty" << endl;
    } else {

        for (auto i: x) {

            cout << i << " ";

        }

        cout << endl;

    }


}


int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }


    return 0;
}