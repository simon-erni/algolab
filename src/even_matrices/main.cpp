#include <iostream>
#include <vector>

using namespace std;


void testCase() {

    int n;
    cin >> n;

    vector<vector<int>> m(n, vector<int>(n, 0));


    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {

            int x;
            cin >> x;

            m[i][j] = x;


        }
    }

    int even_total = 0;


    for (int width = 1; width <= n; width++) {

        vector<vector<int>> odd(n, vector<int>(n, 0));
        vector<vector<int>> even(n, vector<int>(n, 0));

        for (int i = 0; i < n; i++) {
            int sum = 0;

            for (int k = 0; k < width; k++) {
                sum += m[i][k];
            }

            sum = sum % 2;

            for (int left = 0; left + width - 1 < n; left++) {

                if (left > 0) {
                    sum -= m[i][left - 1];
                    sum += m[i][left + width - 1];
                    sum = sum % 2;
                }

                if (sum == 0) {
                    even[i][left]++;
                } else {
                    odd[i][left]++;
                }


                if (i > 0) {

                    if (sum == 0) {
                        even[i][left] += even[i - 1][left];
                        odd[i][left] += odd[i - 1][left];
                    } else {
                        even[i][left] += odd[i - 1][left];
                        odd[i][left] += even[i - 1][left];
                    }

                }


            }

        }


        for (int i = 0; i < n; i++) {

            for (int left = 0; left + width - 1 < n; left++) {

                even_total += even[i][left];

            }
        }

    }


    cout << even_total << endl;

}


int main() {

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}