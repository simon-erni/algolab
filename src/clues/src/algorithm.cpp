#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Point_set_2.h>
#include <utility>
#include <queue>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/bipartite.hpp>
#include <boost/graph/connected_components.hpp>

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::no_property		// interior properties of edges
		>					Graph;
typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<Graph>::vertex_iterator VertexIt;
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<std::pair<long,long>,K> Vb;
typedef CGAL::Triangulation_face_base_2<K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef Triangulation::Edge_circulator Edge_circulator;

typedef Triangulation::Vertex_iterator  Vertex_iterator;
typedef Triangulation::Vertex_circulator  Vertex_circulator;

typedef Triangulation::Face_iterator  Face_iterator;

typedef Triangulation::Vertex_handle  TVertex;


typedef K::Circle_2 C;

typedef CGAL::Point_set_2<K,Tds> PointSet;

typedef K::Point_2 P;

using namespace boost;

typedef std::vector <default_color_type> partition_t;
typedef typename property_map <Graph, vertex_index_t>::type index_map_t;
typedef iterator_property_map <partition_t::iterator, index_map_t> partition_map_t;

#define debug if (debuge) cerr
const bool debuge = false;

using namespace std;

void notBipartite(long m) {
    cerr << "not bipartite!!!" << endl;
    for (int i = 0; i < m; i++) {
        P x1,x2;
        cin >> x1 >> x2;
        cout << "n";
    }
    cout << endl;
}

void testcase() {
    long n,m;
    K::FT r;
    cin >> n >> m >> r;

    cerr << "n: " << n << "m: " << m << "r: " << r << endl;

    r = r * r;
    vector<P> stations(n);
    for (int i = 0; i < n; i++) {
        cin >> stations[i];
    }

    PointSet t;
    t.insert(stations.begin(), stations.end());
    
    Graph G(n);
    
    long k = 0;
    vector<P> points(n);
    for (Vertex_iterator vit = t.finite_vertices_begin(); vit != t.finite_vertices_end(); vit++) {
        vit->info().first = k;
        vit->info().second = -1;
        points[k] = vit->point();
        k++;
    }

    for (Edge_iterator e = t.finite_edges_begin(); e != t.finite_edges_end(); e++) {
        Triangulation::Vertex_handle v1 = e->first->vertex((e->second + 1) % 3);
        Triangulation::Vertex_handle v2 = e->first->vertex((e->second + 2) % 3);
        if (CGAL::squared_distance(v1->point(), v2->point()) <= r) {
            boost::add_edge(v1->info().first, v2->info().first, G);
        }
    }
    

    if (!boost::is_bipartite(G)) {
        notBipartite(m);
        return;
    }

    partition_t partition (num_vertices(G));
    partition_map_t partition_map (partition.begin (), get(vertex_index, G));

    is_bipartite(G, get(vertex_index, G), partition_map);

    PointSet t1,t2;
    vector<P> t1v;
    vector<P> t2v;
    VertexIt vertex_iter,vertex_end;
    for (boost::tie (vertex_iter, vertex_end) = vertices (G); vertex_iter != vertex_end; ++vertex_iter) {
        if (get (partition_map, *vertex_iter) == color_traits <default_color_type>::white ()) {
            t1v.push_back(points[*vertex_iter]);
        } else {
            t2v.push_back(points[*vertex_iter]);
        }
    }

    t1.insert(t1v.begin(), t1v.end());
    t2.insert(t2v.begin(), t2v.end());

    for (Edge_iterator e = t1.finite_edges_begin(); e != t1.finite_edges_end(); e++) {
        P p1 = e->first->vertex((e->second + 1) % 3)->point();
        P p2 = e->first->vertex((e->second + 2) % 3)->point();
        if (CGAL::squared_distance(p1,p2) <= r) {
            notBipartite(m);
            return;
        }
    }

    for (Edge_iterator e = t2.finite_edges_begin(); e != t2.finite_edges_end(); e++) {
        P p1 = e->first->vertex((e->second + 1) % 3)->point();
        P p2 = e->first->vertex((e->second + 2) % 3)->point();
        if (CGAL::squared_distance(p1,p2) <= r) {
            notBipartite(m);
            return;
        }
    }

    std::vector<int> componentmap(n);
    boost::connected_components(G, boost::make_iterator_property_map(componentmap.begin(), boost::get(boost::vertex_index, G)));

    for (int i = 0; i < m; i++) {

        P p1,p2;
        cin >> p1 >> p2;

        TVertex v1 = t.nearest_vertex(p1);
        TVertex v2 = t.nearest_vertex(p2);

        debug << "Nearest Point 1: " << v1->point();
        debug << " Nearest Point 2: " << v2->point();

        if (CGAL::squared_distance(p1,p2) <= r) {
            cout << "y";
            debug << endl;
            continue;
        }

        if (CGAL::squared_distance(v1->point(), p1) > r || CGAL::squared_distance(v2->point(), p2) > r) {
            cout << "n";
            debug << endl;
            continue;
        }

        if (componentmap[v1->info().first] == componentmap[v2->info().first]) {
            cout << "y";
        } else {
            debug << "not same component";
            cout << "n";
        }

        debug << endl;
        
    }

    cout << endl;

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
