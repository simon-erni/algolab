#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/cycle_canceling.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/successive_shortest_path_nonnegative_weights.hpp>
#include <boost/graph/find_flow_cost.hpp>

typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
    boost::property<boost::edge_capacity_t, long,
        boost::property<boost::edge_residual_capacity_t, long,
            boost::property<boost::edge_reverse_t, Traits::edge_descriptor,
                boost::property <boost::edge_weight_t, long> > > > > Graph; // new!
// Interior Property Maps
typedef boost::property_map<Graph, boost::edge_capacity_t>::type      EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_weight_t >::type       EdgeWeightMap; // new!
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type       ReverseEdgeMap;
typedef boost::graph_traits<Graph>::vertex_descriptor          Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor            Edge;
typedef boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIt; // Iterator
class EdgeAdder {
    Graph &G;
    EdgeCapacityMap &capacitymap;
    EdgeWeightMap &weightmap;
    ReverseEdgeMap  &revedgemap;

public:
    EdgeAdder(Graph &G, EdgeCapacityMap &capacitymap, EdgeWeightMap &weightmap, ReverseEdgeMap &revedgemap) 
        : G(G), capacitymap(capacitymap), weightmap(weightmap), revedgemap(revedgemap) {}

    void addEdge(int u, int v, long cap, long cost) {
        Edge e, rev_e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(u, v, G);
        boost::tie(rev_e, boost::tuples::ignore) = boost::add_edge(v, u, G);
        capacitymap[e] = cap;
        weightmap[e] = cost; // new!
        capacitymap[rev_e] = 0;
        weightmap[rev_e] = -cost; // new
        revedgemap[e] = rev_e; 
        revedgemap[rev_e] = e; 
    }
};
using namespace std;

#define debug if (debuge) cerr
#define trace if (tracee) cerr


const bool debuge = true;
const bool tracee = true;
long maxCost;

struct E{
    long u,v,w;
};
long ccc(int t1, int t2, long cost) {
    assert(t2 > t1);
    long result = maxCost*(t2-t1) - cost;
    assert(result >= 0);
    return result;
}

void testcase() {
    int n,m,l;
    cin >> n >> m >> l;

    Graph G(n + 2);
    Vertex source = n;
    Vertex target = n + 1;

    EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
    EdgeWeightMap weightmap = boost::get(boost::edge_weight, G);
    ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
    ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
    EdgeAdder eaG(G, capacitymap, weightmap, revedgemap);


    vector<E> edges;

    for (int i = 0; i < m; i++) {
        E e;
        cin >> e.u >> e.v >> e.w;
        maxCost = std::max(e.w, maxCost);
        edges.push_back(e);
    }

    for (int i = 0; i < m ; i++) {
        eaG.addEdge(edges[i].u,edges[i].v,1,ccc(edges[i].u,edges[i].v,edges[i].w));
    }

     

    for (int i = 0; i < n - 1; i++) {
        eaG.addEdge(i, i + 1, l, ccc(i, i + 1, 0));
    }

    eaG.addEdge(source, 0, l, 0);
    eaG.addEdge(n - 1, target, l, 0);

    boost::successive_shortest_path_nonnegative_weights(G, source, target);
    long cost = boost::find_flow_cost(G);

    cout << maxCost * l * (n-1) - cost << endl;
    
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
