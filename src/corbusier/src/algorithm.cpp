#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <cassert>

using namespace std;

bool testcase() {
    long n,target,k;
    cin >> n >> target >> k;

    assert(n <= 1000);
    assert(target <= 1000);
    assert(k <= 1000);
    assert(target < k);

    cerr << "n: " << n << " target: " << target << " k: " << k << endl;
    vector<bool> r(k, false);
    vector<bool> rnew(k, false);

    vector<long> d(n,0);

    for (int i = 0; i < n; i++) {
        long disk;
        cin >> disk;
        disk = disk % k;
        d[i] = disk;
    }

    for (int i = 0; i < n; i++) {
        if (d[i] == target) {
            return true;
        }
        rnew[d[i]] = true;
        for (int j = 0; j < k; j++) {
            if (r[j]) {
                rnew[(j + d[i]) % k] = true;
            }
        }

        for (int j = 0; j < k; j++) { //Commit
            if (rnew[j]) {
                r[j] = true;
            }
            rnew[j] = false;
        }

        if (r[target]) {
            return true;
        }
        
    }

    return false;

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        if (testcase()) {
            cout << "yes" << endl;
        } else {
            cout << "no" << endl;
        }
    }
}
