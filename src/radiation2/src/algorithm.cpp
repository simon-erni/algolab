#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <cassert>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Point_set_2.h>


typedef CGAL::Exact_predicates_exact_constructions_kernel K;
typedef CGAL::Point_set_2<K> Triangulation;

typedef Triangulation::Finite_faces_iterator  Face_iterator;
typedef Triangulation::Vertex_handle  VH;

typedef K::Point_2 P;
typedef K::Circle_2 C;

using namespace std;

long inter(C &c, Triangulation &t) {
    std::list<VH> LV;
    t.range_search(c, std::back_inserter(LV));

    long count = 0;
    for (VH v : LV) {
        if (CGAL::squared_distance(c.center(), v->point()) < c.squared_radius()) {
            count++;
        }
    }

    return count;
}

void testcase() {
    int m,n;
    cin >> m >> n;

    vector<P> healthy(m);
    vector<P> cancer(n);
    for (int i = 0; i < m; i++) {
        long x,y;
        cin >> x >> y;
        healthy[i] = P(x,y);
    }

    for (int j = 0; j < n; j++) {
        long x,y;
        cin >> x >> y;
        cancer[j] = P(x,y);
    }
    
    random_shuffle(healthy.begin(), healthy.end());
    random_shuffle(cancer.begin(), cancer.end());

    Triangulation tHealthy,tCancer;
    tHealthy.insert(healthy.begin(), healthy.end());
    tCancer.insert(cancer.begin(), cancer.end());

    long maxC = 0;
    for(auto it = tHealthy.finite_faces_begin(); it != tHealthy.finite_faces_end(); it++) {
        P p0,p1,p2;
        p0 = it->vertex(0)->point();
        p1 = it->vertex(1)->point();
        p2 = it->vertex(2)->point();
        C c(p0,p1,p2);
//        cerr << "looking at face " << p0 << " , " << p1 << " , " << p2 << " with circle: " << c.center() << " r^2: " << c.squared_radius() << endl;
        
        
        maxC = std::max(inter(c, tCancer), maxC);
/*
        cerr << "eliminated cancer cells: ";
        for (VH vh : LV) {
            cerr << vh->point() << " , ";
        }
        cerr << endl;
*/
    }

    for (auto it = tHealthy.finite_edges_begin(); it != tHealthy.finite_edges_end(); it++) {
        P p0 = it->first->vertex((it->second + 1) % 3)->point();
        P p1 = it->first->vertex((it->second + 2) % 3)->point();

        C c(p0,p1);

        maxC = std::max(inter(c, tCancer), maxC);

    }

    cout << maxC << endl;
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
    
}
