#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>

#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

// choose exact integral type
#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq ET;

// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

#define trace(x) debug << #x << " = " << x << endl

#define debug if (debuge) cerr

const bool debuge = false;

using namespace std;

void testcase(const int n) {
	
	int d;
	cin >> d;
	
	vector<vector<long>> a(n,vector<long>(d));
	vector<long> b(n);
	
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < d; j++) {
			cin >> a[i][j];
		}
		cin >> b[i];
	}
	
	Program lp (CGAL::SMALLER, false, 0, false, 0); 
	

	
	vector<long> norm (n,0);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < d ; j++) {
			norm[i] += a[i][j] * a[i][j];
		}
		/*if (norm[i] == 0) {
			cout << "none" << endl;
			return;
		}*/	
		norm[i] = sqrt(norm[i]);
		assert(norm[i] > 0);
	}
	
	long c = 0;
	
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < d; j++) {
			lp.set_a(j, c, a[i][j]);
		}
		
		lp.set_a(d, c, norm[i]);
		lp.set_b(c, b[i]);
		c++;
	}

	lp.set_l(d, true, 0);
	
	lp.set_c(d, ET(-1));
	
	  // solve the program, using ET as the exact type
  	Solution s = CGAL::solve_linear_program(lp, ET());	
  	assert (s.solves_linear_program(lp));
  	
  	debug << s;

  	if (s.is_infeasible()) {
		cout << "none" << endl;
  		return;
  	}
  	
  	if (s.is_unbounded()) {
  		cout << "inf" << endl;
  		return;
  	}
  	
  	assert(s.is_optimal());
  	
  	cout << -ceil(CGAL::to_double(s.objective_value())) << endl;

	// output solution
	
	
	
	
	
	

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
	
	int n ;
	cin >> n;
		
	while (n != 0) {
		testcase(n);
		cin >> n;
	}
	    
}
