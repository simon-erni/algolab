#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <queue>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/range_search_delaunay_2.h>
#include <CGAL/Point_set_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>

#include <boost/graph/filtered_graph.hpp>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>

bool debug = false;

#define DEBUG if (debug) cerr

using namespace std;
using namespace boost;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_2<int, K> Vb;
typedef CGAL::Triangulation_data_structure_2<Vb>	Tds;
typedef CGAL::Delaunay_triangulation_2<K, Tds>  Triangulation;
typedef Triangulation::Finite_faces_iterator  Face_iterator;
typedef Triangulation::All_vertices_iterator All_vertices_iterator;
typedef Triangulation::Finite_vertices_iterator Finite_vertices_iterator;

typedef Triangulation::Edge_circulator Edge_circulator;
typedef Triangulation::Vertex_circulator Vertex_circulator;

typedef Triangulation::Vertex_handle  Vertex_handle;
typedef K::Point_2                           Point_2;
typedef K::Circle_2				Circle_2; 
typedef pair<Point_2, Point_2> Mission;

void resetInfo(Triangulation &t) {
	
	Finite_vertices_iterator eit = t.finite_vertices_begin();
	
	for ( ; eit !=t.finite_vertices_end(); ++eit) {
		eit->info() = 0;
	}

	
}

/** Build the connected components using the given P on the delaunay triangulation **/
void updateTriangulation(Triangulation &t, long p) {
	resetInfo(t);
	
	Finite_vertices_iterator eit = t.finite_vertices_begin();

	int cur = 1;
	for ( ; eit !=t.finite_vertices_end(); ++eit) {
		//DEBUG << "visiting a vertex: " << eit->point() << endl;
		
		std::queue<Vertex_handle> q;
		q.push(eit);
		
		while (!q.empty()) {
			Vertex_handle v = q.front();
			
			q.pop();
			if (v->info() != 0) {
				continue;
			}
			
			v->info() = cur;
			
			//DEBUG << v->point() << " has id " << cur << endl;
			
			Vertex_circulator vc = t.incident_vertices(v);
			do {
			
				if (t.is_infinite(vc)) {
					continue;
				}
				
				if (squared_distance(v->point(), vc->point()) > p) {
					continue;
				}
				
				q.push(vc);
			
			} while (++vc != t.incident_vertices(v));
			
			
			
		}
		
		cur++;
		
	
	}
}

/*
	Returns for each mission individually if it will be covered or not for the given p
*/
vector<bool> works(Triangulation &t, long p, vector<Mission> &m) {
	
	updateTriangulation(t,p);
	
	vector<bool> working;
	working.reserve(m.size());
	for (int i = 0; i < m.size(); i++) {
		
		Point_2 q = m[i].first;
		Point_2 r = m[i].second;
		
		Vertex_handle q1 = t.nearest_vertex(q);
		Vertex_handle r1 = t.nearest_vertex(r);
		
		if (squared_distance(q, q1->point()) > p / K::FT(4) || squared_distance(r,r1->point()) > p / K::FT(4) || q1->info() != r1->info()) {
			working.push_back(false);
		} else {
			working.push_back(true);
		}
		
	}
	
	return working;
	
	
}

/**
Calculates the minimum P required to cover all starting & endpoints
**/
long minP(Triangulation &t, vector<Mission> &m) {

	long p = 0;
	for (int i = 0; i < m.size(); i++) {
		Point_2 q = m[i].first;
		Point_2 r = m[i].second;
		
		Vertex_handle q1 = t.nearest_vertex(q);
		Vertex_handle r1 = t.nearest_vertex(r);
		
		if (squared_distance(q, q1->point()) > p) {
			p = squared_distance(q, q1->point());
		} 
		
		if (squared_distance(r,r1->point()) > p) {
			p = squared_distance(r,r1->point());
		}
	}
	
	return p * 4;
}

/**
	Tests if a p satisfies all given start & end vertices
**/
bool allWorking(Triangulation &t, long p, vector<pair<Vertex_handle,Vertex_handle>> &m) {

	updateTriangulation(t,p);
	
	for (int i = 0; i < m.size(); i++) {
		
		if (m[i].first->info() != m[i].second->info()) {
			return false;
		} 
		
	}
	
	return true;
}


/**
	Given a starting estimate p (doesn't have to be correct at all) calculates the optimal p to cover all supplied missions.
*/
long findWorkingForMissions(Triangulation &t, long p, vector<Mission> &missions, bool initialPAllWorking = false) {
	
	long minimumP = minP(t, missions);
	
	if (p < minimumP) {
		p = minimumP;
	}
	
	vector<pair<Vertex_handle, Vertex_handle>> missionV;
	
	for (int i = 0; i < missions.size(); i++) {
		Point_2 q = missions[i].first;
		Point_2 r = missions[i].second;
		
		missionV.push_back(make_pair(t.nearest_vertex(q), t.nearest_vertex(r)));
	}
	
	long left = minimumP;
	long right = p;
	
	DEBUG << "starting with left " << left << " and right " << right << endl;

	if (!initialPAllWorking) {
		while (!allWorking(t, right, missionV)) {
			DEBUG << "not all working, trying with double" << endl;
			left = right;
			right *= 2;		
		}
	}

	DEBUG << "Starting binary search between LEFT: " <<  left << " Right: " << right << endl;
	
	while (left < right) {
		
		long middle = (right - left) / 2 + left;
	

		DEBUG << "L: " << left << " R: " << right << " M: " << middle << endl;
		if (!allWorking(t, middle, missionV)) {
			left = middle + 1;
		} else {
			right = middle;
		}
		
	}
	
	return left;

}

void testcase() {

	size_t n;
	cin >> n;

	long m;
	cin >> m;
	
	long p;
	cin >> p;
		
	vector<Point_2> pts;
	
	
	
	pts.reserve(n);
	for (size_t i = 0; i < n; i++) {
		Point_2 q;
		cin >> q;
		pts.push_back(q);
	}
	
	//sort(pts.begin(), pts.end());

	Triangulation t;
	t.insert(pts.begin(), pts.end());
	
	vector<Mission> missions;
	missions.reserve(m);
	
	for (int i = 0; i < m; i++) {
		
		Point_2 q,r;
		cin >> q >> r;
		
		missions.push_back(make_pair(q,r));
	}
	
	
	vector<bool> workingMissions = works(t, p, missions);
	vector<Mission> minMissions;
	vector<bool> minWorking;
	
	bool areAllWorking = true;
	
	for (int i = 0; i < m; i++) {
		
		if (workingMissions[i]) {
			cout << "y";
			minMissions.push_back(missions[i]);
			minWorking.push_back(true);
		} else {
			cout << "n";
			areAllWorking = false;
		}
	
		
	}
	cout << endl;

	cout << findWorkingForMissions(t,p,missions,areAllWorking) << endl;
	
	cout << findWorkingForMissions(t,p,minMissions,true) << endl;
	
}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    int t;
    cin >> t;
    
    for (int i = 0; i < t; i++) {
    	testcase();
    }
    
}
