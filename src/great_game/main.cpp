#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

void testCase() {

    int n, m, r, b;
    cin >> n >> m >> r >> b;
    
    b--; r--; //Start all at 0
    
    vector<vector<int>> g(n);

    vector<int> maxmin(n, 0);
    vector<int> minmax(n, 0);

    for (int i = 0; i < m; i++) {
        int x, y;
        cin >> x >> y;
        
        x--; y--;        //Start all at 0

        g[x].push_back(y);
    }

    //n - 2, as n -1 is target and is already initialized with 0,0
    for (int i = n - 2; i >= 0; i--) {

        int min = 2147483647;

        for (auto neighbour : g[i]) {
            if (maxmin[neighbour] + 1 < min) {
                min = maxmin[neighbour] + 1;
            }
        }

        minmax[i] = min;

        int max = 0;

        for (auto neighbour : g[i]) {
            if (minmax[neighbour] + 1 > max) {
                max = minmax[neighbour] + 1;
            }
        }

        maxmin[i] = max;

    }


    if (minmax[b] < minmax[r] || (minmax[b] == minmax[r] && minmax[b] % 2 == 0)) {
        cout << "1" << endl;

    } else {
        cout << "0" << endl;
    }
}

int main() {

    ios_base::sync_with_stdio(false);

    int t;

    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

}