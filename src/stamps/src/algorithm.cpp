#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>


// choose exact integral type
#include <CGAL/Gmpq.h>
typedef CGAL::Gmpq ET;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

typedef K::Point_2 P;
typedef K::Segment_2 S;

typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

using namespace std;

void testcase() {
    int l,s,w;
    cin >> l >> s >> w;

    vector<P> lamps;
    lamps.reserve(l);
    vector<P> stamps;
    stamps.reserve(s);
    vector<S> walls;
    walls.reserve(w);

    vector<long> maxIntensity;
    maxIntensity.reserve(s);

    for (int i = 0; i < l; i++) {
        long x,y;
    
        cin >> x >> y;

        P p(x,y);
        lamps.push_back(p);

    }
    for (int i = 0; i < s; i++) {
        long x,y,w;
    
        cin >> x >> y >> w;

        P p(x,y);
        stamps.push_back(p);
        maxIntensity.push_back(w);
    }
    for (int i = 0; i < w; i++) {
        long x,y,a,b;
    
        cin >> x >> y >> a >> b;

        P p1(x,y);
        P p2(a,b);
        S s(p1,p2);
        walls.push_back(s);
    }

    random_shuffle(walls.begin(), walls.end()); //Defeat adversial input

    vector<vector<ET>> fact(l, vector<ET>(s,0));
    for (int i = 0; i < l; i++) {
        for (int j = 0; j < s; j++) {
            S s(lamps[i], stamps[j]);

            bool canReach = true;

            for (int k = 0; k < w; k++) {
                if (CGAL::do_intersect(s, walls[k])) {
                    canReach = false;
                    break;
                }
            }

            if (!canReach) {
                continue;
            }

            fact[i][j] = CGAL::inverse(CGAL::squared_distance(lamps[i], stamps[j]));
        }
    }

    Program lp (CGAL::SMALLER, true, 1, true, 1<<12); 
    int c = 0;
    for (int i = 0; i < s; i++) {
        for (int j = 0; j < l; j++) {
            lp.set_a(j, c, fact[j][i]);
        }

        lp.set_b(c, maxIntensity[i]);
        c++;
    }
    for (int i = 0; i < s; i++) {
        for (int j = 0; j < l; j++) {
            lp.set_a(j, c, fact[j][i]);
        }

        lp.set_b(c, 1);
        lp.set_r(c, CGAL::LARGER);
        c++;
    }

    Solution solution = CGAL::solve_linear_program(lp, ET());
    assert (solution.solves_linear_program(lp));

    if (!solution.is_infeasible()) {
        cout << "yes" << endl;
    } else {
        cout << "no" << endl;
    } 



}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
