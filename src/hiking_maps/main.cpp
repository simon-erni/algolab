#include <iostream>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Ray_2.h>
#include <CGAL/Point_2.h>
#include <CGAL/Triangle_2.h>
#include <CGAL/Segment_2.h>
#include <CGAL/Line_2.h>

#include <vector>
#include <cassert>
typedef CGAL::Exact_predicates_exact_constructions_kernel KE;
typedef CGAL::Exact_predicates_inexact_constructions_kernel KI;


typedef KI::Point_2 PointI;
typedef KI::Line_2 LineI;
typedef KI::Triangle_2 TriangleI;
typedef KI::Ray_2 RayI;
typedef KI::Segment_2 SegmentI;


typedef KE::Point_2 PointE;
typedef KE::Ray_2 RayE;
typedef KE::Segment_2 SegmentE;

using namespace std;



double floor_to_double(const KE::FT& x) {
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}


bool inTriangle(const vector<PointI> &triangle, const PointI &point) {

	auto a = CGAL::orientation(triangle[0], triangle[1], point);

	if (a != CGAL::COLLINEAR && a != CGAL::orientation(triangle[0], triangle[1], triangle[2])) {
		return false;
	}
	
	auto b = CGAL::orientation(triangle[2], triangle[3], point);

	if ( b != CGAL::COLLINEAR && b != CGAL::orientation(triangle[2], triangle[3], triangle[0])) {
		return false;
	}
	
	auto c = CGAL::orientation(triangle[4], triangle[5], point);

	if (c != CGAL::COLLINEAR && c != CGAL::orientation(triangle[4], triangle[5], triangle[0])) {
		return false;
	}

	return true;


}

void testCase() {

   int m,n;
   cin >> m >> n;
   
   vector<SegmentI> legs;

   
   int xB,yB;
   
   cin >> xB >> yB;
      
   for (int i = 1; i < m; i++) {
   
   		int x,y;
   		cin >> x >> y;
   				
		SegmentI segment(PointI(xB,yB),PointI(x,y));
		legs.push_back(segment);
		
		xB = x;
		yB = y;
   }
   
   vector<int> triangleCount(m-1,0);
   
   vector<vector<int>> maps(n, vector<int>());
	for (int i = 0; i < n; i++) {

   		vector<PointI> points;
   		for (int i = 0; i < 6; i++) {
   			int x,y;
   			cin >> x >> y;
   			
   			points.push_back(PointI(x,y));
   		}
   		
   		   		
   		for (int k = 0; k < legs.size(); k++) {
   			if (inTriangle(points, legs[k].source()) && inTriangle(points, legs[k].target())) {
   				maps[i].push_back(k);
   				//triangleCount[k]++;
   			}
   		}
   }
   
   /*
   cerr << "Total segments: " << legs.size() << endl;
   
	for (int i = 0; i < maps.size(); i++) {
		cerr << "Map " << i << " : ";
		for (int k = 0; k < maps[i].size(); k++) {
			cerr << maps[i][k] << ", ";
		}
		
		cerr << endl;
	}
	*/
	
   
   //Inclusive boundaries
   int left = 0;
   int right = 0;
   int min = n - 1;
   
   
   for (int i = 0; i < maps[0].size(); i++) {
   	triangleCount[maps[0][i]]++;
   }
	while (right < n) {
	
		//cerr << "Trying " << left << "," << right << ": " ;
	
		assert(left <= right);
	
		bool allowed = true;
		// Try to remove the current left part
		for (int i = 0; i < triangleCount.size(); i++) {
			if (triangleCount[i] < 1) {
				allowed = false;
				break;
				
			}
		
		}
		
		if (!allowed) { // Move to the right, we don't have enough
			right++;
			//cerr << "Not allowed" << endl;
			
			if (right >= n) {
				break;
			}
			
			for (int i = 0; i < maps[right].size(); i++) {
				triangleCount[maps[right][i]]++;
			}
			
		} else {
			// We have enough, update the min
			
			if (right - left < min) {
				min = right - left ;
				//cerr << "New min: " << min << endl;

			}
			
			//cerr << "Enough, but not a new min" << endl;

			left++; // Let's try with less
			
						
			if (left >= n) {
				break;
			}
			
			if (left > right) {
				right = left;
				
				for (int i = 0; i < triangleCount.size(); i++) {
					triangleCount[i] = 0;
				}
				
				for (int i = 0; i < maps[left].size(); i++) {
					triangleCount[maps[left][i]]++;
				}
				
				
			} else {
				for (int i = 0; i < maps[left - 1].size(); i++) {
					triangleCount[maps[left - 1][i]]--;
				}		
			
			}
			

		}
	
	}
	
	//cout << "Left: " << left << ", Right: " << right << endl;
	min++;
	cout << min << endl;   


}


int main() {
	
	std::ios_base::sync_with_stdio(false);

    long t;

    std::cin >> t;

	for (int i = 0; i < t; i++) {
		testCase();
	}


}
