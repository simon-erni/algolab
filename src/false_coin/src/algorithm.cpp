#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>


using namespace std;

#define trace(x) cerr << #x << ": " << x << endl

struct W {
    vector<int> l;
    vector<int> r;
    char out;
};

void print(const vector<int> &v) {
    for (int k : v) {
        cerr << k << " ";
    }
    cerr << endl;
}


void testcase() {
    int n,k;
    cin >> n >> k;


    vector<int> possible(n);
    for (int i = 0; i < n; i++) {
        possible[i] = i + 1;
    }


    vector<W> weighs(k);
    for (int i = 0; i < k; i++) {
        
        int p;
        cin >> p;

        weighs[i].l = vector<int>(p);
        weighs[i].r = vector<int>(p);

        for (int j = 0; j < p; j++ ) {
            cin >> weighs[i].l[j];
        }
        for (int j = 0; j < p; j++ ) {
            cin >> weighs[i].r[j];
        }       

        sort(weighs[i].l.begin(), weighs[i].l.end());
        sort(weighs[i].r.begin(), weighs[i].r.end());

        cin >> weighs[i].out;

    }

    cerr << "Possible: " << endl;
    print(possible);

    for (W w: weighs) {

        cerr << "Left: " << endl;
        print(w.l);
        cerr << "Right: " << endl;
        print(w.r);

        if (w.out == '=') {
            vector<int> newP;
            set_difference(possible.begin(), possible.end(), w.l.begin(), w.l.end(), inserter(newP, newP.begin()));
            possible = newP;

            newP = vector<int>();
            set_difference(possible.begin(), possible.end(), w.r.begin(), w.r.end(), inserter(newP, newP.begin()));
            possible = newP;
        }
        if (w.out == '<') {
            vector<int> newP;
            //All right may be
            set_intersection(possible.begin(), possible.end(), w.r.begin(), w.r.end(), inserter(newP, newP.begin()));
            possible = newP;
        }
        if (w.out == '>') {
            vector<int> newP;
            set_intersection(possible.begin(), possible.end(), w.l.begin(), w.l.end(), inserter(newP, newP.begin()));
            possible = newP;
        }
        cerr << "Possible: " << endl;
        print(possible);

        if (possible.size() <= 1) {
            break;
        }
    }

    if (possible.size() == 1) {
        cout << possible[0] << endl;
    } else {
        cout << "0" << endl;
    }




}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
