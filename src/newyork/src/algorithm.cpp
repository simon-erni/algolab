#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <utility>
#include <set>
#include <cassert>

#define debug if (debuge) cerr
const bool debuge = false;

using namespace std;

void testcase() {

    int n, m, k;
    cin >> n >> m >> k;

    cerr << "n: " << n << " m: "<< m << " k: " << k << endl;

    m--;

    vector<int> temp(n,0);
    vector<vector<int>> g(n,vector<int>());
    for (int i = 0; i < n ;i++) {
        cin >> temp[i];
    }

    for (int i = 0; i < n - 1; i++) {
        int u,v;
        cin >> u >> v;
        g[u].push_back(v);
    }
    vector<bool> start(n, false);

    vector<pair<int,int>> stack; //Node, next element to look at
    stack.reserve(n);
    stack.push_back(make_pair(0,0));

    multiset<int> pathRisk;

    while (!stack.empty()) {


        int u = stack.back().first;
        int next = stack.back().second;

        int pathLength = stack.size() - 1;

      //  debug << "visiting " << u << " with next: " << next << " pathLength: " << pathLength << " ... ";

       // debug << endl;
        stack.back().second++;

        if (next == 0) {
            pathRisk.insert(temp[u]);
        }


        if (pathLength >= m && next == 0) { //Only update at the beginning
            //Remove the dangling 
            if (pathLength > m) {
              //  debug << "removing at stack pos " << pathLength - m - 1 <<": " << stack[pathLength - m - 1].first << endl;
                pathRisk.erase(pathRisk.find(temp[stack[pathLength - m - 1].first]));
              //  debug << "erase complete " << endl;
            }
            
            //Compare the risk
            if (*pathRisk.rbegin() - *pathRisk.begin() <= k) {
                start[stack[pathLength -m].first] = true;
            }
        }

/*
        debug << "PathRisk: ";
        for (int i : pathRisk) {
            debug << i << " ";
        }
        debug << endl;
        assert(pathRisk.size() <= m + 1);
*/

        if (next < g[u].size()) {
           // debug << endl;
            stack.push_back(make_pair(g[u][next], 0));
        } else {
          //  debug << "no more to visit";
          //  debug << "P: " << pathLength << endl;
            if (pathLength > m) {
                //Add 'parent' to pathRisk again (if path was bigger before!)
                pathRisk.insert(temp[stack[pathLength - m - 1].first]);
            }
            
            pathRisk.erase(pathRisk.find(temp[u]));
            stack.pop_back();
        }
    }

    bool oneWasTrue = false;
    for (int i = 0; i < n; i++) {
        if (start[i]) {
            oneWasTrue = true;
            cout << i << " ";
        }
    }

    if (!oneWasTrue) {
        cout << "Abort mission";
    }

    cout << endl;
    
   // debug << "################" << endl;



}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
