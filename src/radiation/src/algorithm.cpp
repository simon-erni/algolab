#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <utility>

#include <iostream>
#include <cassert>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpz.h>


#define debug if (debuge) cerr
const bool debuge = false;
// choose exact integral type
typedef CGAL::Gmpz ET;

// program and solution types
typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;

using namespace std;

struct Coord{
    double x,y,z;
};

void addToLP(Program &lp, vector<Coord> &p, int d, int c, bool healthy) {
    

    for (int i = 0; i < p.size(); i++) {
        long v = 0;
        for (int x = 0; x <= d; x++) {
            for (int y = 0; y <= d && x + y <= d; y++) {
                for (int z = 0; z <= d && x + y + z <= d; z++) {

                    double value = (pow(p[i].x, x)) * (pow(p[i].y, y)) * (pow(p[i].z, z));
                    //debug << value << " * x^" << x << "y^" << y << "z^" << z << " + ";
                    lp.set_a(v, c + i, value);
                    v++;
                }
            }
        }
        if (healthy) {
            lp.set_r(i + c, CGAL::LARGER);
            lp.set_b(i + c, 1);
        } else {
            lp.set_r(i + c, CGAL::SMALLER);
            lp.set_b(i + c, -1);
        }
    }
}

void testcase() {
    long h_n,t_n;
    cin >> h_n >> t_n;

    vector<Coord> h(h_n);
    for (int i = 0; i < h_n; i++) {
        cin >> h[i].x >> h[i].y >> h[i].z;
    }

    vector<Coord> t(t_n);
    for (int i = 0; i < t_n; i++) {
        cin >> t[i].x >> t[i].y >> t[i].z;
    }

    for (int d = 0; d <= 30; d++) {
        Program lp (CGAL::SMALLER, false, 0, false, 0);

        addToLP(lp, h, d, 0, true);
        addToLP(lp, t, d, h_n, false);

        CGAL::Quadratic_program_options options;
        options.set_pricing_strategy(CGAL::QP_BLAND);
        Solution s = CGAL::solve_linear_program(lp, ET(), options);
        assert (s.solves_linear_program(lp));
        
        if (!s.is_infeasible()) {
            cout << d << endl;
            return;
        }
    }
    
    cout << "Impossible!" << endl;

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    cout << fixed << setprecision(0);
    int t;
    cin >> t;
    
    for (int i = 0; i < t ; i++) {
        testcase();
    }
}
