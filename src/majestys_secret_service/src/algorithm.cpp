#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <climits>
#include <set>

#define debug if (debugenabled) cerr

const bool debugenabled = false;

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/kruskal_min_spanning_tree.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/max_cardinality_matching.hpp>


typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::property<boost::edge_weight_t, int> 		// interior properties of edges
		>					UndGraph;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::property<boost::edge_weight_t, int> 		// interior properties of edges
		>					Graph;

typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef boost::property_map<Graph, boost::edge_weight_t>::type	WeightMap;	// property map to access the interior property edge_weight_t

typedef boost::graph_traits<UndGraph>::edge_descriptor		UndEdge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<UndGraph>::vertex_descriptor		UndVertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<UndGraph>::edge_iterator		UndEdgeIt;		// to iterate over all edges
typedef boost::graph_traits<UndGraph>::out_edge_iterator		UndOutEdgeIt;	// to iterate over all outgoing edges of a vertex
typedef boost::property_map<UndGraph, boost::edge_weight_t>::type	UndWeightMap;	// property map to access the interior property edge_weight_t

const UndVertex NULL_VERTEX = boost::graph_traits<UndGraph>::null_vertex();	// unmatched vertices get the NULL_VERTEX as mate.

using namespace std;

long n,m,a,s,c,d;

bool possible(vector<vector<int>> &D, int limit) {
    debug << "testing " << limit;

    
    int n =  a + c*s;
    UndGraph G(n);

    for (int i = 0; i < a; i++) {
        for (int j = 0; j < c*s; j++) {

            if (D[i][j] > limit) {
                continue;
            }
            
            boost::add_edge(i, a+j, G);
        }
    }

    std::vector<UndVertex> matemap(n);		// We MUST use this vector as an Exterior Property Map: Vertex -> Mate in the matching
	boost::edmonds_maximum_cardinality_matching(G, boost::make_iterator_property_map(matemap.begin(), get(boost::vertex_index, G)));
    int matchingsize = matching_size(G, boost::make_iterator_property_map(matemap.begin(), get(boost::vertex_index, G)));
    debug << "matching size: " << matchingsize;
    if (matchingsize == a) {
        debug << " true" << endl;
        return true;
    } else {
        debug << " false" << endl;
        return false;
    }

  

}

//0: first shelter
//1: 2nd shelter
int shelterToI(int shelter, int i) {
    int pos = n + i * s + shelter;
    debug << "shelter " << shelter <<"," << i << " is at pos " << pos << endl;
    return pos;
}

void testcase() {
    cin >> n >> m >> a >> s >> c >> d;


    Graph G(n + c * s);
    WeightMap weightmap = boost::get(boost::edge_weight, G);

    for (int i = 0; i < m; i++) {
        char w;
        cin >> w;
        long x,y,z;
        cin >> x >> y >> z;

        assert(z >= 0);

        Edge e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(x, y, G);
        weightmap[e] = z;

  
        if (w == 'L') {
            Edge e;
            boost::tie(e, boost::tuples::ignore) = boost::add_edge(y, x, G);
            weightmap[e] = z;
        }
    }

    vector<int> start;
    //Optimize: Eliminiate duplicates
    for (int i = 0; i < a; i++) {
        int x;
        cin >> x;

        start.push_back(x);
    }

    vector<int> end;

    for (int i = 0; i < s; i++) {

        int x;
        cin >> x;

        Edge e;
        boost::tie(e, boost::tuples::ignore) = boost::add_edge(x, shelterToI(i, 0), G);
        weightmap[e] = d;

        end.push_back(shelterToI(i,0));

        if (c == 2) {
            Edge e;
            boost::tie(e, boost::tuples::ignore) = boost::add_edge(shelterToI(i, 0), shelterToI(i, 1), G);
            weightmap[e] = d;

            end.push_back(shelterToI(i,1));

        }

        
    }

    assert(end.size() == s * c);

    vector<vector<int>> D(a, vector<int>(s*c, INT_MAX));

    set<int> limits;

    for (int i = 0; i < a; i++) {
        
            // Dijkstra shortest paths
            // =======================
            std::vector<Vertex> predmap(n + c*s);	// We will use this vector as an Exterior Property Map: Vertex -> Dijkstra Predecessor
            std::vector<int> distmap(n + c*s);		// We will use this vector as an Exterior Property Map: Vertex -> Distance to source
      
            boost::dijkstra_shortest_paths(G, start[i], // We MUST provide at least one of the two maps
                boost::predecessor_map(boost::make_iterator_property_map(predmap.begin(), boost::get(boost::vertex_index, G))).	// predecessor map as Named Parameter
                distance_map(boost::make_iterator_property_map(distmap.begin(), boost::get(boost::vertex_index, G))));	// distance map as Named Parameter
        
       /* for (int j = 0; j < n; j++) {
                        cerr << "from " << start[i] << " to " << j << " in " << distmap[j] << endl; 

        }*/

        for (int j = 0; j < s*c; j++) {
            int dist = distmap[end[j]];
            if (dist == INT_MAX) {
                continue;
            }
//            dist += d;
            D[i][j] = dist;
            limits.insert(dist);

            debug << "agent " << i << " starting at " << start[i] << " can reach shelter " << end[j] << " in " << dist << "s" << endl;
        }
    }

    vector<int> limit;
    debug << "Limits: ";
    for (auto it = limits.begin(); it != limits.end(); it++) {
        limit.push_back(*it);
        debug << *it << " ";
    }
    debug << endl;

    //Binary search on the upper limit
    int l = 0;
    int r = limit.size() - 1;

    while (l <= r) {
        debug << "Left: " << l << "Right: " << r << endl;
        if (l == r) {
            cout << limit[r] << endl;
            return;
        }

        if (r - l == 1) {
            if (possible(D,limit[l])) {
                cout << limit[l] << endl;
                return;
            }

            assert(possible(D,limit[r]));
            cout << limit[r] << endl;
            return;
        }

        int m = (r-l) /2 + l;

        if (possible(D, limit[m])) {
            r = m;
        } else {
            l = m + 1;
        }
    }

    debug << "Left: " << l << "Right: " << r << endl;



}



int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
