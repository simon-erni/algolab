#include <iostream>
#include <string>

#include <vector>
#include <algorithm>
// BGL includes
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>


using namespace std;

// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;

class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
	
		if (capacity == 0) {
			return;
		}
			
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
		
		//cerr << "adding edge from " << from << " to " << to << " with capacity " << capacity << endl;
	}
};


int charToInt(char c) {
	int r = c - 65;
	
	assert(0 <= r && r <= 25);
	
	return r;
}

int countOccurrences(string &s, int c) {
	int count = 0;
	
	for (unsigned long i = 0; i < s.size(); i++) {
		if (charToInt(s[i]) == c) {
			count++;
		}
	}
	
	return count;
}

void testcase() {

	cerr << "init..." << endl;

	int h, w;
	string note;

	cin >> h >> w;
	cin >> note;
	
	long total = 0;
		
	Graph G(26*2 + 2);
	
	int source = 0;
	int target = 26*2 + 1;
	
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);
	

	//cerr << "Read note: " << note << endl;
	cerr << "adding right edges... " << endl;
	
	for (int i = 0; i < 26; i++) {
		
		int from = 2*i + 2;
		
		int c = countOccurrences(note, i);
		eaG.addEdge(from, target, c);
		
		total += c;
		
	}
	
	cerr << "Total: " << total << endl;
	
	cerr << "adding front side" << endl;
	
	//contains the letter
	vector<vector<int>> pieces(h, vector<int>(w, 0));
	
	vector<vector<int>> letter(26, vector<int>(26, 0));
	
	for (int i = 0; i < h; i++) {
		for (int k = 0; k < w; k++) {
			char l;
			cin >> l;
			
			pieces[i][k] = charToInt(l);			
		}
	}
	
		
	for (int i = 0; i < h; i++) {
		for (int k = w - 1; k >= 0; k--) {
			char l;
			cin >> l;
			
			int c = charToInt(l);			
			
			int a = 0;
			int b = 0;
			
			if (c < pieces[i][k]) {
				a = c;
				b = pieces[i][k];
			} else {
				a = pieces[i][k];
				b = c;
			}
			
			letter[a][b]++;
		}
	}

	cerr << "adding all" << endl;
	for (int i = 0; i < 26; i++) {
		for (int k = i; k < 26; k++) {

			if (letter[i][k] == 0) {
				continue;
			}
			
			eaG.addEdge(0, 2*i + 1, letter[i][k]);
			eaG.addEdge(2*i + 1, 2*i + 2, letter[i][k]); //Optimize here maybe?
			
			eaG.addEdge(2*i + 2, 2*k + 2, letter[i][k]);
		}
	}

 	cerr << "calculate flow" << endl;

 	long flow = boost::push_relabel_max_flow(G, source, target);
 	
 	if (flow == total) {
 		cout << "Yes" << endl;
 	} else {
 		cout << "No" << endl;
 	}
	

}

int main() {
	ios_base::sync_with_stdio(false);
	
	int t;
	cin >> t;
	
	for (int i = 0; i < t; i++) {
		testcase();
	}
	
	return 0;
}