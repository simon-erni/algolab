#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>

using namespace std;

// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;


// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
	}
};
int z,j;

int zoneT(int zone) {
	return j + zone + 1;
}

int jobT(int job) {
	return job + 1;
}


void testcase() {
	
	cin >> z >> j;
	
	Graph G(z + j + 2);
	
	int source = 0;
	int target = z + j + 1;
	
	long totalRevenue = 0;
		
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);
	
	for (int i = 0; i < z; i++) {
		int cost;
		cin >> cost;
		
		eaG.addEdge(zoneT(i), target, cost);
	}
	
	for (int job = 0; job < j; job++) {
		int reward;
		cin >> reward;
		
		totalRevenue += reward;
			
		eaG.addEdge(source, jobT(job), reward);
	}
	
	
	for (int job = 0; job < j; job++) {
		int zonecount;
		cin >> zonecount;
		for (int i = 0; i < zonecount; i++) {
			int zone;
			cin >> zone;
			
			eaG.addEdge(jobT(job), zoneT(zone), 5000);
		}
	}
	
	cout << totalRevenue - boost::push_relabel_max_flow(G, source, target) << endl;
	
	
	

}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    
    for (int i = 0; i < t;i++) {
    	testcase();
    }
}
