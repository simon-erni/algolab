#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <cassert>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>
#include <CGAL/Gmpq.h>
#include <utility>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Delaunay_triangulation_2<K>  Triangulation;
typedef Triangulation::Edge_iterator  Edge_iterator;
typedef K::Point_2 Point_2;

#define DEBUG if (debug) cerr

bool debug = true;

// choose exact integral type
typedef CGAL::Gmpq ET;


typedef CGAL::Quadratic_program<ET> Program;
typedef CGAL::Quadratic_program_solution<ET> Solution;


using namespace std;


int getVariable(int n, int m, int s, int w) {
    
    return n * s + w;
    
}

void testcase() {
    int n,m,c;
    cin >> n >> m >> c;
    
    
    vector<Point_2> warehouseLocation(n); 
    vector<int> warehouseSupply(n);
    vector<int> warehouseContent(n);
    
    vector<Point_2> stadiumLocation(m);
    vector<int> stadiumDemand(m);
    vector<int> stadiumLimit(m);
    
    
    vector<vector<ET>> revenue(n, vector<ET>(m,0));

    for (int i = 0; i < n; i++) {
        cin >> warehouseLocation[i] >> warehouseSupply[i] >> warehouseContent[i];    
    }
    
    
    for (int i = 0; i < m; i++) {
        cin >> stadiumLocation[i] >> stadiumDemand[i] >> stadiumLimit[i];        
    }
    
    for (int i = 0 ; i < n; i++) {
        for (int k = 0; k < m; k++) {   
            cin >> revenue[i][k];
        }
    }
    
    
    //ToDo: Circles
    vector<pair<Point_2, long>> circles;
    for (int i = 0; i < c; i++) {
        
        Point_2 p;
        long r;
        cin >> p >> r;
        
        r = r * r;

        circles.push_back(make_pair(p,r));
        
    }

    
    
    
    //Assume lower bound, upper bound
    Program lp (CGAL::SMALLER, true, 0, false, 0);
    
    
    //Demand
    int constraints = 0;
    for (int s = 0; s < m; s++) {
        
        for (int w = 0; w < n; w++) {
            
            lp.set_a(getVariable(n,m,s,w), constraints, 1);
            
        }
        
        lp.set_b(constraints, stadiumDemand[s]);
        lp.set_r(constraints, CGAL::EQUAL);

        
        constraints++;
        
    }
    
    //Supply
    for (int w = 0; w < n; w++) {
        
        for (int s = 0; s < m; s++) {
            
            lp.set_a(getVariable(n,m,s,w), constraints, 1);
            
        }
        
        lp.set_b(constraints, warehouseSupply[w]);
        lp.set_r(constraints, CGAL::SMALLER);

        
        constraints++;
        
    }
    
    //Alcohol!
    for (int s = 0; s < m; s++) {
        
        
        DEBUG << "constraint " << constraints << " for " << s << endl;
        for (int w = 0; w < n; w++) {
            
            lp.set_a(getVariable(n,m,s,w), constraints, warehouseContent[w]);
            DEBUG << "w: " << w << " s: " << s << " i: " << getVariable(n,m,s,w) << " f: "<< warehouseContent[w] << endl;

        }
        
        lp.set_b(constraints, stadiumLimit[s] * 100);
        DEBUG << "stadiumLimit: " << stadiumLimit[s] << endl;
        lp.set_r(constraints, CGAL::SMALLER);

        
        constraints++;
        
    }
    
    Triangulation t;

    t.insert(stadiumLocation.begin(), stadiumLocation.end());
    t.insert(warehouseLocation.begin(), warehouseLocation.end());

    for (int i = 0; i < c ;i++) {
        
        if (CGAL::squared_distance(t.nearest_vertex(circles[i].first)->point(), circles[i].first) > circles[i].second) {
            continue;
        }
            
        for (int k = 0; k < n; k++) {
            for (int l = 0; l < m; l++) {
                bool w = CGAL::squared_distance(warehouseLocation[k], circles[i].first) < circles[i].second;
                bool s = CGAL::squared_distance(stadiumLocation[l], circles[i].first) < circles[i].second;

                if (w != s) {
                    revenue[k][l] -= ET(0.01);
                }

            }
        }
    }
    
    //Revenue
    for (int s = 0; s < m; s++) {
        
        for (int w = 0; w < n; w++) {
            
            lp.set_c(getVariable(n,m,s,w) , -revenue[w][s]);
            
        }
        
    }
    
    
    
    
    Solution s = CGAL::solve_linear_program(lp, ET());
    
    if (s.solves_linear_program(lp) && s.status() != CGAL::QP_INFEASIBLE) {
        cout << fixed;
        cout << (long) floor(-CGAL::to_double(s.objective_value()) )  << endl;
        /* 
        for (CGAL::Quadratic_program_solution<ET>::Variable_value_iterator opt = s.variable_values_begin(); opt != s.variable_values_end(); opt++) {
            cout << *opt << endl;
            
        }*/
        
        
    } else {
        cout << "RIOT!" << endl;
        
    }
    
}


int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    
    cin >> t;
    
   // t = 2;
    
    for (int i = 0; i < t; i++) {
    
        testcase();
        
    }
    
}