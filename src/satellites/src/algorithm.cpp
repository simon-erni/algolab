#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>
#include <queue>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>


// BGL Graph definitions
// =====================
// Graph Type with nested interior edge properties for Flow Algorithms
typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex


// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
	}
};

using namespace std;

int gg,ss;

int gs(int i) {
    return i + 1;

}

int st(int i) {

    return gg + i + 1;
}

void testcase() {

    int g,s,l;

    cin >> g >> s >> l;

    gg = g;
    ss = s;

    Graph G(g + s + 2);

    int source = 0;
    int target = g+s+1;

	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);

    for (int i = 0; i < l; i++) {

        //u = groundstation
        //v = satellite
        int u,v;
        cin >> u >> v;

        eaG.addEdge(gs(u),st(v),1);
    }

    for (int i = 0; i < g; i++) {
        
        eaG.addEdge(source,gs(i),1);

    }

     for (int i = 0; i < s; i++) {

         eaG.addEdge(st(i), target, 1);
        
    }

    boost::push_relabel_max_flow(G, source, target);
/*
    	EdgeIt ebeg, eend;
	for (tie(ebeg, eend) = edges(G); ebeg != eend; ++ebeg) {
		std::cerr << "edge from " << boost::source(*ebeg, G) << " to " << boost::target(*ebeg, G) 
				  << " runs " << capacitymap[*ebeg] - rescapacitymap[*ebeg]
                                  << " units of flow (negative for reverse direction)." << std::endl;
	}
*/
    queue<int> q;
    vector<bool> visited (g+s+2, false);

    q.push(source);

    while(!q.empty()) {

        Vertex u = q.front();
        q.pop();
        visited[u] = true;

		OutEdgeIt oebeg, oeend;
		for (boost::tie(oebeg, oeend) = boost::out_edges(u, G); oebeg != oeend; ++oebeg) {

            if (rescapacitymap[*oebeg] == 0) {
                continue;
            }

			Vertex v = boost::target(*oebeg, G);	// source(*oebeg, G) is guaranteed to be u, even in an undirected graph.

			if (!visited[v]) {
                q.push(v);
            }

        }

    }
/*
    cerr << "visited: ";
    for (int i = 0; i < g + s + 2; i++) {
        if (visited[i]) {
            cerr << i;
        }

    }
    cerr << endl;*/
    //all non visited in groundstation

    int countG = 0, countS = 0;

    for (int i = 0; i < g; i++) {
        if (!visited[gs(i)]) {
            countG++;
        }
    }

    //all  visited in satellites

    for (int i = 0; i < s; i++) {
        if (visited[st(i)]) {
            countS++;
        }
    }

    cout << countG << " " << countS << endl;
    for (int i = 0; i < g; i++) {
        if (!visited[gs(i)]) {
            cout << i << " ";
        }
    }

    for (int i = 0; i < s; i++) {
        if (visited[st(i)]) {
            cout << i << " ";
        }
    }
    if (countG + countS > 0) {
        cout << endl;
    }
    


}



int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {

        testcase();
    }


}
