#include <iostream>
#include <vector>
#include <cassert>
#include <utility>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>


typedef	boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::no_property,
	boost::property<boost::edge_capacity_t, long,
		boost::property<boost::edge_residual_capacity_t, long,
			boost::property<boost::edge_reverse_t, Traits::edge_descriptor> > > >	Graph;
// Interior Property Maps
typedef	boost::property_map<Graph, boost::edge_capacity_t>::type		EdgeCapacityMap;
typedef	boost::property_map<Graph, boost::edge_residual_capacity_t>::type	ResidualCapacityMap;
typedef	boost::property_map<Graph, boost::edge_reverse_t>::type		ReverseEdgeMap;
typedef	boost::graph_traits<Graph>::vertex_descriptor			Vertex;
typedef	boost::graph_traits<Graph>::edge_descriptor			Edge;
typedef	boost::graph_traits<Graph>::edge_iterator			EdgeIt;

using namespace std;

// Custom Edge Adder Class, that holds the references
// to the graph, capacity map and reverse edge map
// ===================================================
class EdgeAdder {
	Graph &G;
	EdgeCapacityMap	&capacitymap;
	ReverseEdgeMap	&revedgemap;

public:
	// to initialize the Object
	EdgeAdder(Graph & G, EdgeCapacityMap &capacitymap, ReverseEdgeMap &revedgemap):
		G(G), capacitymap(capacitymap), revedgemap(revedgemap){}

	// to use the Function (add an edge)
	void addEdge(int from, int to, long capacity) {
	
		//cerr << "adding edge from " << from << " to " << to << " with capacity " << capacity << endl;
		Edge e, rev_e;
		bool success;
		boost::tie(e, success) = boost::add_edge(from, to, G);
		boost::tie(rev_e, success) = boost::add_edge(to, from, G);
		capacitymap[e] = capacity;
		capacitymap[rev_e] = 0; // reverse edge has no capacity!
		revedgemap[e] = rev_e;
		revedgemap[rev_e] = e;
	}
};

void testcase() {

	int n,m;
	
	cin >> n >> m;
	
	vector<pair<int,int>> loc(n); //start, minL
	
	Graph G(n);
	
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, G);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, G);
	ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, G);
	EdgeAdder eaG(G, capacitymap, revedgemap);
		
	for (int i = 0; i < n; i ++) {
		int start, minL;
		
		cin >> start >> minL;
		loc[i] = make_pair(start,minL);
		
	}
	
	for (int i = 0; i < m; i++) {
		
		int u,v,min,max;
		cin >> u >> v >> min >> max;
		
		assert(max >= min);
		int capacity = max - min;
		
		//Assume already travelled
		loc[u].first -= min;
		loc[v].first += min;

		eaG.addEdge(u,v,capacity);
		
	}
	
	Vertex src = boost::add_vertex(G);
	Vertex sink = boost::add_vertex(G);
	
	long sum = 0;

	for (int i = 0; i < n; i++) {
	
		assert(loc[i].second >= 0);
		
		if (loc[i].first > 0) {
			eaG.addEdge(src, i, loc[i].first);
		}
		
		int capout = loc[i].second;
		if (loc[i].first < 0) {
			capout += (0 - loc[i].first);
		}
		
		assert(capout >= 0);
		
		eaG.addEdge(i, sink, capout);
		
		sum += capout;
	}


	long flow = boost::push_relabel_max_flow(G, src, sink);
	
	if (flow >= sum) {
		cout << "yes" << endl;
	} else {
		cout << "no" << endl;
	}
	
	

}


int main() {
	
	ios_base::sync_with_stdio(false);
	
	int t;
	
	t = 1;
	cin >> t;
	
	for (int i = 0; i < t; i++) {
		testcase();
	}

}