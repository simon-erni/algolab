#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <limits>
#include <stdexcept>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>


// BGL Graph definitions
// =====================
// Graph Type, OutEdgeList Type, VertexList Type, (un)directedS
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,		// Use vecS for the VertexList! Choosing setS for the OutEdgeList disallows parallel edges.
		boost::no_property,				// interior properties of vertices	
		boost::no_property	// interior properties of edges
		>					Graph;
typedef boost::graph_traits<Graph>::edge_descriptor		Edge;		// Edge Descriptor: an object that represents a single edge.
typedef boost::graph_traits<Graph>::vertex_descriptor		Vertex;		// Vertex Descriptor: with vecS vertex list, this is really just an int in the range [0, num_vertices(G)).	
typedef boost::graph_traits<Graph>::edge_iterator		EdgeIt;		// to iterate over all edges
typedef boost::graph_traits<Graph>::out_edge_iterator		OutEdgeIt;	// to iterate over all outgoing edges of a vertex


using namespace std;

int n;

int tI(int i, int j) {
    return n*i + j;
}

void addEdge(Graph &G, const vector<vector<bool>> &poss, int u, int v, int i, int j) {
    if (u >= n || u < 0 || v >= n || v < 0) {
        return;
    }

    if (i >= n || i < 0 || j >= n || j < 0) {
        return;
    }

    if (!poss[u][v] || !poss[i][j]) {
        return;
    }


    boost::add_edge(tI(u,v),tI(i,j),G);
}

void testcase() {


    cin >> n;
    vector<vector<bool>> poss(n, vector<bool>(n));
    long holes = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            bool x;
            cin >> x;
            poss[i][j] = x;
            if (!x) {
                holes++;
            }
        }
    }

    Graph G(n*n);

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            //addEdge(G, poss, i, j, i - 1, j - 2);
            //addEdge(G, poss, i, j, i - 1, j + 2);
            addEdge(G, poss, i, j, i + 1, j - 2);
            addEdge(G, poss, i, j, i + 1, j + 2);
            //addEdge(G, poss, i, j, i - 2, j - 1);
            //addEdge(G, poss, i, j, i - 2, j + 1);
            addEdge(G, poss, i, j, i + 2, j - 1);
            addEdge(G, poss, i, j, i + 2, j + 1);
        }
    }

    std::vector<Vertex> matemap(n*n);		// We MUST use this vector as an Exterior Property Map: Vertex -> Mate in the matching
	boost::edmonds_maximum_cardinality_matching(G, boost::make_iterator_property_map(matemap.begin(), get(boost::vertex_index, G)));
    long matchingsize = boost::matching_size(G, boost::make_iterator_property_map(matemap.begin(), get(boost::vertex_index, G)));

    cout << n*n - matchingsize - holes << endl;
    





}

int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false);
    int t;
    cin >> t;
    for (int i = 0; i < t; i++) {
        testcase();
    }
}
