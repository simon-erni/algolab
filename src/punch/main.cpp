#include <iostream>
#include <vector>
#include <utility>
#include <climits>

using namespace std;

bool debug = false;

/*
pair<int,int> calc(const vector<int> &c, const vector<int> &v, int i, int vol) {
	
	if (i < 0 || vol <= 0) {
		return make_pair(0,0);
	}

	


}*/


//Does a dominate b & c? (if two equal, returns true anyways)
bool dominate(const pair<int,int> &a, const pair<int,int> &b, const pair<int,int> &c) {
	
	if (a.second == 0) {
		return false;
	}
	
	if (a.first < b.first && a.first < c.first) {
		return true;
	}
	
	if (a.first > b.first || a.first > c.first) {
		return false;
	}
	
	// a.first <= b.first && a.first <= c.first holds now
	
	if (b.second == 0 && c.second == 0) {
		return true;
	}
	
	if (b.second == 0) {
		if (a.first < c.first || (a.first == c.first && a.second > c.second)) {
			return true;
		}	else {
			return false;
		}
	} else if (c.second == 0) {
		if (a.first < b.first || (a.first == b.first && a.second > b.second)) {
			return true;
		}	else {
			return false;
		}
	}
	
	
	//Compare seconds, first with 

	if (a.first == b.first && a.first < c.first && a.second > b.second) {
		return true;
	}

	if (a.first == c.first && a.first < b.first && a.second > c.second) {
		return true;
	}
	
	if (a.first == c.first && a.first == b.first && a.second >= b.second && a.second >= c.second) {
		return true;
	}
	
	
	
	
	return false;

}

void testcase() {

	int n,k;
	cin >> n >> k;
	
	vector<int> v(n,0);
	vector<int> c(n,0);
	
	
	
	for (int i = 0; i < n; i++) {
		cin >> c[i];
		cin >> v[i];
	}


	
	vector<vector<pair<int,int>>> dp(n, vector<pair<int,int>>(2*k + 1));

	
	for (int i = 0; i < n; i++) {
		for (int j = 1; j < 2*k + 1; j++) {
			
			pair<int,int> without = make_pair(INT_MAX,0);
			pair<int,int> withPrevious = make_pair(INT_MAX,0);
			pair<int,int> withSame = make_pair(INT_MAX,0);
			
			bool possible = false;
			
			
			if (i > 0) {
			
				if (dp[i - 1][j].second > 0 ) {
					without = dp[i - 1][j];	
					possible = true;
				}
				
				if ((j - v[i] > 0 && dp[i - 1][j - v[i]].second > 0)) {
					withPrevious = dp[i - 1][j - v[i]];
					withPrevious.first += c[i];
					withPrevious.second++;
					possible = true;
				}
			}
			
			if ((j - v[i] > 0 && dp[i][j - v[i]].second > 0) || j == v[i]) {
				withSame = dp[i][j - v[i]];
				withSame.first += c[i];
				possible = true;
				
				if (j == v[i]){
					withSame.second++;
				} 
			}
			
			
						
			if (debug) cerr << i << "," << j <<": without: " << without.first << ","<< without.second << " withSame: " << withSame.first << "," << withSame.second << " withPrevious " << withPrevious.first << "," << withPrevious.second << endl;
			
			if (!possible) {
				continue;
			}
			
			if (dominate(without, withPrevious, withSame) && without.second > 0)  {
				//Take without
				
				dp[i][j] = without;
				
				if (debug)  cerr << "Taking without" << endl;
				
			} else if (dominate(withPrevious, without, withSame) && withPrevious.second > 0) {
				//Take with previous
				
				dp[i][j] = withPrevious;
				
				if (debug)  cerr << "Taking withPrevious " << endl;
			} else {
				//Take with same (maybe it's impossible, so pay attention)
				
				dp[i][j] = withSame;
				if (debug) cerr << "Taking withSame " << endl;
			} 
			
			
			
			
			
		}
	}
	
	if (debug) {
	
	
	for (int i = 93; i < n; i++) {
	
		for (int j = 0; j < 2*k + 1; j++) {
			cerr << j << "|" << dp[i][j].first << "," << dp[i][j].second << " ";
		}
		cerr << endl;
	}
	}
	pair<int,int> sol = make_pair(INT_MAX, 0);
	
	for (int i = k; i < 2*k + 1; i++) {
		if (dp[n - 1][i].second > 0 && (dp[n - 1][i].first < sol.first || (dp[n - 1][i].first == sol.first && dp[n - 1][i].second > sol.second))) {
			sol = dp[n - 1][i];
			
			//cerr << "found sol: " << i << endl;
		}
	}
	
	cout << sol.first << " " << sol.second << endl;
	
	
	
	

}

int main() {
	ios_base::sync_with_stdio(false);
	
	int t;
	cin >> t;
	
	for (int i = 0; i < t; i++) {
		testcase();
	}
	
	return 0;
}