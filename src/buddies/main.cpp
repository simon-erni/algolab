#import <vector>
#import <string>
#import <iostream>
#import <algorithm>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

using namespace std;
using namespace boost;


typedef adjacency_list<vecS, vecS, undirectedS> Graph;
		
typedef graph_traits<Graph>::vertex_descriptor		Vertex;	// Vertex type		
typedef graph_traits<Graph>::edge_descriptor		Edge;	// Edge type
typedef graph_traits<Graph>::edge_iterator		EdgeIt;	// Edge iterator
typedef graph_traits<Graph>::out_edge_iterator	OutEdgeIt;
typedef graph_traits<Graph>::vertex_iterator	VertexIt;

int sameElements(vector<string> &first, vector<string> &second) {
	
	int k = 0;
	int count = 0;
	for (int i = 0; i < first.size() && k < second.size();) {
		
		if (first[i] == second[k]) {
			count++;
			i++;
			k++;
		} else if (first[i] < second[k]) {
			i++;
		} else {
			k++;
		}
		
	}
	
	return count;
	
}

void testCase() {
	
	int n, c, f;
	
	cin >> n >> c >> f;
	
	vector<vector<string>> traits(n,vector<string>(c));
	
	for (int i = 0; i < n; i++) {
	
		for (int k = 0; k < c; k++) {
			cin >> traits[i][k];
		}
		
		sort(traits[i].begin(), traits[i].end());
		
		
	}
	

	/*for (int i = 0; i < n; i++) {
		cerr << "Traits Student: " << i << " ";	
		for (int k = 0; k < c; k++) {
			cerr << traits[i][k] << " ";
		}
		
		cerr << endl;
		
	}*/
	
	Graph G(n);
	
	//cerr << "minimum: " << f << endl;
	for (int i = 0; i < n; i++) {
		
		for (int k = i + 1; k < n; k++) {
		
			
			
			int count = sameElements(traits[i], traits[k]);
			
			//cerr << "Student " << i << "," << k << "share " << count << endl;
			if (count - f > 0) {
				boost::add_edge(i,k,G);
			}
			
		}
		
	}
	/*
	EdgeIt eit, eend;
	for (tie(eit, eend) = edges(G); eit != eend; ++eit) {
	// eit is EdgeIterator, *eit is EdgeDescriptor}
		Vertex u = source(*eit, G), v = target(*eit, G);
		cerr << "added edge" << u << "," << v << endl;
	}
	*/
	vector<Vertex> mate(n);
	edmonds_maximum_cardinality_matching(G, &mate[0]);
	
	int matchingsize = boost::matching_size(G, boost::make_iterator_property_map( mate.begin(), boost::get(boost::vertex_index, G)));
	
	//cerr << "matchingsize: " << matchingsize << endl;
	if (matchingsize == n/2) {
		cout << "not optimal" << endl;
	} else {
		cout << "optimal" << endl;
	}
	
	
	

}

int main() {
	
	std::ios_base::sync_with_stdio(false);

    long t;

    std::cin >> t;

	for (int i = 0; i < t; i++) {
		testCase();
	}


}