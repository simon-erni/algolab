#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <cmath>
#include <iomanip>
#include <vector>

using namespace std;

/*
double prob(int days_passed, int current_amount, int target_amount) {

    auto cache = dp.find(make_pair(days_passed, current_amount));

    if (cache != dp.end()) {
        return cache->second;
    }

    if (days_passed == n) {
        if (current_amount >= target_amount) {
            return 1;
        } else {
            return 0;
        }
    }


    double max_prob = prob(days_passed + 1, current_amount, target_amount);

    for (int bet = 1; bet < current_amount; bet++) {

        double pro = prob(days_passed + 1, current_amount + bet, target_amount) * p[days_passed] +
                     prob(days_passed + 1, current_amount - bet, target_amount) * (1 - p[days_passed]);

        if (pro > max_prob) {
            max_prob = pro;
        }
    }

    double pro2 = prob(days_passed + 1, current_amount * 2, target_amount) * p[days_passed];

    if (pro2 > max_prob) {
        max_prob = pro2;
    }

    dp.insert(make_pair(make_pair(days_passed, current_amount), max_prob));


    return max_prob;
}*/

void testCase() {
    int n, start, e;

    cin >> n;
    cin >> start;
    cin >> e;

    vector<vector<double>> dp(n + 1, vector<double>(e + 1, 0));
    vector<double> p;

    for (int i = 0; i < n; i++) {
        double x;

        cin >> x;

        p.push_back(x);
    }

    //Init DP table
    dp[n][e] = 1; // All days passed, count the end

    for (int days_passed = n - 1; days_passed >= 0; days_passed--) {
        for (int current_amount = 1; current_amount <= e; current_amount++) {

            double max_prob = dp[days_passed + 1][current_amount]; // Bet 0
            for (int bet = 1; bet < current_amount && bet + current_amount <= e; bet++) { //MAYBE ERROR HERE

                double pro = dp[days_passed + 1][current_amount + bet] * p[days_passed] +
                             dp[days_passed + 1][current_amount - bet] * (1 - p[days_passed]);
                if (pro > max_prob) {
                    max_prob = pro;
                }

            }

            if (current_amount * 2 <= e) { // Bet all you've got
                double pro2 = dp[days_passed + 1][current_amount * 2] * p[days_passed];
                if (pro2 > max_prob) {
                    max_prob = pro2;
                }
            }

            dp[days_passed][current_amount] = max_prob;

        }
    }


    cout << fixed << std::setprecision(5);
    cout << dp[0][start] << endl;

}


int main() {

    ios_base::sync_with_stdio(false);

    int t;
    cin >> t;

    for (int i = 0; i < t; i++) {
        testCase();
    }

    return 0;
}