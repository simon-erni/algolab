#include <vector>
#include <iostream>
#include <cmath>
#include <utility>
#include <algorithm>

using namespace std;

int defuse(vector<bool> &d, int n, int i) {

	if (d[i]) {
		return 0;
	}

	d[i] = true;
	
	if (i >= (n-1) / 2) {
		return 1;
	}

	
	return defuse(d,n,i*2 + 1) + defuse(d,n,i*2 + 2) + 1;
	

}

void testcase() {
	
	int n;
	
	cin >> n;
	
	vector<pair<int,int>> b(n); //maxtime, index
	vector<bool> d(n, false);
	
	for (int i = 0; i < n; i++) {
		int max;
		cin >> max;
		b[i] = make_pair(max, i);
	}
	
	sort(b.begin(), b.end());
	
	long time = 0;
	for  (int i = 0; i < n; i++) {
		
		time += defuse(d,n,b[i].second);
		
		if (time > b[i].first) {
			cout << "no" << endl;
			return;
		}
	}
	
	cout << "yes" << endl;

}

int main() {

	ios_base::sync_with_stdio(false);
	
	int t;
	cin >> t;
	
	for (int i = 0; i < t; i++) {
		testcase();
	}
	

	return 0;
}

