#include <iostream>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Min_circle_2_traits_2.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Ray_2.h>
#include <CGAL/Point_2.h>
#include <CGAL/Segment_2.h>
#include <vector>

typedef CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt KE;
typedef CGAL::Min_circle_2_traits_2<KE>  Traits;
typedef CGAL::Min_circle_2<Traits>      Min_circle;
typedef CGAL::Circle_2<Traits>      Circle;
typedef CGAL::Exact_predicates_inexact_constructions_kernel KI;


typedef KI::Point_2 PointI;
typedef KI::Ray_2 RayI;
typedef KI::Segment_2 SegmentI;


typedef KE::Circle_2 CircleE;
typedef KE::Point_2 PointE;
typedef KE::Ray_2 RayE;
typedef KE::Segment_2 SegmentE;



double floor_to_double(const KE::FT& x) {
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

double ceil_to_double(const KE::FT& x) {
  double a = std::ceil(CGAL::to_double(x));
  while (a < x) a += 1;
  while (a-1 >= x) a -= 1;
  return a;
}


void testCase(long n) {

	PointE points[n];
	
	for (int i = 0; i < n; i++) {
		long x,y;
		std::cin >> x >> y;
		
		PointE point(x,y);
		points[i] = point;
	}
	
	Min_circle minCircle(points, points+n, true);
	std::cout << std::fixed << std::setprecision(0) << ceil_to_double(CGAL::sqrt(minCircle.circle().squared_radius())) << std::endl;

		
	
   

}


int main() {
	
	std::ios_base::sync_with_stdio(false);

    long n;

    std::cin >> n;

    while (n > 0) {
        testCase(n);
        std::cin >> n;
    }


}
